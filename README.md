# EvidenceGRADEr

## Table of Contents

- [Introduction](#introduction)
- [Getting started](#getting-started)
- [Usage](#usage)
  * [Dataset construction](#dataset-construction)
  * [Modelling](#modelling)
  * [Obtaining source data](#obtaining-source-data)
- [License](#license)
- [Contact](#contact)

## Introduction

This is the code repository for **EvidenceGRADEr**, a system for automatically assessing the quality of medical evidence
according to [GRADE](https://bestpractice.bmj.com/info/toolkit/learn-ebm/what-is-grade/), the most widely adopted
framework for grading the quality of evidence and for making recommendations. The system is based on three
sub-components that deal with encoding:

- numerical (using a FFNN),
- categorical (embedded representations), and
- textual (pretrained language model) inputs.

The resulting representations from the sub-components are handled by a top-level neural network.

The model can be set up to perform different tasks:

* four-tier GRADE scoring (**very low**, **low**, **moderate** and **high quality** of evidence),
* binary grading (**lower** vs. **higher** quality), and
* predicting fine-grained criteria representing the reason for downgrading the quality of evidence:
    * **risk of bias**,
    * **imprecision**,
    * **indirectness**,
    * **inconsistency**,
    * **publication bias**.

## Getting started

You may need to install the packages listed in the [requirements.txt](requirements.txt)
file: `pip install -r requirements.txt`.

If you would like to use Conda:

1. Install [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) if you haven't already.
2. Create a new Conda environment using the `environment.yml` file: `conda env create -f environment.yml`
3. Activate the new environment: `conda activate my_project`

## Usage

### Dataset construction

This step is relevant when you would like to scrape, parse, and build the dataset from scratch (assuming you have access
to source data from Cochrane Database of Systematic Reviews, if not,
see [Obtaining source data](#obtaining-source-data)). The entire procedure is explained in a separate
readme: [Dataset construction](sysrev/dataset_construction/README.md).

If you would like to work with a pre-built dataset, you can also contact the corresponding author of the project.

#### Obtaining source data

The approach has been developed using the Cochrane Database of Systematic Reviews. To obtain our derived data, please
contact first The Cochrane Collaboration by emailing support@cochrane.org. When Cochrane permits (at its discretion) the
use of the data to the third party, Cochrane will grant a licence to use the Cochrane Database of Systematic Reviews,
including a clause which confirms that Cochrane is allowing us to grant the third party access to the data set created
in this work.

### Modelling

The instruction for training and evaluating a model are found in [Running a model](sysrev/modelling/allennlp/README.md).
Generation of explanations is possible
with [Diverse Counterfactual Explanations (DiCE) for ML](https://interpret.ml/DiCE), see
this [script](sysrev/modelling/counterfactuals/play.py).

## License

This project is licensed under an MIT license. See the [LICENSE](LICENSE.txt) file for details.

## Contact

If you have any questions, please contact the corresponding author of the
project, [Simon Šuster](mailto:sim.suster@gmail.com).

## References

Here are some articles related to the project:

1. Šuster, S., Baldwin, T., Lau, J. H., Yepes, A. J., Iraola, D. M., Otmakhova, Y., & Verspoor, K. (
   2023). [Automating Quality Assessment of Medical Evidence in Systematic Reviews: Model Development and Validation Study](https://pubmed.ncbi.nlm.nih.gov/36722350/).
   Journal of Medical Internet Research, 25: e35568.
2. Šuster, S., Baldwin, T., & Verspoor, K. (
   2023). [Analysis of predictive performance and reliability of classifiers for quality assessment of medical evidence revealed important variation by medical area](https://www.jclinepi.com/article/S0895-4356(23)00091-4/fulltext).
   Journal of Clinical Epidemiology.
3. Šuster, S., Baldwin, T., & Verspoor, K. (
   2023). [Promoting Fairness in Classification of Quality of Medical Evidence](https://simonsuster.github.io/publications/promoting.pdf).
   BioNLP, ACL.

## Acknowledgements

We would like to thank Byron Wallace, Xudong Han, Artem Shelmanov, who have in various ways contributed to the
successful development of this project. We would also like to thank the Cochrane Collaboration and John Wiley & Sons
Limited for providing the source data of the Cochrane Database of Systematic Reviews. This research was funded by the
Australian Research Council through an Industrial Transformation Training Center Grant (grant IC170100030).