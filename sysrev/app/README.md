# EvidenceGRADEr app

This is a Streamlit app that allows users to interact with EvidenceGRADEr models. It is based on
the [EvidenceGRADEr](https://bitbucket.org/aimedtech/evidencegrader/) repository.

## Running

In PyCharm, open the project and run the `app.py` file with `streamlit` as a module. Use the following script
parameters:

```
run app.py --server.enableXsrfProtection false
```