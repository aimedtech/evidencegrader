from sysrev.dataset_construction.util.util import load_json


def load_config(f):
    config = load_json(f)
    archive_file_grade = config["archive_file_grade"]
    archive_file_rob = config["archive_file_rob"]
    archive_file_imp = config["archive_file_imp"]
    archive_file_ind = config["archive_file_ind"]
    archive_file_inc = config["archive_file_inc"]
    archive_file_hierarchical = config["archive_file_hierarchical"]
    use_hierarchical_predictor = config["use_hierarchical_predictor"]

    return (archive_file_grade, archive_file_rob, archive_file_imp, archive_file_ind, archive_file_inc,
            archive_file_hierarchical, use_hierarchical_predictor)
