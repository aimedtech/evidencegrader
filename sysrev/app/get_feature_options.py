from sysrev.dataset_construction.util.util import load_json

feature_options = load_json("config/feature_options.json")


def get_feature_options(field: str) -> list:
    """
    Get the pre-set app field options.
    :param field: feat name, such as "type_effect"
    """
    return feature_options[field]
