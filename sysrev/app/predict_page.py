from typing import Dict, List, Union, Any, Tuple, Set

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import streamlit as st
from allennlp.predictors import Predictor

from sysrev.app.get_feature_options import get_feature_options
from sysrev.app.util import load_config
from sysrev.modelling.allennlp.predict import get_predictor, build_instance, get_example, get_hierarchical_predictor


@st.cache_resource
def load_predictors(config_f: str) -> Dict[str, Predictor]:
    """
    Load all predictors from a config file's archive paths.
    :param config_f: json configuration file
    :return: dict of predictors
    """
    (archive_file_grade, archive_file_imp, archive_file_ind, archive_file_inc, archive_file_pub,
     archive_file_hierarchical, use_hierarchical_predictor) = load_config(config_f)

    if use_hierarchical_predictor:
        predictors = {"predictor_hierarchical": get_hierarchical_predictor(archive_file_hierarchical)}
    else:
        predictors = {"predictor_grade": get_predictor(archive_file_grade),
                      "predictor_rob": get_predictor(archive_file_imp),
                      "predictor_imp": get_predictor(archive_file_imp),
                      "predictor_ind": get_predictor(archive_file_ind),
                      "predictor_inc": get_predictor(archive_file_inc)}

    return predictors


@st.cache_resource
def load_hierarchical_predictor(config_f: str) -> Predictor:
    """
    Load a single hierarchical predictor from a config file's archive paths.
    Note that this predictor serves for both GRADE and reasons.

    :param config_f: json configuration file
    :return: dict of predictors
    """
    archive_file = load_hierarchical_config(config_f)
    predictors = get_hierarchical_predictor(archive_file)

    return predictors


def plot_output_probs(probs: list, labels: list) -> None:
    """
    Produce a ploty bar chart of the output probabilities.

    :param probs: class output probabilities
    :param labels: class labels
    """
    fig = go.Figure(data=go.Bar(y=probs, x=labels))
    fig.update_yaxes(range=[0, 1], title_text="output probability")
    st.plotly_chart(fig)


def predict_grade(inst: Dict[str, Any], predictor: Predictor) -> Tuple[str, List[float]]:
    instance = build_instance(predictor, inst)
    pred = predictor.predict_instance(instance)

    return pred["predicted"], pred["probs"]


def predict_hierarchical(inst: Dict[str, Any], predictor: Predictor) -> Tuple[str, List[float]]:
    instance = build_instance(predictor, inst)
    pred = predictor.predict_instance(instance)

    return (pred["predicted_grade"], pred["predicted_probs_grade"], pred["predicted_reasons"],
            pred["predicted_probs_reasons"], pred["grade_label_vocab"], pred["reasons_label_vocab"])


def do_grade(inst: Dict[str, Any], predictor: Predictor) -> None:
    """
    Predict the GRADE score for a single instance and show the output.
    :param inst: a dict instance containing feature values
    :param predictor: a predictor object
    """
    # Predict
    output, probs = predict_grade(inst, predictor)
    # Show the output
    st.metric("GRADE score", output)
    # Show the interpretation
    if output == "high-mod":
        st.write(
            "We are very to moderately confident that the true effect lies close to that of the estimate of the effect.")
    elif output == "not-high":
        st.write(
            "We have little confidence in the effect estimate, and the true effect is likely to be substantially different.")
    # Show the output probabilities
    labels = get_labels(predictor)
    plot_output_probs(probs, labels)


def predict_reasons(inst: Dict[str, Any], predictors: Dict[str, Predictor]) -> Set[str]:
    """
    Get outputs from all reason classifiers.

    :param inst: a dict instance containing feature values
    :param predictors: a dict of predictor objects
    :return: all recognised downgrading reasons
    """
    downgraded_outputs = set()  # Recognised downgrading reasons are put inside here
    for predictor_name, predictor in predictors.items():
        # Skip GRADE as we cover it separately
        if predictor_name.endswith("grade"):
            continue
        instance = build_instance(predictor, inst)
        pred = predictor.predict_instance(instance)
        output = pred["predicted"]
        if output != "other":
            downgraded_outputs.add(output)

    return downgraded_outputs


def do_reasons(inst: Dict[str, Any], predictors: Dict[str, Predictor]) -> None:
    # Predict the downgrading reasons
    downgraded_outputs = predict_reasons(inst, predictors)

    # Show
    rob_col, imp_col, inc_col, ind_col = st.columns(4)
    with rob_col:
        st.metric("Risk of bias", "↓") if "risk-of-bias" in downgraded_outputs else st.metric(
            "Risk of bias", "—")
    with imp_col:
        st.metric("Imprecision", "↓") if "imprecision" in downgraded_outputs else st.metric("Imprecision", "—")
    with inc_col:
        st.metric("Inconsistency", "↓") if "inconsistency" in downgraded_outputs else st.metric("Inconsistency",
                                                                                                "—")
    with ind_col:
        st.metric("Indirectness", "↓") if "indirectness" in downgraded_outputs else st.metric("Indirectness", "—")


def do_hierarchical(inst, predictor):
    (output, probs_grade, reasons, probs_reasons, grade_label_vocab,
     reasons_label_vocab) = predict_hierarchical(inst, predictor)

    # Show the output
    st.metric("GRADE score", output)
    # Show the interpretation
    if output in {"moderate", "high"}:
        st.write(
            "We are very to moderately confident that the true effect lies close to that of the estimate of the effect.")
    elif output in {"low", "very low"}:
        st.write(
            "We have little confidence in the effect estimate, and the true effect is likely to be substantially different.")
    # Show the output probabilities
    labels = get_labels_hierarchical(predictor)
    reindex_arr = [labels.index(label) for label in ["very low", "low", "moderate", "high"]]
    plot_output_probs(list(np.array(probs_grade)[reindex_arr]), list(np.array(labels)[reindex_arr]))
    # Show the downgrading reasons
    rob_col, imp_col, inc_col, ind_col, pub_col = st.columns(5)
    with rob_col:
        st.metric("Risk of bias", "↓") if "risk-of-bias" in reasons else st.metric(
            "Risk of bias", "—")
    with imp_col:
        st.metric("Imprecision", "↓") if "imprecision" in reasons else st.metric("Imprecision", "—")
    with inc_col:
        st.metric("Inconsistency", "↓") if "inconsistency" in reasons else st.metric("Inconsistency",
                                                                                     "—")
    with ind_col:
        st.metric("Indirectness", "↓") if "indirectness" in reasons else st.metric("Indirectness", "—")
    with pub_col:
        st.metric("Publication bias", "↓") if "publication-bias" in reasons else st.metric("Publication bias", "—")
    print()


def predict_button(name: str, inst: Dict[str, Any], predictors: Dict[str, Predictor]) -> None:
    result = st.button(name, type="primary")
    if result:
        print("Building instance.")
        if "predictor_hierarchical" in predictors:
            # use a single model for both GRADE and reasons
            predictor = predictors["predictor_hierarchical"]
            do_hierarchical(inst, predictor)
        else:
            # predict GRADE
            do_grade(inst, predictors["predictor_grade"])
            # predict justifications, i.e. downgrading reasons
            do_reasons(inst, predictors)


def predict_button_batch(name: str, insts: List[dict], predictors: Dict[str, Predictor]) -> None:
    result = st.button(name, type="primary")
    if result:
        if "predictor_hierarchical" in predictors:
            predictor = predictors["predictor_hierarchical"]
            labels = get_labels_hierarchical(predictor)
            reindex_arr = [labels.index(label) for label in ["very low", "low", "moderate", "high"]]
            label_0, label_1, label_2, label_3 = list(np.array(labels)[reindex_arr])
            outputs = {"grade": [], "risk-of-bias": [], "imprecision": [], "indirectness": [], "inconsistency": [],
                       "publication-bias": [], label_0: [], label_1: [], label_2: [], label_3: []}
            for c, i in enumerate(insts):
                inst = get_example(i)
                (output, probs_grade, reasons, probs_reasons, grade_label_vocab,
                 reasons_label_vocab) = predict_hierarchical(inst, predictor)

                # grade
                outputs["grade"].append(output)
                probs_grade = np.array(probs_grade)[reindex_arr]
                outputs[label_0].append(round(probs_grade[0], 3))
                outputs[label_1].append(round(probs_grade[1], 3))
                outputs[label_2].append(round(probs_grade[2], 3))
                outputs[label_3].append(round(probs_grade[3], 3))
                # reasons
                for reason in ["risk-of-bias", "imprecision", "indirectness", "inconsistency", "publication-bias"]:
                    outputs[reason].append(reason in reasons)

            st.write(pd.DataFrame(outputs,
                                  columns=["grade", "risk-of-bias", "imprecision", "indirectness", "inconsistency",
                                           "publication-bias",
                                           label_0, label_1, label_2, label_3]))

        else:
            predictor_grade = predictors["predictor_grade"]
            labels = get_labels(predictors["predictor_grade"])
            assert len(labels) == 2
            label_0, label_1 = labels
            outputs = {"grade": [], "risk-of-bias": [], "imprecision": [], "indirectness": [], "inconsistency": [],
                       label_0: [], label_1: []}
            for c, i in enumerate(insts):
                inst = get_example(i)
                # grade
                output, probs = predict_grade(inst, predictor_grade)
                outputs["grade"].append(output)
                outputs[label_0].append(round(probs[0], 3))
                outputs[label_1].append(round(probs[1], 3))
                # downgrading reasons
                downgraded_outputs = predict_reasons(inst, predictors)
                for reason in ["risk-of-bias", "imprecision", "indirectness", "inconsistency"]:
                    outputs[reason].append(reason in downgraded_outputs)

            st.write(pd.DataFrame(outputs,
                                  columns=["grade", "risk-of-bias", "imprecision", "indirectness", "inconsistency",
                                           label_0, label_1]))


def get_labels(predictor: Predictor) -> List[str]:
    index_to_label = predictor._model.index_to_label
    sorted_index_to_label = dict(sorted(index_to_label.items()))

    return list(sorted_index_to_label.values())


def get_labels_hierarchical(predictor: Predictor) -> List[str]:
    """ Access GRADE labels from the predictor when using the hierarchical predictor.
    It has two types of labels, one for GRADE and the other for reasons. Here, access GRADE labels.
    """
    index_to_label = predictor._model.index_to_label_top
    sorted_index_to_label = dict(sorted(index_to_label.items()))

    return list(sorted_index_to_label.values())


def initialise_inst() -> Dict[str, Union[None, List]]:
    """
    Create an empty instance corresponding to our required features.
    """
    return {"review_type": None,
            "topics": [],
            "year": None,
            "n_sofs": None,
            "n_outcomes": None,
            "ci_lower": None,
            "ci_upper": None,
            "n_participants": None,
            "n_studies": None,
            "relative_effect": None,
            "type_effect": None,
            "n_included_studies": None,
            "n_excluded_studies": None,
            "n_ongoing_studies": None,
            "n_additional_studies": None,
            "n_other_studies": None}


def show_predict_page(config_f: str) -> None:
    st.title(":label: :orange[Evidence]GRADE:orange[r]")
    st.subheader("This is an automated tool for grading the quality of medical evidence.",
                 help="The quality of evidence reflects the extent to which we are confident that an estimate of the effect is correct. Because systematic reviews do not, or at least should not, make recommendations, they require a different definition. Authors of systematic reviews grade quality of a body of evidence separately for each patient-important outcome. The quality of evidence is rated for each outcome across studies (i.e. for a body of evidence). This does not mean rating each study as a single unit. Rather, GRADE is “outcome centric”; rating is done for each outcome, and quality may differ - indeed, is likely to differ - from one outcome to another within a single study and across a body of evidence.")

    # Add sidebar selectbox
    mode = st.sidebar.selectbox("Choose mode", ["Single instance", "Batch mode"])

    # Load the predictor and do single vs. batch prediction
    predictors = load_predictors(config_f)
    if mode == "Batch mode":
        uploaded_file = st.sidebar.file_uploader("Upload CSV file", type=["csv"])
        if uploaded_file is not None:
            # Process and show the uploaded CSV file
            df = pd.read_csv(uploaded_file)
            df = df.fillna("")
            st.dataframe(df)
            # Predict scores for the uploaded CSV file
            insts = df.to_dict("records")
            predict_button_batch("Predict GRADE", insts, predictors)
    else:
        # Collect user input for a single instance
        inst = initialise_inst()

        title = st.text_area("Enter the title of the review here.")
        # PICO criteria
        # population = st.text_input("Enter the Population.", placeholder="e.g. Adults with cardiovascular disease")
        # intervention = st.text_input("Enter the Intervention.", placeholder="e.g. Mediterranean diet")
        # comparator = st.text_input("Enter the Comparator.", placeholder="e.g. No dietary intervention")
        # outcome = st.text_input("Enter the Outcome.", placeholder="e.g. Myocardial infarction")
        # Get categorical feature options
        type_effect_options = get_feature_options("type_effect")
        review_type_options = get_feature_options("review_type")
        topics_options = get_feature_options("topics")
        # Get meta information
        inst["topics"] = st.multiselect("Select relevant topics", topics_options)
        inst["review_type"] = st.selectbox("Select a review type", review_type_options, disabled=False)
        inst["year"] = st.number_input("Year of review", value=2021, step=1)
        # Get information about the studies
        st.write("Enter the details of studies here.")
        inst["n_studies"] = st.number_input("Number of studies", value=1, step=1)
        inst["n_participants"] = st.number_input("Number of participants", value=1, step=1)
        inst["n_sofs"] = st.number_input("Number of summaries of findings", value=1, step=1)
        inst["n_outcomes"] = st.number_input("Number of outcomes", value=1, step=1)
        # Get information about the effect
        inst["type_effect"] = st.selectbox("Type of effect", type_effect_options)
        inst["relative_effect"] = st.number_input("Relative effect", value=0., step=0.1)
        inst["ci_lower"] = st.number_input("Lower bound of confidence interval", value=0., step=0.1)
        inst["ci_upper"] = st.number_input("Upper bound of confidence interval", value=0., step=0.1)
        # Get eligibility information
        inst["n_included_studies"] = st.number_input("Number of included studies", value=0, step=1)
        inst["n_excluded_studies"] = st.number_input("Number of excluded studies", value=0, step=1)
        inst["n_ongoing_studies"] = st.number_input("Number of ongoing studies", value=0, step=1)
        inst["n_additional_studies"] = st.number_input("Number of additional studies", value=0, step=1)
        inst["n_other_studies"] = st.number_input("Number of other studies", value=0, step=1)
        # Predict the quality score
        predict_button("Predict evidence quality", inst, predictors)
