# DiCE imports
import dice_ml
from dice_ml.utils import helpers  # helper functions

dataset = helpers.load_adult_income_dataset()

# Dataset for training an ML model
d = dice_ml.Data(dataframe=dataset,
                 continuous_features=['age', 'hours_per_week'],
                 outcome_name='income')
# Pre-trained ML model
m = dice_ml.Model(model_path=dice_ml.utils.helpers.get_adult_income_modelpath(backend="sklearn"), backend="PYT")
# m = dice_ml.Model(model_path=dice_ml.utils.helpers.get_adult_income_modelpath())
# DiCE explanation instance
exp = dice_ml.Dice(d, m)

query_instances = [{'age': 22,
                    'workclass': 'Private',
                    'education': 'HS-grad',
                    'marital_status': 'Single',
                    'occupation': 'Service',
                    'race': 'White',
                    'gender': 'Female',
                    'hours_per_week': 45}]

# Generate counterfactual examples
dice_exp = exp.generate_counterfactuals(query_instances, total_CFs=4, desired_class="opposite")
# Visualize counterfactual explanation
dice_exp.visualize_as_dataframe()

print()
