require(ggplot2)
file2 = "/home/simon/Apps/SysRev/data/dataset/plots/grade_faceted/grade_faceted_pred_err_topic_highmod_vs_lowvlow_extended"
f2 = read.table(file2, skip = 0, header = TRUE, sep = "\t", na.strings = "NA", dec = ".", strip.white = TRUE)
d2 = data.frame(f2)
#d1$acc = 1-d1$error_prop
#d1$topic <-factor(d1$topic, levels=d1$topic[order(d1$acc, decreasing=TRUE)])
d2$topic <- factor(d2$topic, levels = d2$topic[order(d2$error_prop, decreasing = FALSE)])
d2 <- subset(d2, d2$topic != "Child health")


library(faraway)
pairs(d2, col = "dodgerblue", panel = panel.smooth)

#error_prop_vs_brier = data.frame(value=c(d1$error_prop,d1$ece_pos), metric=c(rep("error_prop", length(d1$error_prop)), rep("brier", length(d1$ece_pos))), topic=c(d1$topic, d1$topic))
error_prop_vs_brier = data.frame(value = c(d2$error_prop, d2$ece_pos), metric = c(rep("error_prop", length(d2$error_prop)), rep("ECE", length(d2$ece_pos))), topic = c(d2$topic, d2$topic))

ggplot(error_prop_vs_brier, aes(x = topic, y = value, colour = metric)) +
  geom_point(aes(colour = metric, shape = metric), size = 3.5) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 35, vjust = 1, hjust = 1, size = 14),
        #panel.grid.minor = element_blank(),
        #panel.grid.major = element_blank(),
        axis.text.y = element_text(size = 14),
        axis.title.x = element_text(size = 18),
        axis.title.y = element_text(size = 18),
        panel.background = element_blank(),
        legend.title = element_text(size = 14),
        legend.text = element_text(size = 14),
        panel.border = element_blank()) #+ theme(axis.text.x = element_blank(), axis.title.x = element_blank())

#ggsave("/home/simon/Apps/SysRev/data/modelling/plots/err_vs_brier.png", width=9, height=7)
ggsave("/home/simon/Apps/SysRev/data/modelling/plots/err_vs_ece.png", width = 12, height = 7)

#p2= ggplot(d1, aes(x=topic, y=brier)) + geom_point() +  scale_y_continuous(limits = c(0, 1)) + theme(axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))

#ggarrange(p1, p2, ncol=1, nrow=2)
#library(grid)
#grid.newpage()
#grid.draw(rbind(ggplotGrob(p1), ggplotGrob(p2), size = "last"))
#ggsave("/home/simon/Apps/SysRev/data/modelling/plots/acc_vs_brier.png")
