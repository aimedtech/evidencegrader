from collections import defaultdict
from pathlib import Path

from sysrev.dataset_construction.util.util import load_json

# on SLUG
source_dirs = [
    "/scratch/ssuster/Apps/SysRev/data/modelling/saved/learning_curve/numerical/"]  # , "/scratch/ssuster/Apps/SysRev/data/modelling/saved/learning_curve/ordinal_alltext/", "/scratch/ssuster/Apps/SysRev/data/modelling/saved/learning_curve/simple_transformer_text2/"]

for source_dir in source_dirs:
    dir_name = Path(source_dir).name
    scores = defaultdict(list)
    for tenth_part in range(1, 11):
        fh = load_json(f"{source_dir}{tenth_part}_10_{dir_name}_0/metrics.json")
        if "best_validation_mae" in fh:
            scores["mae"].append(fh["best_validation_mae"])
        if "best_validation_accuracy" in fh:
            scores["accuracy"].append(fh["best_validation_accuracy"])
        if "best_validation_precision_macro" in fh:
            scores["precision"].append(fh["best_validation_precision_macro"])
        if "best_validation_recall_macro" in fh:
            scores["recall"].append(fh["best_validation_recall_macro"])
        if "best_validation_fscore_macro" in fh:
            scores["fscore"].append(fh["best_validation_fscore_macro"])

        # binary label 0
        if "best_validation_precision_macro_class0" in fh:
            scores["precision_class0"].append(fh["best_validation_precision_macro_class0"])
        if "best_validation_recall_macro_class0" in fh:
            scores["recall_class0"].append(fh["best_validation_recall_macro_class0"])
        if "best_validation_fscore_macro_class0" in fh:
            scores["fscore_class0"].append(fh["best_validation_fscore_macro_class0"])

        # binary label 1
        if "best_validation_precision_macro_class1" in fh:
            scores["precision_class1"].append(fh["best_validation_precision_macro_class1"])
        if "best_validation_recall_macro_class1" in fh:
            scores["recall_class1"].append(fh["best_validation_recall_macro_class1"])
        if "best_validation_fscore_macro_class1" in fh:
            scores["fscore_class1"].append(fh["best_validation_fscore_macro_class1"])

        if "training_mae" in fh:
            scores["training_mae"].append(fh["training_mae"])

    out_f = f"{source_dir}learning_curve_{dir_name}"
    with open(out_f, "w") as fh_out:
        fh_out.write("i score metric\n")
        for metric, score_list in scores.items():
            for i, score in enumerate(score_list, 1):
                fh_out.write(f"{i} {score} {metric}\n")
    print(f"Written to: {out_f}")

# copy out_f to local /home/simon/Apps/SysRev/data/modelling/learning_curve:
# scp -P 7000 ssuster@localhost:/scratch/ssuster/Apps/SysRev/data/modelling/saved/learning_curve/numerical/learning_curve_numerical /home/simon/Apps/SysRev/data/modelling/learning_curve/numerical/
