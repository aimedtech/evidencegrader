import csv

from sysrev.dataset_construction.util.util import read_csv

# model_to_path = {
#    "RR: AC": "/home/ssuster/tmp_pycharm_project_989/data/dataset/robotreviewer_metrics_per_topicsAllocation concealment.csv",
#    "RR: BOA": "/home/ssuster/tmp_pycharm_project_989/data/dataset/robotreviewer_metrics_per_topicsBlinding of outcome assessment.csv",
#    "RR: BPP": "/home/ssuster/tmp_pycharm_project_989/data/dataset/robotreviewer_metrics_per_topicsBlinding of participants and personnel.csv",
#    "RR: RSG": "/home/ssuster/tmp_pycharm_project_989/data/dataset/robotreviewer_metrics_per_topicsRandom sequence generation.csv",
#    "EG: binary GRADE": "/home/ssuster/tmp_pycharm_project_989/data/dataset/evidencegrader_metrics_per_topicsbinary GRADE.csv",
#    "EG: imp": "/home/ssuster/tmp_pycharm_project_989/data/dataset/evidencegrader_metrics_per_topicsimprecision.csv",
#    "EG: inc": "/home/ssuster/tmp_pycharm_project_989/data/dataset/evidencegrader_metrics_per_topicsinconsistency.csv",
#    "EG: ind": "/home/ssuster/tmp_pycharm_project_989/data/dataset/evidencegrader_metrics_per_topicsindirectness.csv",
#    "EG: pub": "/home/ssuster/tmp_pycharm_project_989/data/dataset/evidencegrader_metrics_per_topicspublication bias.csv",
#    "EG: rob": "/home/ssuster/tmp_pycharm_project_989/data/dataset/evidencegrader_metrics_per_topicsrisk of bias.csv"
# }

# on rat:

model_to_path = {
    "RR: AC": "/home/ssuster/sysrev/data/dataset/robotreviewer_metrics_per_topicsAllocation concealment.csv",
    "RR: BOA": "/home/ssuster/sysrev/data/dataset/robotreviewer_metrics_per_topicsBlinding of outcome assessment.csv",
    "RR: BPP": "/home/ssuster/sysrev/data/dataset/robotreviewer_metrics_per_topicsBlinding of participants and personnel.csv",
    "RR: RSG": "/home/ssuster/sysrev/data/dataset/robotreviewer_metrics_per_topicsRandom sequence generation.csv",
    "EG: binary GRADE": "/home/ssuster/sysrev/data/dataset/evidencegrader_metrics_per_topicsbinary GRADE.csv",
    "EG: imp": "/home/ssuster/sysrev/data/dataset/evidencegrader_metrics_per_topicsimprecision.csv",
    "EG: inc": "/home/ssuster/sysrev/data/dataset/evidencegrader_metrics_per_topicsinconsistency.csv",
    "EG: ind": "/home/ssuster/sysrev/data/dataset/evidencegrader_metrics_per_topicsindirectness.csv",
    "EG: pub": "/home/ssuster/sysrev/data/dataset/evidencegrader_metrics_per_topicspublication bias.csv",
    "EG: rob": "/home/ssuster/sysrev/data/dataset/evidencegrader_metrics_per_topicsrisk of bias.csv"
}

for score in ["ece", "brier", "error_prop", "aurc", "f1", "label_ratio", "entropy", "variance_from_point_five"]:
    all_data = {model: {} for model in model_to_path.keys()}
    for model, f in model_to_path.items():
        for row in read_csv(f, delimiter="\t"):
            topic = row["topic"]
            topic.replace("_", " ")
            all_data[model][row["topic"]] = row[score]

    # f_out = "/home/ssuster/tmp_pycharm_project_989/data/dataset/metrics_per_topic_per_model_" + score + ".csv"
    f_out = "/home/ssuster/sysrev/data/dataset/metrics_per_topic_per_model_" + score + ".csv"
    topics = list({topic for model, scores_per_topic in all_data.items() for topic in scores_per_topic.keys()})
    with open(f_out, "w") as fh_out:
        writer = csv.writer(fh_out)
        writer.writerow(["model"] + topics)
        for model, scores_per_topic in all_data.items():
            writer.writerow([model] + [scores_per_topic.get(topic, 'NA') for topic in topics])
