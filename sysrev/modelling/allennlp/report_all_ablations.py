import argparse
import os
from collections import defaultdict

import numpy as np

from sysrev.dataset_construction.util.util import get_dir_list, load_json
from sysrev.modelling.allennlp.util import accumulate_metrics


def report_metrics(metrics):
    scores = []
    for metric in ["test_mae", "test_precision_macro", "test_recall_macro", "test_fscore_macro",
                   "test_fscore_macro_class_high",
                   "test_fscore_macro_class_moderate", "test_fscore_macro_class_low",
                   "test_fscore_macro_class_very low"]:
        scores.append(np.mean(metrics[metric]))

    return scores


config = load_json("configs/report.json")
serialization_path = config["serialization_path"]
exp_paths = get_dir_list(serialization_path, identifiers=["class_nonaugm_all_abl_"])
n_folds = 10

for exp_path in exp_paths:
    metrics = defaultdict(list)
    exp_name = os.path.basename(exp_path)
    for k in range(n_folds):
        serialization_dir = f"{exp_path}/{exp_name}_{k}"
        accumulate_metrics(metrics, serialization_dir)

    scores = report_metrics(metrics)
    scores_str = '\t'.join([str(round(score, 3)) for score in scores])
    print(f"{exp_name}\t{scores_str}")
