import pickle
from pathlib import Path

import dice_ml
import pandas as pd
from allennlp.models import load_archive
from tqdm import tqdm

from sysrev.dataset_construction.util.util import load_json
from sysrev.modelling.allennlp.counterfactuals import get_dataframe
from sysrev.modelling.allennlp.my_project.dataset_reader import NUMERICAL_FEATS

pd.set_option("display.max_rows", None, "display.max_columns", None)

config = load_json("configs/counterfactual.json")
data_dir = config["data_dir"]
serialization_dir = config["serialization_dir"]

archive = load_archive(serialization_dir + "model.tar.gz")
dataset = get_dataframe(data_dir=data_dir, archive=archive)

# Dataset for training an ML model
continuous_features = [f for f in NUMERICAL_FEATS if f != "n_additional_studies"]
d = dice_ml.Data(dataframe=dataset,
                 continuous_features=continuous_features,
                 outcome_name='label')

# Pre-trained ML model
model = archive.model
custom_backend = {"model": "allennlp_model.AllennlpModel", "explainer": "dice_pytorch.DicePyTorch"}
m = dice_ml.Model(model=model, backend=custom_backend)

# DiCE explanation instance
exp = dice_ml.Dice(d, m)

# produce CFs for this n of instances from the training set:
sample_size = 10

query_instances = dataset.sample(n=sample_size).drop("n_additional_studies", axis=1).to_dict("records")

# example query:
# query_instances = [{'n_participants': 99.0, 'n_studies': 4.0, 'year': 2013.0, 'n_sofs': 2.0, 'n_outcomes': 6.0,
#                    'ci_lower': 1.0299999713897705, 'ci_upper': 1.2400000095367432, 'relative_effect': 1.1299999952316284,
#                    'n_included_studies': 10.0, 'n_excluded_studies': 8.0, 'n_ongoing_studies': 0.0,
#                    'n_other_studies': 0.0}]  # 'label': 'not-high'

# Define the permitted range for each feature
permitted_range = {'n_participants': [10, 50000], 'n_studies': [3, 6254], 'year': [2003, 2020], 'n_sofs': [1, 5],
                   'n_outcomes': [1, 10],
                   'ci_lower': [0., 795.], 'ci_upper': [0., 15978.], 'relative_effect': [0., 998.],
                   'n_included_studies': [3., 500.], 'n_excluded_studies': [0., 451.], 'n_ongoing_studies': [0., 89.],
                   'n_other_studies': [0., 10.]}

# Generate counterfactual examples
features_to_vary = [f for f in NUMERICAL_FEATS if f not in {"year"}]

# Generate counterfactuals for different feature weights and proximity weights
total_CFs = 10
for feature_weight in ["inverse_mad", "uniform"]:
    if feature_weight == "uniform":
        feature_weights = {f: 1. for f in features_to_vary}
    else:
        feature_weights = feature_weight
    for proximity_weight in [0.5, 1., 1.5, 2., 2.5]:
        outdir = serialization_dir + f"CFs_prox{proximity_weight}_w{feature_weight}_{sample_size}_{total_CFs}/"
        if not Path(outdir).exists():
            Path(outdir).mkdir()
        outfile = outdir + "counterfactuals.pickle"
        outfile_interim = outdir + "counterfactuals_dice_exps.pickle"

        if Path(outfile_interim).exists():
            with open(outfile_interim, "rb") as fh:
                dice_exps = pickle.load(fh)
        else:
            dice_exps = []
            for query_instance in tqdm(query_instances):
                dice_exps.append(
                    exp.generate_counterfactuals(query_instance, total_CFs=total_CFs, desired_class="opposite",
                                                 permitted_range=permitted_range,
                                                 proximity_weight=proximity_weight,
                                                 features_to_vary=features_to_vary,
                                                 feature_weights=feature_weights))
            with open(outfile_interim, "wb") as fh:
                pickle.dump(dice_exps, fh)
            print(f"Saved to {outfile}.")
