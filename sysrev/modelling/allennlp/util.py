import json
import os
from pathlib import Path

import _jsonnet
import numpy as np
from scipy.stats import stats

from sysrev.dataset_construction.util.util import load_json, get_file_list
from sysrev.modelling.allennlp.configs.constants import *


def load_jsonnet(f):
    """
    Load a jsonnet main configuration file and return the json object.
    """
    eval_f = _jsonnet.evaluate_file(f)

    return json.loads(_jsonnet.evaluate_snippet("", eval_f))


def spawn_config_files(master_f, n_folds, tenth_part=None, start_fold=0):
    """
    We use the same master config file for all folds. This function spawns the config files for each fold.

    :param master_f: path to the master config file in jsonnet format, normally located in the training_config directory
    :param n_folds: defined in a main script, usually 10
    :param tenth_part: used for creating learning curves, where we only use incremnets of 10% of the data
    :param start_fold: used for omitting some folds, e.g. when we want to run only a subset of folds

    :return: yields the path to the config file for each fold
    """
    master_f_obj = Path(master_f).absolute()
    dir_folds = f"{master_f_obj.parent}/{master_f_obj.stem}"
    Path(dir_folds).mkdir(exist_ok=True)

    for i in range(start_fold, n_folds):
        out_f = f"{dir_folds}/{master_f_obj.stem}_{i}{master_f_obj.suffix}"

        with open(master_f) as fh, open(out_f, "w") as fh_out:
            f_read = fh.read()
            f_read = f_read.replace("FOLD", str(i))
            if tenth_part is not None:
                f_read = f_read.replace("PART", f"_{tenth_part}_10")
            else:
                f_read = f_read.replace("PART", "")
            fh_out.write(f_read)

        yield out_f


def accumulate_metrics(metrics, serialization_dir, fn="metrics.json", metric_prefix_name=True):
    """
    :param metrics:
    :param serialization_dir:
    :param fn:
    :param metric_prefix_name: whether to require best_validation* and test* as prefixes for the metrics that will be read. This shuold be False when running allennlp in evaluate mode.
    :return:
    """
    try:
        d = load_json(f"{serialization_dir}/{fn}")
        for metric in d:
            if metric.startswith("best_validation") or metric.startswith("test_"):
                metrics[metric].append(d[metric])
            else:
                if not metric_prefix_name:
                    metrics[metric].append(d[metric])
    except FileNotFoundError:
        print(f"{serialization_dir}/{fn} missing.")
        pass


def report_metrics(metrics, f1_average="macro", labels=None):
    for metric, vals in metrics.items():
        if metric in {"best_epoch", "training_duration", "epoch"}:
            continue
        if f1_average is None and metric in {"f1_macro", "precision_macro", "recall_macro"}:
            assert labels is not None
            for i in range(len(labels)):
                cl_i = [val[i] for val in vals]
                mean_i = np.mean(cl_i)
                median_i = np.median(cl_i)
                try:
                    mad_i = stats.median_absolute_deviation(cl_i)
                except AttributeError:
                    mad_i = stats.median_abs_deviation(cl_i)
                print(f"{metric}_{labels[i]}: mean: {mean_i}, median: {median_i}, MAD: {mad_i}")
        else:
            mean = np.mean(vals)
            median = np.median(vals)
            try:
                mad = stats.median_absolute_deviation(vals)
            except AttributeError:
                mad = stats.median_abs_deviation(vals)
            print(f"{metric}: mean: {mean}, median: {median}, MAD: {mad}")


def label_to_idx(label, binary=False, binary_type="high-vs-rest", num_reasons=False):
    if binary:
        if binary_type == "high-vs-rest":
            mapping = BINARY_MAPPING1
        elif binary_type == "high-mod-vs-rest":
            mapping = BINARY_MAPPING2
        else:
            raise NotImplementedError
    else:
        if num_reasons:
            mapping = NUM_REASONS_MAPPING
        else:
            mapping = MAPPING

    return mapping[label]


def cleanup_large_files(ser_dir):
    for f in get_file_list(ser_dir):
        if f.endswith(".th"):
            os.remove(f)
