# Running a model

`python main.py configs/mains/some_config.json` will train a model. The main configuration file specifies the basic
parameters such as serialization paths, the training device, and the path to the master configuration file. The latter
includes all finer-grained modelling parameters. It is a jsonnet configuration from [training_config](training_config).
A training run normally involves 10-fold cross validation.

The master configuration files define various training and evaluation settings, including:

- `feat_type`: which set of features to use (numerical, categorical, textual)
- `dataset_reader` type, which is a class from [my_project/dataset_reader.py](my_project/dataset_reader.py)
- `max_instances`: for debugging purposes, reads a limited number of data instances
- `train_data_path`, `validation_data_path`, `test_data_path`: point to csv files (
  e.g. `data/derivations/all/splits/FOLD/dev.csv`). Note the use of FOLD placeholder in the path name, which gets
  replaced during runtime with actual fold numbers ranging from 0 to 9. The data needs to be available for each
  cross-validation iteration, e.g. `data/derivations/all/splits/0/dev.csv`, `data/derivations/all/splits/1/dev.csv`,
  etc.
- `model` type: the name of the model that matches a class in [my_project/model.py](my_project/model.py)
- other data loader and trainer parameters

It is recommended to prepend the main python command with `PYTHONHASHSEED=0` to ensure reproducibility of the results.
We fix the other sources of randomness already in the code.