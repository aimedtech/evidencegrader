local feat_type = {"num": true, "text": true, "cat": true};
local bert_model = "allenai/scibert_scivocab_uncased";

# split seqs that are too long into smaller segments:
local bert_truncate_length = 512;

// For more info on config files generally, see https://guide.allennlp.org/using-config-files
{
"random_seed": 2022,
"numpy_seed": 2022,
"pytorch_seed": 2022,
    "evaluate_on_test": true,
    "dataset_reader" : {
        // This name needs to match the name that you used to register your dataset reader, with
        // the call to `@DatasetReader.register()`.
        "type": "bi-classification-simple-2",
        # "max_instances": 100, # uncomment to limit the number of instances read
        "feat_type": feat_type,
        "token_indexers": {
            "tokens": {
                "type": "pretrained_transformer",
                "model_name": bert_model,
                # split seqs that are too long into smaller segments:
                #"max_length": bert_max_length
            }
        },
        "tokenizer": {
            //"type": "spacy",
            "type": "pretrained_transformer",
            "model_name": bert_model,
            // truncate the seq if too long
            "max_length": bert_truncate_length
		}
    },
    "train_data_path": "../../data/derivations/all/splits/FOLD/train.csv",
    "validation_data_path": "../../data/derivations/all/splits/FOLD/dev.csv",
    "test_data_path": "../../data/derivations/all/splits/FOLD/test.csv",
    "model": {
        // This name needs to match the name that you used to register your model, with
        // the call to `@Model.register()`.
        #"type": "numerical_classifier_ordinal_simple",
        #"type": "regressor",
        "type": "classifier",
        "feat_type": feat_type,
        "feedforward": {
            "input_dim": 55,
            "num_layers": 3,
            "hidden_dims": 64,
            "activations": "relu",
            "dropout": 0.0
            },
        "embedder": {
        "token_embedders": {
                "tokens": {
                    "type": "pretrained_transformer",
                    "model_name": bert_model,
                    "gradient_checkpointing": true,
                    //split seqs that are too long into smaller segments:
                    #"max_length": bert_max_length
                    }
            }
        },
        "cat_feats_embedder": {
        "token_embedders": {
                "cat_feats": {
                    "type": "embedding",
                    "embedding_dim": 32,
                    }
            }
        },
        "encoder": {
            "type": "bert_pooler",
            "pretrained_model": bert_model,
            "dropout": 0.0,
        },
        "encoder_cat_feats": {
                        "type": "bag_of_embeddings", // sums the embeddings
                        "embedding_dim": 32
        },
        "metrics": {},
     },
    "data_loader": {
        // See http://docs.allennlp.org/master/api/data/dataloader/ for more info on acceptable
        // parameters here.
        "batch_size": 2,
        "shuffle": true
    },
    "trainer": {
        // See http://docs.allennlp.org/master/api/training/trainer/#gradientdescenttrainer-objects
        // for more info on acceptable parameters here.
        "optimizer": {"type": "adam", "lr": 2e-5},
        "num_epochs": 3,
        "patience":1,
        "validation_metric": "-mae"
    }
    // There are a few other optional parameters that can go at the top level, e.g., to configure
    // vocabulary behavior, to use a separate dataset reader for validation data, or other things.
    // See http://docs.allennlp.org/master/api/commands/train/#from_partial_objects for more info
    // on acceptable parameters.
}
