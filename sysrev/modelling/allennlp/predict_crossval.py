import os
import sys
import tarfile

from allennlp.commands import main

from sysrev.dataset_construction.util.util import load_json

config = load_json("configs/predict_crossval.json")
archive_file = config["archive_file"]
input_file = config["input_file"]
output_file = config["output_file"]

predictor = config["predictor"]
if predictor not in {"classifier_predictor", "classifier_multilabel_predictor", "attentive_classifier_predictor",
                     "regressor_predictor", "hierarchical_interactive_predictor"}:
    raise ValueError(f"Predictor {predictor} not supported.")

n_folds = 10
for fold_n in range(0, n_folds):
    # Update the fold number in the generic file paths.
    archive_file.replace("_k", f"_{fold_n}")
    input_file.replace("/k/", f"/{fold_n}/")

    # Make model weights available for prediction.
    tar = tarfile.open(f"{archive_file}/model.tar.gz", "r:gz")
    tar.extract("weights.th", path=archive_file)
    tar.close()
    os.rename(f'{archive_file}weights.th', f'{archive_file}best.th')

    cuda_device = -1

    # Assemble the command into sys.argv
    sys.argv = [
        "allennlp",
        "predict",
        archive_file,
        input_file,
        "--cuda-device", str(cuda_device),
        "--predictor", predictor,
        "--use-dataset-reader",
        "--include-package", "my_project",
        "--output-file", output_file,
    ]

    main()

    # Clean up.
    os.remove(f'{archive_file}best.th')
