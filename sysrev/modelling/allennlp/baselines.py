import csv
from collections import Counter, defaultdict

import numpy as np
from sklearn.metrics import accuracy_score, f1_score, mean_absolute_error, recall_score, precision_score
from sklearn.preprocessing import MultiLabelBinarizer

from sysrev.dataset_construction.util.util import load_json
from sysrev.modelling.allennlp.util import report_metrics, label_to_idx


def get_labels(f, binary=False, binary_type="high-vs-rest", label_type="Grade"):
    """
    :param f: ground truth file with labels
    :param binary: whether to binarise the labels
    :param binary_type: what labels to group when binarising. 'high-vs-rest' or 'high-mod-vs-rest'
    :param label_type: 'Grade' or 'Num-reason-types'
    :return: list of labels
    """

    labels = []

    with open(f) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            if label_type == "Num-reason-types":
                label = row["Reasons"]
            else:
                label = row[label_type]
            if label_type == "Grade" and binary:
                if binary_type == "high-vs-rest":
                    _label = label if label == "high" else "not-high"
                elif binary_type == "high-mod-vs-rest":
                    _label = "high-mod" if label == "high" or label == "moderate" else "not-high"
                else:
                    raise NotImplementedError
                labels.append(_label)
            elif label_type == "Num-reason-types":
                labels.append(len(list(eval(label).keys())))
            else:
                if label_type == "Reasons":
                    labels.append(list(eval(label).keys()))
                else:
                    labels.append(label)

    return labels


def get_majority_class(f, binary, binary_type, label_type="Grade"):
    labels = get_labels(f, binary, binary_type, label_type=label_type)
    if label_type == "Reasons":
        labels = [str(label) for label in labels]
    return Counter(labels).most_common(1)[0][0]


def majority_classifier(generic_path, n_folds=10, part="dev", binary=False, binary_type="high-vs-rest",
                        f1_average="macro", label_type="Grade", label_reason=None):
    metrics = defaultdict(list)

    for i in range(n_folds):
        path = generic_path.replace("FOLD", str(i))
        train_f = f"{path}train.csv"
        label = get_majority_class(train_f, binary, binary_type, label_type=label_type)
        if label_type == "Reasons":
            label = eval(label)
        dev_f = f"{path}{part}.csv"
        y_true = get_labels(dev_f, binary, binary_type, label_type=label_type)
        y_pred = [label] * len(y_true)
        if label_type == "Grade" or label_type == "Num-reason-types":
            acc = accuracy_score(y_true, y_pred)
            if binary:
                if binary_type == "high-vs-rest":
                    labels = ["high", "not-high"]
                elif binary_type == "high-mod-vs-rest":
                    labels = ["high-mod", "not-high"]
                else:
                    raise NotImplementedError
            else:
                if label_type == "Num-reason-types":
                    labels = list(range(3))
                else:
                    labels = ["high", "moderate", "low", "very low"]
            f1_macro = f1_score(y_true, y_pred, average=f1_average, labels=labels)
            precision_macro = precision_score(y_true, y_pred, average=f1_average, labels=labels)
            recall_macro = recall_score(y_true, y_pred, average=f1_average, labels=labels)
            if not binary and (label_type == "Grade" or label_type == "Num-reason-types"):
                mae = mean_absolute_error(
                    [label_to_idx(y, num_reasons=label_type == "Num-reason-types") for y in y_true],
                    [label_to_idx(y, num_reasons=label_type == "Num-reason-types") for y in y_pred])
                metrics["mae"].append(mae)
        elif label_type == "Reasons" and label_reason is None:
            mlb = MultiLabelBinarizer()
            mlb_fitted = mlb.fit(y_true)
            y_true = mlb_fitted.transform(y_true)
            y_pred = mlb_fitted.transform(y_pred)
            acc = accuracy_score(y_true, y_pred)
            f1_macro = f1_score(y_true, y_pred, average=f1_average)
            precision_macro = precision_score(y_true, y_pred, average=f1_average)
            recall_macro = recall_score(y_true, y_pred, average=f1_average)
        elif label_type == "Reasons" and label_reason in {"risk of bias", "imprecision", "inconsistency",
                                                          "indirectness", "publication bias"}:
            labels = [label_reason, "no"]
            _y_true = []
            for y in y_true:
                if label_reason in y:
                    _y_true.append(label_reason)
                else:
                    _y_true.append("no")
            _y_pred = []
            for y in y_pred:
                if label_reason in y:
                    _y_pred.append(label_reason)
                else:
                    _y_pred.append("no")
            acc = accuracy_score(_y_true, _y_pred)
            f1_macro = f1_score(_y_true, _y_pred, average=f1_average, pos_label=label_reason)
            precision_macro = precision_score(_y_true, _y_pred, average=f1_average, pos_label=label_reason)
            recall_macro = recall_score(_y_true, _y_pred, average=f1_average, pos_label=label_reason)
        else:
            raise ValueError

        metrics["acc"].append(acc)
        metrics["f1_macro"].append(f1_macro)
        metrics["precision_macro"].append(precision_macro)
        metrics["recall_macro"].append(recall_macro)

    print(f"\nMajority classifier results for part: {part}")
    report_metrics(metrics, f1_average, labels=labels if (label_type == "Grade" or label_type == "Num-reason-types" or (
            label_type == "Reasons" and label_reason is not None)) else mlb_fitted.classes_)


def random_classifier(generic_path, n_folds=10, part="dev", binary=False, binary_type="high-vs-rest",
                      f1_average="macro", label_type="Grade"):
    metrics = defaultdict(list)

    for i in range(n_folds):
        path = generic_path.replace("FOLD", str(i))

        dev_f = f"{path}{part}.csv"
        y_true = get_labels(dev_f, binary, binary_type, label_type=label_type)
        # y_true_uniq = set(y_true)
        # y_true_to_idx = dict(zip(y_true_uniq, range(len(y_true_uniq))))
        # y_pred = np.random.randint(0, len(y_true_uniq), len(y_true))
        if binary:
            if binary_type == "high-vs-rest":
                labels = ["high", "not-high"]
            elif binary_type == "high-mod-vs-rest":
                labels = ["high-mod", "not-high"]
            else:
                raise NotImplementedError
            n_labels = 2
        else:
            if label_type == "Grade":
                labels = ["high", "moderate", "low", "very low"]
                n_labels = len(labels)
            elif label_type == "Num-reason-types":
                labels = list(range(3))
                n_labels = len(labels)
            else:
                mlb = MultiLabelBinarizer()
                mlb_fitted = mlb.fit(y_true)
                n_labels = len(mlb_fitted.classes_)

        if label_type == "Grade" or label_type == "Num-reason-types":
            y_pred = np.random.randint(0, n_labels, len(y_true))
            y_true_idx = [label_to_idx(y, binary, binary_type, num_reasons=label_type == "Num-reason-types") for y in
                          y_true]
            acc = accuracy_score(y_true_idx, y_pred)
            f1_macro = f1_score(y_true_idx, y_pred, average=f1_average)
            precision_macro = precision_score(y_true_idx, y_pred, average=f1_average)
            precision_macro_total = precision_score(y_true_idx, y_pred, average="macro")
            recall_macro = recall_score(y_true_idx, y_pred, average=f1_average)
            recall_macro_total = recall_score(y_true_idx, y_pred, average="macro")

            if not binary:
                mae = mean_absolute_error(y_true_idx, y_pred)
                metrics["mae"].append(mae)
        elif label_type == "Reasons":
            y_set = list(set([str(y) for y in y_true]))
            y_pred = np.random.randint(0, len(y_set), len(y_true))
            y_pred = [y_set[i] for i in y_pred]
            y_pred = [eval(y) for y in y_pred]

            y_true = mlb_fitted.transform(y_true)
            y_pred = mlb_fitted.transform(y_pred)
            acc = accuracy_score(y_true, y_pred)
            f1_macro = f1_score(y_true, y_pred, average=f1_average)
            precision_macro = precision_score(y_true, y_pred, average=f1_average)
            precision_macro_total = precision_score(y_true, y_pred, average="macro")
            recall_macro = recall_score(y_true, y_pred, average=f1_average)
            recall_macro_total = recall_score(y_true, y_pred, average="macro")
        else:
            raise ValueError

        metrics["acc"].append(acc)
        metrics["f1_macro"].append(f1_macro)
        metrics["precision_macro"].append(precision_macro)
        metrics["precision_macro_total"].append(precision_macro_total)
        metrics["recall_macro"].append(recall_macro)
        metrics["recall_macro_total"].append(recall_macro_total)

    print(f"\nRandom classifier results for part: {part}")
    report_metrics(metrics, f1_average, labels=labels if (
            label_type == "Grade" or label_type == "Num-reason-types") else mlb_fitted.classes_)


config = load_json("configs/baselines.json")
baselines_generic_path = config["baselines_generic_path"]

print("majority,overall")
majority_classifier(baselines_generic_path, part="test")
print()

print("majority,per class")
majority_classifier(baselines_generic_path, part="test", f1_average=None)
print()

print("majority,binary:high-vs-rest,overall")
majority_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-vs-rest")
print()

print("majority,binary:high-vs-rest,per class")
majority_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-vs-rest", f1_average=None)
print()

print("majority,binary:high-mod-vs-rest,overall")
majority_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-mod-vs-rest")
print()

print("majority,binary:high-mod-vs-rest,per class")
majority_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-mod-vs-rest", f1_average=None)
print()

print("majority,reasons,per class")
majority_classifier(baselines_generic_path, part="test", label_type="Reasons", f1_average=None)
print()

print("majority,rob,per class")
majority_classifier(baselines_generic_path, part="test", label_type="Reasons", label_reason="risk of bias",
                    f1_average="binary")
print()

print("majority,imp,per class")
majority_classifier(baselines_generic_path, part="test", label_type="Reasons", label_reason="imprecision",
                    f1_average="binary")
print()

print("majority,inc,per class")
majority_classifier(baselines_generic_path, part="test", label_type="Reasons", label_reason="inconsistency",
                    f1_average="binary")
print()

print("majority,ind,per class")
majority_classifier(baselines_generic_path, part="test", label_type="Reasons", label_reason="indirectness",
                    f1_average="binary")
print()

print("majority,pub,per class")
majority_classifier(baselines_generic_path, part="test", label_type="Reasons", label_reason="publication bias",
                    f1_average="binary")
print()

print("random,overall")
random_classifier(baselines_generic_path, part="test")
print()

print("random,per class")
random_classifier(baselines_generic_path, part="test", f1_average=None)
print()

print("random,binary:high-vs-rest,overall")
random_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-vs-rest", n_folds=1)
print()

print("random,binary:high-vs-rest,per class")
random_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-vs-rest", f1_average=None)
print()

print("random,binary:high-mod-vs-rest,overall")
random_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-mod-vs-rest", n_folds=1)
print()

print("random,binary:high-mod-vs-rest,per class")
random_classifier(baselines_generic_path, part="test", binary=True, binary_type="high-mod-vs-rest", f1_average=None)
print()

print("random,reasons,overall")
random_classifier(baselines_generic_path, part="test", label_type="Reasons")
print()

print("random,reasons,per class")
random_classifier(baselines_generic_path, part="test", label_type="Reasons", f1_average=None)
print()
