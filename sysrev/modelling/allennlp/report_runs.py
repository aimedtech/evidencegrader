from collections import defaultdict

from sysrev.dataset_construction.util.util import load_json
from sysrev.modelling.allennlp.util import accumulate_metrics, report_metrics

config = load_json("configs/report.json")
serialization_path = config["serialization_path"]
exp_name = config["exp_name"]
serialization_par_dir = f'{serialization_path}{exp_name}/'
n_folds = config["n_folds"]
metrics_f = config["metrics_file"]
metrics = defaultdict(list)

for k in range(n_folds):
    serialization_dir = f"{serialization_par_dir}{exp_name}_{k}"
    accumulate_metrics(metrics, serialization_dir, fn=metrics_f, metric_prefix_name=False)

report_metrics(metrics)
