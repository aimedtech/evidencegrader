import os

import torch
from allennlp.data import DataLoader
from allennlp.data.data_loaders import MultiProcessDataLoader
from allennlp.models import load_archive
from torch import nn, optim

from sysrev.modelling.allennlp.my_project.dataset_reader import *
from sysrev.modelling.allennlp.my_project.loss import _ECELoss

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def get_data_loader(data_dir, archive, part="dev"):
    # prepare data:
    reader = archive.dataset_reader
    vocab = archive.model.vocab
    loader = build_data_loaders(reader, data_dir + f"{part}.csv")
    loader.index_with(vocab)

    for i in loader:
        yield i


class TemperatureScaler(nn.Module):
    def __init__(self, model):
        super(TemperatureScaler, self).__init__()
        self.model = model
        self.temperature = nn.Parameter(torch.ones(1) * 1.5)

    def forward(self, input):
        output = self.model(input["feat"], input["text_list"], input["cat_feats_list"], input["label"])

        scaled_logits = self.temperature_scale(output["logits"])
        scaled_probs = torch.nn.functional.softmax(scaled_logits, dim=-1)

        return scaled_logits, scaled_probs

    def temperature_scale(self, logits):
        """
        Perform temperature scaling on logits
        """
        # Expand temperature to match the size of logits
        temperature = self.temperature.unsqueeze(1).expand(logits.size(0), logits.size(1))
        return logits / temperature

    # This function probably should live outside of this class, but whatever
    def set_temperature(self, valid_loader):
        """
        Tune the tempearature of the model (using the validation set).
        We're going to set it to optimize NLL.
        valid_loader (DataLoader): validation set loader
        """
        self.cuda()
        nll_criterion = nn.CrossEntropyLoss().cuda()
        ece_criterion = _ECELoss().cuda()

        # First: collect all the logits and labels for the validation set
        logits_list = []
        labels_list = []
        with torch.no_grad():
            for input in tqdm(valid_loader):
                logits, probs = self(input)
                logits_list.append(logits)
                labels_list.append(input["label"])
            logits = torch.cat(logits_list)  # .cuda()
            labels = torch.cat(labels_list)  # .cuda()

        # Calculate NLL and ECE before temperature scaling
        before_temperature_nll = nll_criterion(logits, labels).item()
        before_temperature_ece = ece_criterion(logits, labels).item()
        print('Before temperature - NLL: %.3f, ECE: %.3f' % (before_temperature_nll, before_temperature_ece))

        # Next: optimize the temperature w.r.t. NLL
        optimizer = optim.LBFGS([self.temperature], lr=0.01, max_iter=50)

        def eval():
            optimizer.zero_grad()
            loss = nll_criterion(self.temperature_scale(logits), labels)
            loss.backward()
            return loss

        optimizer.step(eval)

        # Calculate NLL and ECE after temperature scaling
        after_temperature_nll = nll_criterion(self.temperature_scale(logits), labels).item()
        after_temperature_ece = ece_criterion(self.temperature_scale(logits), labels).item()
        print('Optimal temperature: %.3f' % self.temperature.item())
        print('After temperature - NLL: %.3f, ECE: %.3f' % (after_temperature_nll, after_temperature_ece))

        return self


def build_data_loaders(reader, data_path: str) -> DataLoader:
    train_loader = MultiProcessDataLoader(reader, data_path, batch_size=16, shuffle=False, cuda_device=0)

    return train_loader


def tune_temperature(data_dir, serialization_dir, output_name='model_with_temperature.pth'):
    archive = load_archive(serialization_dir + "model.tar.gz")
    valid_loader = get_data_loader(data_dir=data_dir, archive=archive)

    # Pre-trained ML model
    orig_model = archive.model
    orig_model = orig_model.to(device)

    model = TemperatureScaler(orig_model)
    model = model.to(device)

    # Tune the model temperature, and save the results
    model.set_temperature(valid_loader)
    output_model_f = os.path.join(serialization_dir, output_name)
    torch.save(model.state_dict(), output_model_f)
    print('Temperature scaled model saved to %s' % output_model_f)

    return model


def predict(data_dir, serialization_dir, model):
    # obtain data-related stuff from the original model's archive
    archive = load_archive(serialization_dir + "model.tar.gz")
    test_loader = get_data_loader(data_dir=data_dir, archive=archive, part="test")

    logits_list = []
    final = []
    with torch.no_grad():
        for input in tqdm(test_loader):
            logits, probs = model(input)
            logits_list.append(logits)
            label_vocab = model.model.vocab.get_index_to_token_vocabulary('labels')
            predicted = [label_vocab[i.item()] for i in torch.argmax(probs, 1)]
            label = [label_vocab[i.item()] for i in input["label"]]
            token_vocab = model.model.vocab.get_index_to_token_vocabulary('tokens')
            topics = []
            for i in input["cat_feats_list"]["cat_feats"]["tokens"]:  # over batch elements
                topics.append([token_vocab[topic_id.item()] for topic_id in i[2]])
            for c in range(len(predicted)):
                final.append([predicted[c], label[c], probs[c].tolist(), topics[c]])

    with open(f"{serialization_dir}/test_predictions_extended_ts", "w") as fh_out:
        writer = csv.writer(fh_out, delimiter="\t")
        for i in final:
            writer.writerow(i)


config = load_json("configs/temperature_scaling.json")
data_dir = config["data_dir"]
serialization_dir = config["serialization_dir"]
output_name = config["output_name"]

for generic_path in [config["generic_path_rob"],
                     config["generic_path_imp"],
                     config["generic_path_ind"],
                     config["generic_path_inc"],
                     config["generic_path_pub"]
                     ]:
    for fold_n in range(10):
        data_dir = f"{data_dir}{fold_n}/"
        serialization_dir = serialization_dir.replace("_k", f"_{fold_n}")
        output_model_f = os.path.join(serialization_dir, output_name)
        model = tune_temperature(data_dir, serialization_dir, output_name=output_name)
        predict(data_dir, serialization_dir, model)
