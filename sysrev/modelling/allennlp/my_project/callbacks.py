"""
#from allennlp.data.dataloader import TensorDict
from allennlp.data.data_loaders import TensorDict
from comet_ml import Experiment, ExistingExperiment

from allennlp.training.trainer import BatchCallback, EpochCallback
from typing import Any, Dict, Iterator, List, Optional, Tuple, Union
from allennlp.common import Registrable, FromParams
from allennlp.common.params import Params


class CometMLExperiment(FromParams):
    def __init__(self, api_key, project_name):
        if "COMET_EXPERIMENT_KEY" in os.environ:
            self.obj = ExistingExperiment(
                api_key=api_key,
                project_name=project_name,
                previous_experiment=os.environ["COMET_EXPERIMENT_KEY"],
            )
        else:
            self.obj = Experiment(api_key=api_key, project_name=project_name)
            os.environ["COMET_EXPERIMENT_KEY"] = self.obj.get_key()


@BatchCallback.register("prediction_logger")
class PredictionLogger(BatchCallback):
    def __init__(self, experiment: CometMLExperiment):
        super().__init__()
        self.experiment = experiment.obj

    def __call__(
            self,
            trainer: "GradientDescentTrainer",
            batch_inputs: List[List[TensorDict]],
            batch_outputs: List[Dict[str, Any]],
            batch_metrics: Dict[str, Any],
            epoch: int,
            batch_number: int,
            is_training: bool,
            is_master: bool,
    ) -> None:
        if not is_master:
            return
        if not is_training:
            return
        with self.experiment.train():
            self.experiment.log_metrics(trainer.model.get_metrics())


@EpochCallback.register("performance_logger")
class PerformanceLogger(EpochCallback):
    def __init__(self, experiment: CometMLExperiment):
        super().__init__()
        self.experiment = experiment.obj

    def __call__(
        self,
        trainer: "GradientDescentTrainer",
        metrics: Dict[str, Any],
        epoch: int,
        is_master: bool,
    ) -> None:
        if not is_master:
            return
        if epoch == -1:
            params = Params.from_file(f"{trainer._serialization_dir}/config.json").as_flat_dict()
            self.experiment.log_parameters(params)
            return
        self.experiment.log_metrics(metrics)
"""
