import csv
import logging
import pickle
import re
import sys
from collections import defaultdict
from pathlib import Path
from typing import Dict, Iterable, Optional, List, Union

import numpy as np
from allennlp.data import DatasetReader, Instance
from allennlp.data.fields import LabelField, TextField, ArrayField, ListField, MultiLabelField, MetadataField
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from allennlp.data.tokenizers import Tokenizer, WhitespaceTokenizer
from bs4 import BeautifulSoup
from sklearn import preprocessing
from tqdm import tqdm

from sysrev.dataset_construction.parse.statistics_studies import get_relevant_studies
from sysrev.dataset_construction.util.util import load_json, save_json
from sysrev.modelling.allennlp.configs.constants import NUMERICAL_FEATS, NUMERICAL_FEATS_PER_OUTCOME, CATEGORICAL_FEATS, \
    TEXTUAL_FEATS
from sysrev.modelling.allennlp.util import label_to_idx

logger = logging.getLogger(__name__)


def fit_scaler(serialization_dir,
               training_path,
               xml_dir_path=None,
               json_dataset_path=None,
               json_dataset_invalid_path=None,
               ablation_filter=None):
    """
    Fit a minmax scaler on training data of one CV iteration, and save it to serialization_dir.
    """
    if training_path.endswith("dataset_invalid.csv"):
        curr_training_path = training_path
    else:
        # get the training dir for the correct CV iteration
        cv_n = serialization_dir.rsplit("_", 1)[-1]
        try:
            cv_n_int = int(cv_n)
        except ValueError:
            # back off to 0th CV iteration
            cv_n_int = 0
        curr_training_path = training_path.replace("FOLD", str(cv_n_int)) if "FOLD" in training_path else training_path

    if xml_dir_path is not None:
        doi_to_studies, doi_to_studies_per_outcome = get_studies(xml_dir_path, json_dataset_path,
                                                                 json_dataset_invalid_path)

    # get all numerical features from the training set as a np array
    numericals_all = []
    with open(curr_training_path) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            if xml_dir_path is not None:
                # try to obtain studies per outcome; if None, skip
                studies_per_outcome, vals_per_outcome = get_relevant_studies(doi_to_studies_per_outcome,
                                                                             row["doi"],
                                                                             row["sof_title"],
                                                                             row["O"],
                                                                             row["n_studies"],
                                                                             row["type_effect"],
                                                                             row["relative_effect"])

            numericals_all.append(CsvReader.get_numericals(row, scaler=None, ablation_filter=ablation_filter,
                                                           row_per_outcome=vals_per_outcome if xml_dir_path is not None else None))
    numericals_all = np.array(numericals_all)

    # fit the scaler
    # scaler = preprocessing.MinMaxScaler()
    scaler = preprocessing.StandardScaler()
    scaler.fit(numericals_all)

    # save
    pickle.dump(scaler, open(f"{serialization_dir}/scaler.pickle", "wb"))
    logger.info(f"Fitted a scaler as {serialization_dir}/scaler.pickle.")


@DatasetReader.register('classification-simple')
class CsvReader(DatasetReader):
    """
    Reads input fields like P, I, C, O, Grade, Reasons etc. from a csv file
    """

    def __init__(self,
                 tokenizer: Tokenizer = None,
                 tokenizer_cat_feats: Tokenizer = None,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 cat_feats_indexers: Dict[str, TokenIndexer] = None,
                 max_tokens: int = None,
                 feat_type: Dict[str, bool] = None,
                 scaler_training_path: str = None,
                 ablated_feat: str = None,
                 xml_dir_path: str = None,
                 json_dataset_path: str = None,
                 json_dataset_invalid_path: str = None,
                 n_numerical: int = None,
                 **kwargs):
        super().__init__(**kwargs)
        self.tokenizer = tokenizer or WhitespaceTokenizer()
        self.tokenizer_cat_feats = tokenizer_cat_feats or WhitespaceTokenizer()
        self.token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}
        self.cat_feats_indexers = cat_feats_indexers or {'cat_feats': SingleIdTokenIndexer()}
        self.max_tokens = max_tokens
        # use only numeric features if not otherwise specified:
        self.feat_type = feat_type or {"num": True}
        self.scaler_training_path = scaler_training_path
        self.ablated_feat = ablated_feat
        # to filter out only the features that are not ablated:
        self.ablation_filter_fun = (lambda x: x != self.ablated_feat) if self.ablated_feat is not None else None
        # number of numerical features to use, defaults to 13 or 55
        self.n_numerical = n_numerical
        # self.n_numerical = 13
        self.xml_dir_path = xml_dir_path
        self.json_dataset_path = json_dataset_path
        self.json_dataset_invalid_path = json_dataset_invalid_path

        # scaler for numeric features
        self.scaler = None
        if self.scaler_training_path and self.feat_type["num"]:
            scaler_path = Path(f"{self.serialization_dir}/scaler.pickle")
            if not scaler_path.exists():
                fit_scaler(self.serialization_dir, training_path=self.scaler_training_path,
                           xml_dir_path=self.xml_dir_path, json_dataset_path=self.json_dataset_path,
                           json_dataset_invalid_path=self.json_dataset_invalid_path,
                           ablation_filter=self.ablation_filter)
            self.scaler = pickle.load(open(scaler_path, "rb"))

    @staticmethod
    def get_numericals(row, scaler=None, ablation_filter=None, row_per_outcome=None, n_numerical=None):
        # find if row represents extended inputs (55 feats given as csv input, e.g. from CogTale manually constructed data)
        if "val_chi2" in row:
            numerical_feats = NUMERICAL_FEATS + NUMERICAL_FEATS_PER_OUTCOME
        else:
            numerical_feats = NUMERICAL_FEATS
        numerical_feats_per_outcome = NUMERICAL_FEATS_PER_OUTCOME

        numerical_feats = ablation_filter(numerical_feats) if ablation_filter is not None else numerical_feats

        if row_per_outcome is not None:
            numerical_feats_per_outcome = ablation_filter(
                numerical_feats_per_outcome) if ablation_filter is not None else numerical_feats_per_outcome

        numericals = []
        for feat in numerical_feats:
            if feat in row:
                numericals.append(row[feat])
            else:
                numericals.append(0.)

        if n_numerical is None:
            for feat in numerical_feats_per_outcome:
                if feat not in numerical_feats:
                    if row_per_outcome is not None and feat in row_per_outcome:
                        val = row_per_outcome[feat]
                        if val is None or np.isnan(val):
                            val = 0.
                        numericals.append(val)
                    else:
                        numericals.append(0.)
        numericals_float = []

        for v in numericals:
            try:
                numericals_float.append(float(v) if v is not None else 0.)
            except ValueError:
                numericals_float.append(0.)

        return scaler.transform([numericals_float]).squeeze() if scaler is not None else numericals_float

    def fields_to_instance(self, text_list_field: ListField[TextField] = None, feat_field: ArrayField = None,
                           cat_feats_list_field: ListField[TextField] = None,
                           label_field: Union[LabelField, MultiLabelField] = None,
                           metadata_field: MetadataField = None) -> Instance:
        fields = {}
        if text_list_field is not None:
            fields["text_list"] = text_list_field
        if feat_field is not None:
            fields["feat"] = feat_field
        if cat_feats_list_field is not None:
            fields["cat_feats_list"] = cat_feats_list_field
        if label_field is not None:
            fields["label"] = label_field
        if metadata_field is not None:
            fields["metadata"] = metadata_field

        return Instance(fields)

    def metadata_to_field(self, metadata: dict) -> MetadataField:
        return MetadataField(metadata)

    def text_to_field(self, text: str) -> TextField:
        tokens = self.tokenizer.tokenize(text)
        if self.max_tokens:
            tokens = tokens[:self.max_tokens]

        return TextField(tokens, self.token_indexers)

    def cat_feat_to_field(self, text: str) -> TextField:
        tokens = self.tokenizer_cat_feats.tokenize(text)
        if self.max_tokens:
            tokens = tokens[:self.max_tokens]

        return TextField(tokens, self.cat_feats_indexers)

    def feat_to_field(self, feat: np.ndarray) -> ArrayField:
        return ArrayField(feat, dtype=np.dtype(np.float32))

    def label_to_field(self, label: str = None, namespace: str = None, skip_indexing=False) -> Optional[LabelField]:
        if label is not None:
            if namespace is not None:
                return LabelField(label, namespace, skip_indexing=skip_indexing)
            else:
                return LabelField(label, skip_indexing=skip_indexing)
        else:
            return None

    def multiple_labels_to_field(self, labels: List[str] = None, namespace: str = "labels") -> Optional[
        MultiLabelField]:
        # can return an empty list of labels
        return MultiLabelField(labels, namespace)


    def transform(self, label):
        """
        Convenience method that gets superseded by inheriting classes.
        """
        return label

    def get_label(self, row):
        grade = row["Grade"]
        grade = self.transform(grade)

        label_field = self.label_to_field(grade)
        return label_field

    def ablation_filter(self, feats):
        return list(filter(self.ablation_filter_fun, feats))

    def get_metadata(self, row):
        return self.metadata_to_field({"doi": row["doi"]})

    def get_categoricals(self, row):
        categoricals = self.ablation_filter(CATEGORICAL_FEATS)
        cat_feats_list = []
        for feat_type in categoricals:
            feat = row[feat_type]
            if feat_type == "topics":
                if not isinstance(feat, list):
                    feat = eval(feat)
                # deal with the list of topics
                list_of_topics = [i.replace(" ", "_") for i in feat]
                feat = " ".join(list_of_topics)
            cat_feats_list.append(self.cat_feat_to_field(feat))

        return ListField(cat_feats_list)

    def get_textuals(self, row):
        textual_feats = self.ablation_filter(TEXTUAL_FEATS)
        texts = [row[feat] for feat in textual_feats]
        # population (P), intervention (I), comparison (C), outcome (O) features
        picos = []
        # picos.append(" ".join(row["P"].split()[:20]))
        # picos.append(" ".join(row["I"].split()[:20]))
        # picos.append(" ".join(row["C"].split()[:20]))
        picos.append(" ".join(row["O"].split()[:20]))
        picos_sep = "[SEP]".join(picos)
        text_fields = [self.text_to_field(picos_sep + "[SEP]" + text) for text in texts]

        return ListField(text_fields)

    def get_fields(self, row):

        # numerical features:
        feat_field = None
        if self.feat_type["num"]:
            numericals_float = self.get_numericals(row, scaler=self.scaler, ablation_filter=self.ablation_filter,
                                                   n_numerical=self.n_numerical)
            feat_field = self.feat_to_field(np.array(numericals_float))

        # textual features:
        text_list_field = None
        if self.feat_type["text"]:
            text_list_field = self.get_textuals(row)

        # categorical (textual) features:
        cat_feats_list_field = None
        if self.feat_type["cat"]:
            cat_feats_list_field = self.get_categoricals(row)

        # metadata (e.g. doi)
        # metadata_field = self.get_metadata(row)

        return feat_field, text_list_field, cat_feats_list_field  #, metadata_field

    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                # get input fields
                # feat_field, text_list_field, cat_feats_list_field, metadata_field = self.get_fields(row)
                feat_field, text_list_field, cat_feats_list_field = self.get_fields(row)

                # label:
                label_field = self.get_label(row)

                yield self.fields_to_instance(text_list_field=text_list_field, feat_field=feat_field,
                                              cat_feats_list_field=cat_feats_list_field,
                                              label_field=label_field)  #,metadata_field=metadata_field)


@DatasetReader.register('bi-classification-simple')
class BiCsvReader(CsvReader):
    def transform(self, label, label_type="grade"):
        """
        Binarise the quality labels to high/not-high.

        :param label: textual label
        :param label_type: "grade" or "reason"
        :return: mapped label where appropriate
        """
        if label_type == "grade":
            if label_to_idx(label) != 3:
                label = "not-high"

        return label


@DatasetReader.register('bi-classification-simple-2')
class BiCsvReader2(CsvReader):
    def transform(self, label, label_type="grade"):
        """
        Binarise the quality labels to high-mod/not-high.

        :param label: textual label
        :param label_type: "grade" or "reason"
        :return: mapped label where appropriate
        """
        if label_type == "grade":
            if label_to_idx(label) not in {2, 3}:
                label = "not-high"
            else:
                label = "high-mod"
        return label


@DatasetReader.register('bi-classification-simple-2-from-bi-reasons')
class BiCsvReader2FromBiReasons(BiCsvReader2):
    def get_numericals(self, row, scaler=None, ablation_filter=None, row_per_outcome=None):
        # numerical_feats = ["logit_rob", "logit_imp", "logit_inc", "logit_ind", "logit_pub"]
        numericals = []
        # for feat in numerical_feats:
        #    if feat in row:
        for feat, value in row.items():
            if feat != "Grade":
                numericals.append(value)

        numericals_float = []
        for v in numericals:
            try:
                numericals_float.append(float(v) if v is not None else 0.)
            except ValueError:
                numericals_float.append(0.)

        return numericals_float


@DatasetReader.register('classification-simple-from-bi-reasons')
class CsvReaderFromBiReasons(CsvReader):
    def get_numericals(self, row, scaler=None, ablation_filter=None, row_per_outcome=None):
        # numerical_feats = ["logit_rob", "logit_imp", "logit_inc", "logit_ind", "logit_pub"]
        numericals = []
        # for feat in numerical_feats:
        #    if feat in row:
        for feat, value in row.items():
            if feat != "Grade":
                numericals.append(value)

        numericals_float = []
        for v in numericals:
            try:
                numericals_float.append(float(v) if v is not None else 0.)
            except ValueError:
                numericals_float.append(0.)

        return numericals_float


@DatasetReader.register('bi-classification-simple-reason')
class BiCsvReaderReason(CsvReader):
    def __init__(self, reason_type=None, **kwargs):
        if reason_type is None:
            sys.exit("Specify the reason type.")
        self.reason_type = reason_type
        super().__init__(**kwargs)

    def get_label(self, row):
        reasons_dict = eval(row["Reasons"])
        label = "other"
        for reason in reasons_dict.keys():
            if self.reason_type in reason.replace(" ", "-"):
                label = self.reason_type
        label_field = self.label_to_field(label)

        return label_field


@DatasetReader.register('classification-simple-reasons')
class CsvReaderReasons(CsvReader):
    def get_label(self, row):
        reasons_dict = eval(row["Reasons"])
        reasons = []
        for reason in reasons_dict.keys():
            reasons.append(reason.replace(" ", "-"))
        label_field = self.multiple_labels_to_field(reasons)
        return label_field


@DatasetReader.register('grade-reasons')
class CsvReaderGradeAndReasons(CsvReader):
    def fields_to_instance(self, text_list_field: ListField[TextField] = None, feat_field: ArrayField = None,
                           cat_feats_list_field: ListField[TextField] = None,
                           label_field: Union[LabelField, MultiLabelField] = None,
                           top_label_field: Union[LabelField, MultiLabelField] = None) -> Instance:
        fields = {}
        if text_list_field is not None:
            fields["text_list"] = text_list_field
        if feat_field is not None:
            fields["feat"] = feat_field
        if cat_feats_list_field is not None:
            fields["cat_feats_list"] = cat_feats_list_field
        if label_field is not None:
            fields["label"] = label_field
        if top_label_field is not None:
            fields["top_label"] = top_label_field

        return Instance(fields)

    def get_label(self, row, restricted=False):
        """
        :param restricted: consider only RoB and imprecision (two most freq) labels
        """
        reasons_dict = eval(row["Reasons"])
        reasons = []
        for reason in reasons_dict.keys():
            if restricted:
                if reason in {"risk of bias", "imprecision"}:
                    reasons.append(reason.replace(" ", "-"))
            else:
                reasons.append(reason.replace(" ", "-"))
        # use dummy label for no downgrading (in the case of High GRADE score)
        # if not reasons:
        #    reasons = ["NA"]

        label_field = self.multiple_labels_to_field(reasons)
        return label_field

    def get_top_label(self, row):
        grade = row["Grade"]
        grade = self.transform(grade)

        label_field = self.label_to_field(grade, namespace="top_labels")
        return label_field

    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                # get input fields
                # feat_field, text_list_field, cat_feats_list_field, metadata_field = self.get_fields(row)
                feat_field, text_list_field, cat_feats_list_field = self.get_fields(row)

                # label (reasons)
                label_field = self.get_label(row)
                # label (GRADE):
                top_label_field = self.get_top_label(row)

                yield self.fields_to_instance(text_list_field=text_list_field, feat_field=feat_field,
                                              cat_feats_list_field=cat_feats_list_field, label_field=label_field,
                                              top_label_field=top_label_field)


@DatasetReader.register('bi-grade-reasons-2')
class BiCsvReader2GradeAndReasons(CsvReaderGradeAndReasons, BiCsvReader2):
    pass


@DatasetReader.register('ordinal-regression')
class CsvReaderOrdinal(CsvReader):
    def label_to_field(self, label: str = None, namespace: str = None) -> Optional[ArrayField]:
        if label:
            return ArrayField(self.label_to_levels(label_to_idx(label)))
        else:
            return None

    def label_to_levels(self, label, num_classes=4):
        """
        Encode the grade label as a binary array:
        "very low" -> [0,0,0]
        "low" -> [1,0,0]
        "moderate" -> [1,1,0]
        "high" -> [1,1,1]
        """
        levels = [1] * label + [0] * (num_classes - 1 - label)

        return np.array(levels, dtype="float32")


@DatasetReader.register('ordinal-regression-simple')
class CsvReaderOrdinalSimple(CsvReaderOrdinal):
    def label_to_levels(self, label, num_classes=4):
        """
                Encode the grade label to lie between 0 and num_classes.

                "very low" -> 0.
                "low" -> 1.
                "moderate" -> 2.
                "high" -> 3.
        """
        return np.array([label], dtype="float32")


@DatasetReader.register('ordinal-regression-simple-num-reasons')
class CsvReaderOrdinalSimpleNumReasons(CsvReader):
    def get_label(self, row):
        reasons_dict = eval(row["Reasons"])
        n_reasons = sum(reasons_dict.values())

        label_field = self.label_to_array_field(n_reasons)  # , skip_indexing=True)
        return label_field

    def label_to_levels_array(self, label):
        return np.array([label], dtype="float32")

    def label_to_array_field(self, label: str = None, namespace: str = None) -> Optional[ArrayField]:
        if label is not None:
            return ArrayField(self.label_to_levels_array(label_to_idx(label, num_reasons=True)))
        else:
            return None


@DatasetReader.register('ordinal-regression-simple-num-reason-types')
class CsvReaderOrdinalSimpleNumReasonTypes(CsvReader):
    def get_label(self, row):
        reasons_dict = eval(row["Reasons"])
        n_reasons = len(reasons_dict.keys())

        label_field = self.label_to_array_field(n_reasons)  # , skip_indexing=True)
        return label_field

    def label_to_levels_array(self, label, num_classes=4):
        return np.array([label], dtype="float32")

    def label_to_array_field(self, label: str = None, namespace: str = None) -> Optional[ArrayField]:
        if label is not None:
            return ArrayField(self.label_to_levels_array(label_to_idx(label, num_reasons=True)))
        else:
            return None


@DatasetReader.register('ordinal-regression-text')
class CsvReaderOrdinalText(CsvReaderOrdinal):
    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                grade = row["Grade"]
                text = row["abstract_conclusion_txt"]
                # text = row["plain_language_summary_txt"]
                # text = row["authors_conclusions_txt"]
                # population = " ".join(row["P"].split()[:50])
                # intervention = " ".join(row["I"].split()[:50])
                # comparison = " ".join(row["C"].split()[:50])
                # outcome = " ".join(row["O"].split()[:50])
                # text_field = self.text_to_field(population + "[SEP]" + intervention + "[SEP]" + comparison + "[SEP]" + outcome)
                outcome = " ".join(row["O"].split()[:20])
                text_field = self.text_to_field(outcome + "[SEP]" + text)
                # text_field = self.text_to_field(text + "[SEP]" + outcome)
                label_field = self.label_to_field(grade)

                fields = {'text': text_field}
                if grade:
                    fields['label'] = label_field

                yield Instance(fields)

    def label_to_levels(self, label, num_classes=4):
        """
            Encode the grade label to lie between 0 and num_classes.

                "very low" -> 0.
                "low" -> 1.
                "moderate" -> 2.
                "high" -> 3.

        """
        # return np.array([label / (num_classes - 1)], dtype="float32")
        return np.array([label], dtype="float32")


@DatasetReader.register('ordinal-regression-text2')
class CsvReaderOrdinalText2(CsvReaderOrdinalText):
    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                grade = row["Grade"]
                text = row["abstract_conclusion_txt"]
                # text = row["plain_language_summary_txt"]
                # text = row["authors_conclusions_txt"]
                # population = " ".join(row["P"].split()[:50])
                # intervention = " ".join(row["I"].split()[:50])
                # comparison = " ".join(row["C"].split()[:50])
                # outcome = " ".join(row["O"].split()[:50])
                # text_field = self.text_to_field(population + "[SEP]" + intervention + "[SEP]" + comparison + "[SEP]" + outcome)
                outcome = " ".join(row["O"].split()[:20])
                text_field = self.text_to_field(outcome + "[SEP]" + text)
                # text_field = self.text_to_field(text + "[SEP]" + outcome)
                label_field = self.label_to_field(grade)

                fields = {'text': text_field}
                if grade:
                    fields['label'] = label_field

                yield Instance(fields)


@DatasetReader.register('bi-classification-simple-num')
class BiCsvReaderNumerical(CsvReader):
    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                grade = row["Grade"]
                # binarise the quality labels to high/not-high
                if label_to_idx(grade) != 3:
                    grade = "not-high"
                numericals_float = self.get_numericals(row, scaler=self.scaler)
                # outcome = " ".join(row["O"].split()[:20])
                # text_field = self.text_to_field(outcome + "[SEP]" + text)
                label_field = self.label_to_field(grade)
                feat_field = self.feat_to_field(np.array(numericals_float))

                fields = {'feat': feat_field}
                if grade:
                    fields['label'] = label_field

                yield Instance(fields)


def get_cochrane_id(txt):
    found = re.findall("CD\d+", txt)
    return found[0] if found else None


def get_val_from_child(child, attr_name, to_numeric=False):
    try:
        val = child[attr_name]
        if to_numeric:
            val = float(val)
    except KeyError:
        val = None

    return val


def get_studies_per_outcome(cochrane_id, xml_dir_path):
    f = f"{xml_dir_path}{cochrane_id}.xml"
    studies_per_outcome = defaultdict(dict)
    if Path(f).exists():
        with open(f, encoding="ISO-8859-1") as fh:
            soup = BeautifulSoup(fh, "lxml")
            analyses = soup.find("analyses_and_data")
            if analyses is not None:
                comparisons = analyses.find_all("comparison")
                for comparison in comparisons:
                    comparison_name = None
                    for child in comparison:
                        if child.name is None:
                            continue
                        if child.name == "name":
                            comparison_name = child.get_text(strip=True, separator=" ")
                        elif "outcome" in child.name:
                            outcome_name = child.find("name").get_text(strip=True, separator=" ")
                            effect_measure = get_val_from_child(child, "effect_measure")
                            effect_size = get_val_from_child(child, "effect_size", to_numeric=True)
                            if effect_size is not None:
                                effect_size = round(effect_size, 2)
                            val_chi2 = get_val_from_child(child, "chi2", to_numeric=True)
                            val_df = get_val_from_child(child, "df", to_numeric=True)
                            val_i2 = get_val_from_child(child, "i2", to_numeric=True)
                            val_i2_q = get_val_from_child(child, "i2_q", to_numeric=True)
                            val_p_chi2 = get_val_from_child(child, "p_chi2", to_numeric=True)
                            val_p_q = get_val_from_child(child, "p_q", to_numeric=True)
                            val_p_z = get_val_from_child(child, "p_z", to_numeric=True)
                            val_q = get_val_from_child(child, "q", to_numeric=True)
                            val_tau2 = get_val_from_child(child, "tau2", to_numeric=True)
                            val_z = get_val_from_child(child, "z", to_numeric=True)

                            studies = [entry["study_id"] for entry in child.find_all(attrs={"study_id": True})]
                            studies_per_outcome[comparison_name][outcome_name] = {"studies": studies,
                                                                                  "effect_size": effect_size,
                                                                                  "effect_measure": effect_measure,
                                                                                  "val_chi2": val_chi2,
                                                                                  "val_df": val_df,
                                                                                  "val_i2": val_i2,
                                                                                  "val_i2_q": val_i2_q,
                                                                                  "val_p_chi2": val_p_chi2,
                                                                                  "val_p_q": val_p_q,
                                                                                  "val_p_z": val_p_z,
                                                                                  "val_q": val_q,
                                                                                  "val_tau2": val_tau2,
                                                                                  "val_z": val_z}
                        else:
                            print(child.name)
    return studies_per_outcome


def get_doi_to_studies(xml_dir_path, json_dataset_path):
    fh = load_json(json_dataset_path)
    doi_to_studies = defaultdict(list)
    doi_to_studies_per_outcome = defaultdict(dict)
    for fold_n, insts in fh.items():
        for inst in tqdm(insts):
            studies = inst["studies"]["included"]
            for study in studies:
                chars = study["study_chars"]["char"]
                rob = study["study_chars"]["rob"]
                study_info = {"participants": chars["participants"], "interventions": chars["interventions"],
                              "outcomes": chars["outcomes"],
                              "methods": chars["methods"],
                              "alloc": rob["allo_conceal"],
                              "incom": rob["incomplete_out_data"],
                              "other": rob["other_bias"],
                              "outco": rob["outcome_blinding"],
                              "partb": rob["part_blinding"],
                              "blind": rob["blinding"],
                              "rands": rob["rand_seq_gen"],
                              "selec": rob["selective_reporting"],
                              "title": study["study_chars"]["title"]}
                doi_to_studies[inst["doi"]].append(study_info)

            # get studies relevant for each outcome
            cochrane_id = get_cochrane_id(inst["doi"])
            doi_to_studies_per_outcome[inst["doi"]] = get_studies_per_outcome(cochrane_id, xml_dir_path)

    return doi_to_studies, doi_to_studies_per_outcome


def get_doi_to_studies_invalid(xml_dir_path, json_dataset_path):
    fh = load_json(json_dataset_path)
    doi_to_studies = defaultdict(list)
    doi_to_studies_per_outcome = defaultdict(dict)
    for inst in fh:
        studies = inst["studies"]["included"]
        for study in studies:
            chars = study["study_chars"]["char"]
            rob = study["study_chars"]["rob"]
            study_info = {"participants": chars["participants"], "interventions": chars["interventions"],
                          "outcomes": chars["outcomes"], "methods": chars["methods"],
                          "alloc": rob["allo_conceal"],
                          "incom": rob["incomplete_out_data"],
                          "other": rob["other_bias"],
                          "outco": rob["outcome_blinding"],
                          "partb": rob["part_blinding"],
                          "blind": rob["blinding"],
                          "rands": rob["rand_seq_gen"],
                          "selec": rob["selective_reporting"],
                          "title": study["study_chars"]["title"]}
            doi_to_studies[inst["doi"]].append(study_info)
        # get studies relevant for each outcome
        cochrane_id = get_cochrane_id(inst["doi"])
        doi_to_studies_per_outcome[inst["doi"]] = get_studies_per_outcome(cochrane_id, xml_dir_path)

    return doi_to_studies, doi_to_studies_per_outcome


@DatasetReader.register('attentive-classification-simple')
class AttentiveCsvReader(CsvReader):
    def __init__(self, xml_dir_path, json_dataset_path, json_dataset_invalid_path, **kwargs):
        super().__init__(**kwargs)

        self.xml_dir_path = xml_dir_path
        self.json_dataset_path = json_dataset_path
        self.json_dataset_invalid_path = json_dataset_invalid_path
        self.doi_to_studies, self.doi_to_studies_per_outcome = get_studies(xml_dir_path, self.json_dataset_path,
                                                                           self.json_dataset_invalid_path)

    def fields_to_instance(self, text_list_field: ListField[TextField] = None,
                           pop_field: TextField = None,
                           int_field: TextField = None,
                           com_field: TextField = None,
                           out_field: TextField = None,
                           population_studies_list_field: ListField[TextField] = None,
                           intervention_studies_list_field: ListField[TextField] = None,
                           outcome_studies_list_field: ListField[TextField] = None,
                           methods_studies_list_field: ListField[TextField] = None,
                           alloc_studies_list_field: ListField[TextField] = None,
                           incom_studies_list_field: ListField[TextField] = None,
                           other_studies_list_field: ListField[TextField] = None,
                           outco_studies_list_field: ListField[TextField] = None,
                           partb_studies_list_field: ListField[TextField] = None,
                           blind_studies_list_field: ListField[TextField] = None,
                           rands_studies_list_field: ListField[TextField] = None,
                           selec_studies_list_field: ListField[TextField] = None,
                           alloc_studies_supp_list_field: ListField[TextField] = None,
                           incom_studies_supp_list_field: ListField[TextField] = None,
                           other_studies_supp_list_field: ListField[TextField] = None,
                           outco_studies_supp_list_field: ListField[TextField] = None,
                           partb_studies_supp_list_field: ListField[TextField] = None,
                           blind_studies_supp_list_field: ListField[TextField] = None,
                           rands_studies_supp_list_field: ListField[TextField] = None,
                           selec_studies_supp_list_field: ListField[TextField] = None,
                           feat_field: ArrayField = None,
                           cat_feats_list_field: ListField[TextField] = None,
                           label_field: Union[LabelField, MultiLabelField] = None) -> Instance:
        fields = {}
        if text_list_field is not None:
            fields["text_list"] = text_list_field
        if pop_field is not None:
            fields["population"] = pop_field
        if int_field is not None:
            fields["intervention"] = int_field
        if com_field is not None:
            fields["comparator"] = com_field
        if out_field is not None:
            fields["outcome"] = out_field
        if population_studies_list_field is not None:
            fields["population_studies_list"] = population_studies_list_field
        if intervention_studies_list_field is not None:
            fields["intervention_studies_list"] = intervention_studies_list_field
        if outcome_studies_list_field is not None:
            fields["outcome_studies_list"] = outcome_studies_list_field
        if methods_studies_list_field is not None:
            fields["methods_studies_list"] = methods_studies_list_field
        if alloc_studies_list_field is not None:
            fields["alloc_studies_list"] = alloc_studies_list_field
        if incom_studies_list_field is not None:
            fields["incom_studies_list"] = incom_studies_list_field
        if other_studies_list_field is not None:
            fields["other_studies_list"] = other_studies_list_field
        if outco_studies_list_field is not None:
            fields["outco_studies_list"] = outco_studies_list_field
        if partb_studies_list_field is not None:
            fields["partb_studies_list"] = partb_studies_list_field
        if blind_studies_list_field is not None:
            fields["blind_studies_list"] = blind_studies_list_field
        if rands_studies_list_field is not None:
            fields["rands_studies_list"] = rands_studies_list_field
        if selec_studies_list_field is not None:
            fields["selec_studies_list"] = selec_studies_list_field
        if alloc_studies_supp_list_field is not None:
            fields["alloc_studies_supp_list"] = alloc_studies_supp_list_field
        if incom_studies_supp_list_field is not None:
            fields["incom_studies_supp_list"] = incom_studies_supp_list_field
        if other_studies_supp_list_field is not None:
            fields["other_studies_supp_list"] = other_studies_supp_list_field
        if outco_studies_supp_list_field is not None:
            fields["outco_studies_supp_list"] = outco_studies_supp_list_field
        if partb_studies_supp_list_field is not None:
            fields["partb_studies_supp_list"] = partb_studies_supp_list_field
        if blind_studies_supp_list_field is not None:
            fields["blind_studies_supp_list"] = blind_studies_supp_list_field
        if rands_studies_supp_list_field is not None:
            fields["rands_studies_supp_list"] = rands_studies_supp_list_field
        if selec_studies_supp_list_field is not None:
            fields["selec_studies_supp_list"] = selec_studies_supp_list_field
        if feat_field is not None:
            fields["feat"] = feat_field
        if cat_feats_list_field is not None:
            fields["cat_feats_list"] = cat_feats_list_field
        if label_field is not None:
            fields["label"] = label_field

        return Instance(fields)

    def studies_feat_to_list_field(self, feat_name, feat_subname, studies):
        feat_studies = []
        for study in studies:
            feat = study[feat_name] or ""
            if isinstance(feat, dict) and "judgement" in feat and "support" in feat:
                feat = feat[feat_subname] or ""
            feat_studies.append(self.cat_feat_to_field(feat))
        if not feat_studies:
            feat_studies = ListField([self.cat_feat_to_field("")])

        return ListField(feat_studies)

    def get_fields(self, row):
        # numerical features:
        feat_field = None
        if self.feat_type["num"]:
            numericals_float = self.get_numericals(row, scaler=self.scaler)
            feat_field = self.feat_to_field(np.array(numericals_float))

        # textual features:
        text_list_field = None
        if self.feat_type["text"]:
            text1 = row["full_abstract_txt"]
            text2 = row["abstract_conclusion_txt"]
            text3 = row["plain_language_summary_txt"]
            text4 = row["authors_conclusions_txt"]
            picos = []
            # picos.append(" ".join(row["P"].split()[:20]))
            # picos.append(" ".join(row["I"].split()[:20]))
            # picos.append(" ".join(row["C"].split()[:20]))
            picos.append(" ".join(row["O"].split()[:20]))
            picos_sep = "[SEP]".join(picos)
            text_field1 = self.text_to_field(picos_sep + "[SEP]" + text1)
            text_field2 = self.text_to_field(picos_sep + "[SEP]" + text2)
            text_field3 = self.text_to_field(picos_sep + "[SEP]" + text3)
            text_field4 = self.text_to_field(picos_sep + "[SEP]" + text4)
            text_list_field = ListField([text_field1, text_field2, text_field3, text_field4])

        # pico mentions:
        max_len = round((512 - 3) / 4)  # 4 PICOs, 3 SEPs
        pop_field = self.cat_feat_to_field(" ".join(row["P"].split()[:max_len]))
        int_field = self.cat_feat_to_field(" ".join(row["I"].split()[:max_len]))
        com_field = self.cat_feat_to_field(" ".join(row["C"].split()[:max_len]))
        out_field = self.cat_feat_to_field(" ".join(row["O"].split()[:max_len]))

        studies = self.doi_to_studies[row["doi"]]

        # picos from primary studies
        population_studies = []
        intervention_studies = []
        outcome_studies = []
        for study in studies:
            p = study["participants"] or ""
            population_studies.append(self.cat_feat_to_field(" ".join(p.split()[:max_len])))
            i = study["interventions"] or ""
            intervention_studies.append(self.cat_feat_to_field(" ".join(i.split()[:max_len])))
            o = study["outcomes"] or ""
            outcome_studies.append(self.cat_feat_to_field(" ".join(o.split()[:max_len])))
        if not population_studies:
            population_studies_list_field = ListField([self.cat_feat_to_field("")])
        else:
            population_studies_list_field = ListField(population_studies)
        if not intervention_studies:
            intervention_studies_list_field = ListField([self.cat_feat_to_field("")])
        else:
            intervention_studies_list_field = ListField(intervention_studies)
        if not outcome_studies:
            outcome_studies_list_field = ListField([self.cat_feat_to_field("")])
        else:
            outcome_studies_list_field = ListField(outcome_studies)

        # methods from primary studies
        methods_studies_list_field = self.studies_feat_to_list_field("methods", None, studies)

        # rob criteria from primary studies
        alloc_studies_list_field = self.studies_feat_to_list_field("alloc", "judgement", studies)
        incom_studies_list_field = self.studies_feat_to_list_field("incom", "judgement", studies)
        other_studies_list_field = self.studies_feat_to_list_field("other", "judgement", studies)
        outco_studies_list_field = self.studies_feat_to_list_field("outco", "judgement", studies)
        partb_studies_list_field = self.studies_feat_to_list_field("partb", "judgement", studies)
        blind_studies_list_field = self.studies_feat_to_list_field("blind", "judgement", studies)
        rands_studies_list_field = self.studies_feat_to_list_field("rands", "judgement", studies)
        selec_studies_list_field = self.studies_feat_to_list_field("selec", "judgement", studies)

        alloc_studies_supp_list_field = self.studies_feat_to_list_field("alloc", "support", studies)
        incom_studies_supp_list_field = self.studies_feat_to_list_field("incom", "support", studies)
        other_studies_supp_list_field = self.studies_feat_to_list_field("other", "support", studies)
        outco_studies_supp_list_field = self.studies_feat_to_list_field("outco", "support", studies)
        partb_studies_supp_list_field = self.studies_feat_to_list_field("partb", "support", studies)
        blind_studies_supp_list_field = self.studies_feat_to_list_field("blind", "support", studies)
        rands_studies_supp_list_field = self.studies_feat_to_list_field("rands", "support", studies)
        selec_studies_supp_list_field = self.studies_feat_to_list_field("selec", "support", studies)

        # categorical (textual) features:
        cat_feats_list_field = None
        if self.feat_type["cat"]:
            cat_feats_list_field = self.get_categoricals(row)

        return feat_field, text_list_field, pop_field, int_field, com_field, out_field, population_studies_list_field, intervention_studies_list_field, outcome_studies_list_field, methods_studies_list_field, \
               alloc_studies_list_field, incom_studies_list_field, other_studies_list_field, outco_studies_list_field, \
               partb_studies_list_field, blind_studies_list_field, rands_studies_list_field, selec_studies_list_field, \
               alloc_studies_supp_list_field, incom_studies_supp_list_field, other_studies_supp_list_field, outco_studies_supp_list_field, \
               partb_studies_supp_list_field, blind_studies_supp_list_field, rands_studies_supp_list_field, selec_studies_supp_list_field, \
               cat_feats_list_field

    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                # get input fields
                feat_field, text_list_field, pop_field, int_field, com_field, out_field, population_studies_list_field, \
                intervention_studies_list_field, outcome_studies_list_field, methods_studies_list_field, \
                alloc_studies_list_field, incom_studies_list_field, other_studies_list_field, outco_studies_list_field, \
                partb_studies_list_field, blind_studies_list_field, rands_studies_list_field, selec_studies_list_field, \
                alloc_studies_supp_list_field, incom_studies_supp_list_field, other_studies_supp_list_field, outco_studies_supp_list_field, \
                partb_studies_supp_list_field, blind_studies_supp_list_field, rands_studies_supp_list_field, selec_studies_supp_list_field, \
                cat_feats_list_field = self.get_fields(row)

                # label:
                label_field = self.get_label(row)

                yield self.fields_to_instance(text_list_field=text_list_field,
                                              pop_field=pop_field,
                                              int_field=int_field,
                                              com_field=com_field,
                                              out_field=out_field,
                                              population_studies_list_field=population_studies_list_field,
                                              intervention_studies_list_field=intervention_studies_list_field,
                                              outcome_studies_list_field=outcome_studies_list_field,
                                              methods_studies_list_field=methods_studies_list_field,
                                              alloc_studies_list_field=alloc_studies_list_field,
                                              incom_studies_list_field=incom_studies_list_field,
                                              other_studies_list_field=other_studies_list_field,
                                              outco_studies_list_field=outco_studies_list_field,
                                              partb_studies_list_field=partb_studies_list_field,
                                              blind_studies_list_field=blind_studies_list_field,
                                              rands_studies_list_field=rands_studies_list_field,
                                              selec_studies_list_field=selec_studies_list_field,
                                              alloc_studies_supp_list_field=alloc_studies_supp_list_field,
                                              incom_studies_supp_list_field=incom_studies_supp_list_field,
                                              other_studies_supp_list_field=other_studies_supp_list_field,
                                              outco_studies_supp_list_field=outco_studies_supp_list_field,
                                              partb_studies_supp_list_field=partb_studies_supp_list_field,
                                              blind_studies_supp_list_field=blind_studies_supp_list_field,
                                              rands_studies_supp_list_field=rands_studies_supp_list_field,
                                              selec_studies_supp_list_field=selec_studies_supp_list_field,
                                              feat_field=feat_field,
                                              cat_feats_list_field=cat_feats_list_field,
                                              label_field=label_field)


def get_studies(xml_dir_path, json_dataset_path, json_dataset_invalid_path):
    if Path(f"{xml_dir_path}doi_to_studies.json").exists() and Path(
            f"{xml_dir_path}doi_to_studies_per_outcome.json").exists():
        doi_to_studies = load_json(f"{xml_dir_path}doi_to_studies.json")
        doi_to_studies_per_outcome = load_json(f"{xml_dir_path}doi_to_studies_per_outcome.json")
        logger.info("Loaded doi_to_studies and doi_to_studies_per_outcome as json files.")
    else:
        doi_to_studies, doi_to_studies_per_outcome = get_doi_to_studies(
            f"{xml_dir_path}cochrane-last-published/", json_dataset_path)
        logger.info("Obtained doi_to_studies and doi_to_studies_per_outcome.")
        doi_to_studies_invalid, doi_to_studies_per_outcome_invalid = get_doi_to_studies_invalid(
            f"{xml_dir_path}cochrane-last-published/", json_dataset_invalid_path)
        logger.info("Obtained doi_to_studies and doi_to_studies_per_outcome for invalid instances.")
        doi_to_studies.update(doi_to_studies_invalid)
        doi_to_studies_per_outcome.update(doi_to_studies_per_outcome_invalid)
        save_json(doi_to_studies, f"{xml_dir_path}doi_to_studies.json")
        save_json(doi_to_studies_per_outcome, f"{xml_dir_path}doi_to_studies_per_outcome.json")
        logger.info("Saved doi_to_studies and doi_to_studies_per_outcome as json files.")

    return doi_to_studies, doi_to_studies_per_outcome


def fix_unicode(study_name):
    def get_char(code):
        return f'\\u{code}'.encode('ascii').decode('unicode_escape')

    codes = re.findall("_x(.*?)_", study_name)
    for code in codes:
        study_name = re.sub(f"_x{code}_", get_char(code), study_name)
    return study_name


def select_studies(all_studies, studies_per_outcome):
    study_names_per_outcome = set()
    for s in studies_per_outcome:
        assert s.startswith("STD-")
        study_name = " ".join(s[4:].rsplit("-", 1))
        # substitute the unicode code with the character
        study_name = fix_unicode(study_name)
        study_names_per_outcome.add(study_name)

    selected_studies = []
    for s in all_studies:
        if s["title"] in study_names_per_outcome:
            selected_studies.append(s)

    return selected_studies


def expand_vals_per_outcome(vals_per_outcome, alloc_studies_list_field, incom_studies_list_field,
                            other_studies_list_field,
                            outco_studies_list_field, partb_studies_list_field, blind_studies_list_field,
                            rands_studies_list_field, selec_studies_list_field):
    def get_count(level, studies):
        c = 0
        for i in studies:
            if level in set(map(lambda x: x.text.lower(), i.tokens)):
                c += 1
        return c

    vals_per_outcome["val_n_high_risk_alloc"] = get_count("high", alloc_studies_list_field)
    vals_per_outcome["val_n_high_risk_incom"] = get_count("high", incom_studies_list_field)
    vals_per_outcome["val_n_high_risk_other"] = get_count("high", other_studies_list_field)
    vals_per_outcome["val_n_high_risk_outco"] = get_count("high", outco_studies_list_field)
    vals_per_outcome["val_n_high_risk_partb"] = get_count("high", partb_studies_list_field)
    vals_per_outcome["val_n_high_risk_blind"] = get_count("high", blind_studies_list_field)
    vals_per_outcome["val_n_high_risk_rands"] = get_count("high", rands_studies_list_field)
    vals_per_outcome["val_n_high_risk_selec"] = get_count("high", selec_studies_list_field)

    vals_per_outcome["val_n_low_risk_alloc"] = get_count("low", alloc_studies_list_field)
    vals_per_outcome["val_n_low_risk_incom"] = get_count("low", incom_studies_list_field)
    vals_per_outcome["val_n_low_risk_other"] = get_count("low", other_studies_list_field)
    vals_per_outcome["val_n_low_risk_outco"] = get_count("low", outco_studies_list_field)
    vals_per_outcome["val_n_low_risk_partb"] = get_count("low", partb_studies_list_field)
    vals_per_outcome["val_n_low_risk_blind"] = get_count("low", blind_studies_list_field)
    vals_per_outcome["val_n_low_risk_rands"] = get_count("low", rands_studies_list_field)
    vals_per_outcome["val_n_low_risk_selec"] = get_count("low", selec_studies_list_field)

    vals_per_outcome["val_n_unclear_risk_alloc"] = get_count("unclear", alloc_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_incom"] = get_count("unclear", incom_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_other"] = get_count("unclear", other_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_outco"] = get_count("unclear", outco_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_partb"] = get_count("unclear", partb_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_blind"] = get_count("unclear", blind_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_rands"] = get_count("unclear", rands_studies_list_field)
    vals_per_outcome["val_n_unclear_risk_selec"] = get_count("unclear", selec_studies_list_field)

    vals_per_outcome["val_prop_high_risk_alloc"] = get_count("high", alloc_studies_list_field) / len(
        alloc_studies_list_field)
    vals_per_outcome["val_prop_high_risk_incom"] = get_count("high", incom_studies_list_field) / len(
        incom_studies_list_field)
    vals_per_outcome["val_prop_high_risk_other"] = get_count("high", other_studies_list_field) / len(
        other_studies_list_field)
    vals_per_outcome["val_prop_high_risk_outco"] = get_count("high", outco_studies_list_field) / len(
        outco_studies_list_field)
    vals_per_outcome["val_prop_high_risk_partb"] = get_count("high", partb_studies_list_field) / len(
        partb_studies_list_field)
    vals_per_outcome["val_prop_high_risk_blind"] = get_count("high", blind_studies_list_field) / len(
        blind_studies_list_field)
    vals_per_outcome["val_prop_high_risk_rands"] = get_count("high", rands_studies_list_field) / len(
        rands_studies_list_field)
    vals_per_outcome["val_prop_high_risk_selec"] = get_count("high", selec_studies_list_field) / len(
        selec_studies_list_field)


@DatasetReader.register('classification-simple-studies')
class CsvReaderStudies(CsvReader):
    """
    Loads only those instances where we can unambiguously extract the primary studies relevant for the outcome that
    we are assessing.
    """

    def __init__(self, xml_dir_path, json_dataset_path, json_dataset_invalid_path, **kwargs):
        kwargs["xml_dir_path"] = xml_dir_path
        kwargs["json_dataset_path"] = json_dataset_path
        kwargs["json_dataset_invalid_path"] = json_dataset_invalid_path
        super().__init__(**kwargs)

        self.doi_to_studies, self.doi_to_studies_per_outcome = get_studies(xml_dir_path, self.json_dataset_path,
                                                                           self.json_dataset_invalid_path)

    def fields_to_instance(self, text_list_field: ListField[TextField] = None,
                           pop_field: TextField = None,
                           int_field: TextField = None,
                           com_field: TextField = None,
                           out_field: TextField = None,
                           population_studies_list_field: ListField[TextField] = None,
                           intervention_studies_list_field: ListField[TextField] = None,
                           outcome_studies_list_field: ListField[TextField] = None,
                           methods_studies_list_field: ListField[TextField] = None,
                           alloc_studies_list_field: ListField[TextField] = None,
                           incom_studies_list_field: ListField[TextField] = None,
                           other_studies_list_field: ListField[TextField] = None,
                           outco_studies_list_field: ListField[TextField] = None,
                           partb_studies_list_field: ListField[TextField] = None,
                           blind_studies_list_field: ListField[TextField] = None,
                           rands_studies_list_field: ListField[TextField] = None,
                           selec_studies_list_field: ListField[TextField] = None,
                           alloc_studies_supp_list_field: ListField[TextField] = None,
                           incom_studies_supp_list_field: ListField[TextField] = None,
                           other_studies_supp_list_field: ListField[TextField] = None,
                           outco_studies_supp_list_field: ListField[TextField] = None,
                           partb_studies_supp_list_field: ListField[TextField] = None,
                           blind_studies_supp_list_field: ListField[TextField] = None,
                           rands_studies_supp_list_field: ListField[TextField] = None,
                           selec_studies_supp_list_field: ListField[TextField] = None,
                           feat_field: ArrayField = None,
                           cat_feats_list_field: ListField[TextField] = None,
                           label_field: Union[LabelField, MultiLabelField] = None) -> Instance:
        fields = {}
        if text_list_field is not None:
            fields["text_list"] = text_list_field
        if pop_field is not None:
            fields["population"] = pop_field
        if int_field is not None:
            fields["intervention"] = int_field
        if com_field is not None:
            fields["comparator"] = com_field
        if out_field is not None:
            fields["outcome"] = out_field
        if population_studies_list_field is not None:
            fields["population_studies_list"] = population_studies_list_field
        if intervention_studies_list_field is not None:
            fields["intervention_studies_list"] = intervention_studies_list_field
        if outcome_studies_list_field is not None:
            fields["outcome_studies_list"] = outcome_studies_list_field
        if methods_studies_list_field is not None:
            fields["methods_studies_list"] = methods_studies_list_field
        if alloc_studies_list_field is not None:
            fields["alloc_studies_list"] = alloc_studies_list_field
        if incom_studies_list_field is not None:
            fields["incom_studies_list"] = incom_studies_list_field
        if other_studies_list_field is not None:
            fields["other_studies_list"] = other_studies_list_field
        if outco_studies_list_field is not None:
            fields["outco_studies_list"] = outco_studies_list_field
        if partb_studies_list_field is not None:
            fields["partb_studies_list"] = partb_studies_list_field
        if blind_studies_list_field is not None:
            fields["blind_studies_list"] = blind_studies_list_field
        if rands_studies_list_field is not None:
            fields["rands_studies_list"] = rands_studies_list_field
        if selec_studies_list_field is not None:
            fields["selec_studies_list"] = selec_studies_list_field
        if alloc_studies_supp_list_field is not None:
            fields["alloc_studies_supp_list"] = alloc_studies_supp_list_field
        if incom_studies_supp_list_field is not None:
            fields["incom_studies_supp_list"] = incom_studies_supp_list_field
        if other_studies_supp_list_field is not None:
            fields["other_studies_supp_list"] = other_studies_supp_list_field
        if outco_studies_supp_list_field is not None:
            fields["outco_studies_supp_list"] = outco_studies_supp_list_field
        if partb_studies_supp_list_field is not None:
            fields["partb_studies_supp_list"] = partb_studies_supp_list_field
        if blind_studies_supp_list_field is not None:
            fields["blind_studies_supp_list"] = blind_studies_supp_list_field
        if rands_studies_supp_list_field is not None:
            fields["rands_studies_supp_list"] = rands_studies_supp_list_field
        if selec_studies_supp_list_field is not None:
            fields["selec_studies_supp_list"] = selec_studies_supp_list_field
        if feat_field is not None:
            fields["feat"] = feat_field
        if cat_feats_list_field is not None:
            fields["cat_feats_list"] = cat_feats_list_field
        if label_field is not None:
            fields["label"] = label_field

        return Instance(fields)

    def studies_feat_to_list_field(self, feat_name, feat_subname, studies):
        feat_studies = []
        for study in studies:
            feat = study[feat_name] or ""
            if isinstance(feat, dict) and "judgement" in feat and "support" in feat:
                feat = feat[feat_subname] or ""
            feat_studies.append(self.cat_feat_to_field(feat))
        if not feat_studies:
            feat_studies = ListField([self.cat_feat_to_field("")])

        return ListField(feat_studies)

    def get_fields_studies(self, row, studies_per_outcome, vals_per_outcome):
        # textual features:
        text_list_field = None
        if self.feat_type["text"]:
            text1 = row["full_abstract_txt"]
            text2 = row["abstract_conclusion_txt"]
            text3 = row["plain_language_summary_txt"]
            text4 = row["authors_conclusions_txt"]
            picos = []
            # picos.append(" ".join(row["P"].split()[:20]))
            # picos.append(" ".join(row["I"].split()[:20]))
            # picos.append(" ".join(row["C"].split()[:20]))
            picos.append(" ".join(row["O"].split()[:20]))
            picos_sep = "[SEP]".join(picos)
            text_field1 = self.text_to_field(picos_sep + "[SEP]" + text1)
            text_field2 = self.text_to_field(picos_sep + "[SEP]" + text2)
            text_field3 = self.text_to_field(picos_sep + "[SEP]" + text3)
            text_field4 = self.text_to_field(picos_sep + "[SEP]" + text4)
            text_list_field = ListField([text_field1, text_field2, text_field3, text_field4])

        # pico mentions:
        max_len = round((512 - 3) / 4)  # 4 PICOs, 3 SEPs
        pop_field = self.cat_feat_to_field(" ".join(row["P"].split()[:max_len]))
        int_field = self.cat_feat_to_field(" ".join(row["I"].split()[:max_len]))
        com_field = self.cat_feat_to_field(" ".join(row["C"].split()[:max_len]))
        out_field = self.cat_feat_to_field(" ".join(row["O"].split()[:max_len]))

        all_studies = self.doi_to_studies[row["doi"]]
        # selected studies based on studies_per_outcome
        studies = select_studies(all_studies, studies_per_outcome)

        # picos from primary studies
        population_studies = []
        intervention_studies = []
        outcome_studies = []
        for study in studies:
            p = study["participants"] or ""
            population_studies.append(self.cat_feat_to_field(" ".join(p.split()[:max_len])))
            i = study["interventions"] or ""
            intervention_studies.append(self.cat_feat_to_field(" ".join(i.split()[:max_len])))
            o = study["outcomes"] or ""
            outcome_studies.append(self.cat_feat_to_field(" ".join(o.split()[:max_len])))
        if not population_studies:
            population_studies_list_field = ListField([self.cat_feat_to_field("")])
        else:
            population_studies_list_field = ListField(population_studies)
        if not intervention_studies:
            intervention_studies_list_field = ListField([self.cat_feat_to_field("")])
        else:
            intervention_studies_list_field = ListField(intervention_studies)
        if not outcome_studies:
            outcome_studies_list_field = ListField([self.cat_feat_to_field("")])
        else:
            outcome_studies_list_field = ListField(outcome_studies)

        # methods from primary studies
        methods_studies_list_field = self.studies_feat_to_list_field("methods", None, studies)

        # rob criteria from primary studies
        alloc_studies_list_field = self.studies_feat_to_list_field("alloc", "judgement", studies)
        incom_studies_list_field = self.studies_feat_to_list_field("incom", "judgement", studies)
        other_studies_list_field = self.studies_feat_to_list_field("other", "judgement", studies)
        outco_studies_list_field = self.studies_feat_to_list_field("outco", "judgement", studies)
        partb_studies_list_field = self.studies_feat_to_list_field("partb", "judgement", studies)
        blind_studies_list_field = self.studies_feat_to_list_field("blind", "judgement", studies)
        rands_studies_list_field = self.studies_feat_to_list_field("rands", "judgement", studies)
        selec_studies_list_field = self.studies_feat_to_list_field("selec", "judgement", studies)

        alloc_studies_supp_list_field = self.studies_feat_to_list_field("alloc", "support", studies)
        incom_studies_supp_list_field = self.studies_feat_to_list_field("incom", "support", studies)
        other_studies_supp_list_field = self.studies_feat_to_list_field("other", "support", studies)
        outco_studies_supp_list_field = self.studies_feat_to_list_field("outco", "support", studies)
        partb_studies_supp_list_field = self.studies_feat_to_list_field("partb", "support", studies)
        blind_studies_supp_list_field = self.studies_feat_to_list_field("blind", "support", studies)
        rands_studies_supp_list_field = self.studies_feat_to_list_field("rands", "support", studies)
        selec_studies_supp_list_field = self.studies_feat_to_list_field("selec", "support", studies)

        expand_vals_per_outcome(vals_per_outcome, alloc_studies_list_field, incom_studies_list_field,
                                other_studies_list_field,
                                outco_studies_list_field, partb_studies_list_field, blind_studies_list_field,
                                rands_studies_list_field, selec_studies_list_field)
        # categorical (textual) features:
        cat_feats_list_field = None
        if self.feat_type["cat"]:
            cat_feats_list_field = self.get_categoricals(row)

        # numerical features:
        feat_field = None
        if self.feat_type["num"]:
            numericals_float = self.get_numericals(row, scaler=self.scaler, row_per_outcome=vals_per_outcome)
            feat_field = self.feat_to_field(np.array(numericals_float))

        return feat_field, text_list_field, pop_field, int_field, com_field, out_field, population_studies_list_field, intervention_studies_list_field, outcome_studies_list_field, methods_studies_list_field, \
               alloc_studies_list_field, incom_studies_list_field, other_studies_list_field, outco_studies_list_field, \
               partb_studies_list_field, blind_studies_list_field, rands_studies_list_field, selec_studies_list_field, \
               alloc_studies_supp_list_field, incom_studies_supp_list_field, other_studies_supp_list_field, outco_studies_supp_list_field, \
               partb_studies_supp_list_field, blind_studies_supp_list_field, rands_studies_supp_list_field, selec_studies_supp_list_field, \
               cat_feats_list_field

    def _read(self, file_path: str) -> Iterable[Instance]:
        with open(file_path) as fh:
            reader = csv.DictReader(fh)
            for row in reader:
                # try to obtain studies per outcome; if None, skip
                studies_per_outcome, vals_per_outcome = get_relevant_studies(self.doi_to_studies_per_outcome,
                                                                             row["doi"],
                                                                             row["sof_title"],
                                                                             row["O"],
                                                                             row["n_studies"],
                                                                             row["type_effect"],
                                                                             row["relative_effect"])
                if studies_per_outcome is None:
                    continue

                # get input fields
                feat_field, text_list_field, pop_field, int_field, com_field, out_field, population_studies_list_field, \
                intervention_studies_list_field, outcome_studies_list_field, methods_studies_list_field, \
                alloc_studies_list_field, incom_studies_list_field, other_studies_list_field, outco_studies_list_field, \
                partb_studies_list_field, blind_studies_list_field, rands_studies_list_field, selec_studies_list_field, \
                alloc_studies_supp_list_field, incom_studies_supp_list_field, other_studies_supp_list_field, outco_studies_supp_list_field, \
                partb_studies_supp_list_field, blind_studies_supp_list_field, rands_studies_supp_list_field, selec_studies_supp_list_field, \
                cat_feats_list_field = self.get_fields_studies(row, studies_per_outcome, vals_per_outcome)

                # label:
                label_field = self.get_label(row)

                yield self.fields_to_instance(text_list_field=text_list_field,
                                              pop_field=pop_field,
                                              int_field=int_field,
                                              com_field=com_field,
                                              out_field=out_field,
                                              population_studies_list_field=population_studies_list_field,
                                              intervention_studies_list_field=intervention_studies_list_field,
                                              outcome_studies_list_field=outcome_studies_list_field,
                                              methods_studies_list_field=methods_studies_list_field,
                                              alloc_studies_list_field=alloc_studies_list_field,
                                              incom_studies_list_field=incom_studies_list_field,
                                              other_studies_list_field=other_studies_list_field,
                                              outco_studies_list_field=outco_studies_list_field,
                                              partb_studies_list_field=partb_studies_list_field,
                                              blind_studies_list_field=blind_studies_list_field,
                                              rands_studies_list_field=rands_studies_list_field,
                                              selec_studies_list_field=selec_studies_list_field,
                                              alloc_studies_supp_list_field=alloc_studies_supp_list_field,
                                              incom_studies_supp_list_field=incom_studies_supp_list_field,
                                              other_studies_supp_list_field=other_studies_supp_list_field,
                                              outco_studies_supp_list_field=outco_studies_supp_list_field,
                                              partb_studies_supp_list_field=partb_studies_supp_list_field,
                                              blind_studies_supp_list_field=blind_studies_supp_list_field,
                                              rands_studies_supp_list_field=rands_studies_supp_list_field,
                                              selec_studies_supp_list_field=selec_studies_supp_list_field,
                                              feat_field=feat_field,
                                              cat_feats_list_field=cat_feats_list_field,
                                              label_field=label_field)


@DatasetReader.register('attentive-classification-simple-reasons')
class AttentiveCsvReaderReasons(AttentiveCsvReader, CsvReaderReasons):
    pass


@DatasetReader.register('classification-simple-studies-reasons')
class CsvReaderStudiesReasons(CsvReaderStudies, CsvReaderReasons):
    pass


@DatasetReader.register('bi-classification-simple-studies-reasons')
class BiCsvReaderStudiesReasons(CsvReaderStudies, BiCsvReaderReason):
    pass


@DatasetReader.register('attentive-ordinal-regression')
class AttentiveCsvReaderOrdinal(AttentiveCsvReader):
    def label_to_field(self, label: str = None, namespace: str = None) -> Optional[ArrayField]:
        if label:
            return ArrayField(self.label_to_levels(label_to_idx(label)))
        else:
            return None

    def label_to_levels(self, label, num_classes=4):
        """
        Encode the grade label as a binary array:
        "very low" -> [0,0,0]
        "low" -> [1,0,0]
        "moderate" -> [1,1,0]
        "high" -> [1,1,1]
        """
        levels = [1] * label + [0] * (num_classes - 1 - label)

        return np.array(levels, dtype="float32")


@DatasetReader.register('attentive-ordinal-regression-simple')
class AttentiveCsvReaderOrdinalSimple(AttentiveCsvReaderOrdinal):
    def label_to_levels(self, label, num_classes=4):
        """
                Encode the grade label to lie between 0 and num_classes.

                "very low" -> 0.
                "low" -> 1.
                "moderate" -> 2.
                "high" -> 3.
        """
        return np.array([label], dtype="float32")


@DatasetReader.register('grade-num-reasons')
class CsvReaderGradeAndNumReasons(CsvReaderOrdinalSimpleNumReasons, CsvReaderGradeAndReasons):
    pass


@DatasetReader.register('reasons-num-reasons')
class CsvReaderReasonsAndNumReasons(CsvReaderOrdinalSimpleNumReasons, CsvReaderGradeAndReasons):
    def get_top_label(self, row, restricted=False):
        """
        :param restricted: consider only RoB and imprecision (two most freq) labels
        """
        reasons_dict = eval(row["Reasons"])
        reasons = []
        for reason in reasons_dict.keys():
            if restricted:
                if reason in {"risk of bias", "imprecision"}:
                    reasons.append(reason.replace(" ", "-"))
            else:
                reasons.append(reason.replace(" ", "-"))

        label_field = self.multiple_labels_to_field(reasons)
        return label_field


@DatasetReader.register('reasons-num-reason-types')
class CsvReaderReasonsAndNumReasonTypes(CsvReaderOrdinalSimpleNumReasonTypes, CsvReaderGradeAndReasons):
    def get_top_label(self, row, restricted=False):
        """
        :param restricted: consider only RoB and imprecision (two most freq) labels
        """
        reasons_dict = eval(row["Reasons"])
        reasons = []
        for reason in reasons_dict.keys():
            if restricted:
                if reason in {"risk of bias", "imprecision"}:
                    reasons.append(reason.replace(" ", "-"))
            else:
                reasons.append(reason.replace(" ", "-"))

        label_field = self.multiple_labels_to_field(reasons)
        return label_field
