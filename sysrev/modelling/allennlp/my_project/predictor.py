import numpy as np
from allennlp.common import JsonDict
from allennlp.common.util import sanitize
from allennlp.data import Instance
from allennlp.predictors.predictor import Predictor
from scipy.special import softmax
from scipy.stats import stats


@Predictor.register('hierarchical_interactive_predictor')
class HierachicalInteractivePredictor(Predictor):
    def predict_instance(self, instance: Instance) -> JsonDict:
        outputs = self._model.forward_on_instance(instance)
        label_vocab = self._model.vocab.get_index_to_token_vocabulary('top_labels')
        label_reasons_vocab = self._model.vocab.get_index_to_token_vocabulary('labels')
        outputs["grade_label_vocab"] = label_vocab
        outputs["reasons_label_vocab"] = label_reasons_vocab

        # pre-GRADE
        outputs['predicted_pre_grade'] = label_vocab[np.argmax(outputs["pre_logits_grade"])]
        # GRADE
        outputs['predicted_grade'] = label_vocab[np.argmax(outputs["logits_grade"])]
        outputs['predicted_probs_grade'] = outputs["probs_grade"]

        # reasons
        preds_reasons = outputs["logits_reasons"] >= 0.0
        outputs["predicted_reasons"] = np.array(list(label_reasons_vocab.values()))[preds_reasons]
        outputs["predicted_probs_reasons"] = outputs["probs_reasons"]

        # true labels
        if "top_label" in outputs:
            outputs['top_label'] = instance.fields['top_label'].label

        return sanitize(outputs)

    def dump_line(self, outputs: JsonDict) -> str:
        return f"{outputs['predicted_pre_grade']}\t{outputs['predicted_grade']}\t{outputs['top_label']}\n"


@Predictor.register('classifier_multilabel_predictor')
class ClassifierMultilabelPredictor(Predictor):
    def predict_instance(self, instance: Instance) -> JsonDict:
        outputs = self._model.forward_on_instance(instance)
        label_vocab = self._model.vocab.get_index_to_token_vocabulary('labels')

        pred_labels = []
        # threshold should be set to 0.0 as we are dealing with logits and not probs
        # preds = outputs["logits"] >= 0.5
        preds = outputs["logits"] >= 0.0

        for i, pred in enumerate(preds):
            if pred:
                pred_labels.append(label_vocab[i])
        outputs['predicted'] = pred_labels

        # true labels
        outputs['labels'] = instance.fields['label'].labels

        return sanitize(outputs)

    def dump_line(self, outputs: JsonDict) -> str:
        return f'{outputs["predicted"]}\t{outputs["labels"]}\t{outputs["logits"]}\n'


@Predictor.register('classifier_predictor')
class ClassifierPredictor(Predictor):
    def predict_instance(self, instance: Instance) -> JsonDict:
        outputs = self._model.forward_on_instance(instance)
        label_vocab = self._model.vocab.get_index_to_token_vocabulary('labels')

        outputs['predicted'] = label_vocab[np.argmax(outputs["probs"])]
        # true labels
        if "label" in instance.fields:
            outputs['label'] = instance.fields['label'].label
        else:
            outputs['label'] = None
        outputs['topics'] = instance["cat_feats_list"][2].tokens
        num_outputs = instance.fields['feat'].array.reshape(1, -1)
        if self._dataset_reader.scaler is not None:
            num_outputs = self._dataset_reader.scaler.inverse_transform()
        num_outputs = num_outputs.flatten()
        outputs['year'] = num_outputs[2]
        outputs['n_outcomes'] = num_outputs[4]
        outputs['n_participants'] = num_outputs[0]
        outputs['n_studies'] = num_outputs[1]
        outputs['n_sofs'] = num_outputs[3]

        return sanitize(outputs)

    def dump_line(self, outputs: JsonDict) -> str:
        return f"{outputs['predicted']}\t{outputs['label']}\t{outputs['probs']}\t{outputs['topics']}\t{outputs['year']}\t{outputs['n_outcomes']}\t{outputs['n_participants']}\t{outputs['n_studies']}\t{outputs['n_sofs']}\n"
        # return str(outputs["predicted"]) + "\n"


@Predictor.register('attentive_classifier_predictor')
class AttentiveClassifierPredictor(Predictor):
    def predict_instance(self, instance: Instance) -> JsonDict:
        outputs = self._model.forward_on_instance(instance)
        label_vocab = self._model.vocab.get_index_to_token_vocabulary('labels')

        outputs['predicted'] = label_vocab[np.argmax(outputs["probs"])]
        # true labels
        outputs['label'] = instance.fields['label'].label
        outputs["max_population_in_studies"] = instance.fields["population_studies_list"][
            outputs["alphas_population"].argmax()].tokens
        outputs["max_intervention_in_studies"] = instance.fields["intervention_studies_list"][
            outputs["alphas_intervention"].argmax()].tokens
        outputs["max_outcome_in_studies"] = instance.fields["outcome_studies_list"][
            outputs["alphas_outcome"].argmax()].tokens
        outputs["population"] = instance.fields["population"].tokens
        outputs["intervention"] = instance.fields["intervention"].tokens
        outputs["comparator"] = instance.fields["comparator"].tokens
        outputs["outcome"] = instance.fields["outcome"].tokens
        outputs["alphas_desc"] = str(stats.describe(outputs['alphas']))
        outputs["alphas_population_desc"] = str(stats.describe(softmax(outputs['alphas_population'])))
        outputs["alphas_intervention_desc"] = str(stats.describe(softmax(outputs['alphas_intervention'])))
        outputs["alphas_outcome_desc"] = str(stats.describe(softmax(outputs['alphas_outcome'])))

        return sanitize(outputs)

    def dump_line(self, outputs: JsonDict) -> str:
        return f"{outputs['predicted']}\t{outputs['label']}\t{outputs['alphas_desc']}\t{outputs['alphas_population_desc']}\t{outputs['alphas_intervention_desc']}\t{outputs['alphas_outcome_desc']}\t{outputs['population']}\t{outputs['intervention']}\t{outputs['comparator']}\t{outputs['outcome']}\t{outputs['max_population_in_studies']}\t{outputs['max_intervention_in_studies']}\t{outputs['max_outcome_in_studies']}\n"


@Predictor.register('regressor_predictor')
class RegressorPredictor(Predictor):
    def predict_instance(self, instance: Instance) -> JsonDict:
        outputs = self._model.forward_on_instance(instance)
        label_vocab = self._model.index_to_label
        outputs["predicted"] = label_vocab[outputs["pred_labels"]]
        # true labels
        outputs['label'] = label_vocab[int(instance.fields['label'].array[0])]

        return sanitize(outputs)

    def dump_line(self, outputs: JsonDict) -> str:
        return f"{outputs['predicted']}\t{outputs['label']}\n"
