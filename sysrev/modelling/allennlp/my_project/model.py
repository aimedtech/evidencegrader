import logging
import random
from typing import Dict, Set

import numpy as np
import torch
from allennlp.data import Vocabulary
from allennlp.models import Model
from allennlp.modules import TextFieldEmbedder, FeedForward, LayerNorm, Seq2VecEncoder
from allennlp.modules.attention import BilinearAttention
from allennlp.nn import util, Activation
from allennlp.training.metrics import CategoricalAccuracy, FBetaMeasure, MeanAbsoluteError, FBetaMultiLabelMeasure
from torch.nn import Parameter

from sysrev.dataset_construction.dataset.statistics import get_grade_and_reasons
from sysrev.modelling.allennlp.my_project.loss import ordinal_loss2, mse_loss, LabelSmoothingLoss
from sysrev.modelling.allennlp.util import INV_MAPPING

logger = logging.getLogger(__name__)


class HLoss(torch.nn.Module):
    def __init__(self):
        super(HLoss, self).__init__()

    def forward(self, logits):
        p = torch.nn.functional.softmax(logits, dim=0)
        # p is a normalised array (prob. distribution)
        b = p * torch.nn.functional.log_softmax(logits, dim=0)
        b = -1.0 * b.sum(dim=0)

        # mean across batch elements
        return b.mean()


def get_reason_weights(index_to_label, device='cpu',
                       f='/home/ssuster/tmp_pycharm_project_989/data/derivations/all/splits/0/train.csv'):
    # get counts for grade and reason labels from the first CV train fold
    c_grade, c_reasons = get_grade_and_reasons(f)
    # overall counts needed for weights will be all the reasons and 'high' grade label (represented as 'NA')
    counts = c_reasons.copy()
    # counts["NA"] = c_grade["high"]
    max_count = max(counts.values())

    # prepare tensor
    weights = []
    for i in range(len(index_to_label)):
        label = index_to_label[i]
        weights.append(max_count / counts[label])

    return torch.tensor(weights, device=device)


def get_grade_weights(index_to_label, device='cpu',
                      f='/home/ssuster/tmp_pycharm_project_989/data/derivations/all/splits/0/train.csv'):
    # get counts for grade and reason labels from the first CV train fold
    c_grade, c_reasons = get_grade_and_reasons(f)
    # overall counts needed for weights will be all the reasons and 'high' grade label (represented as 'NA')
    counts = c_grade.copy()
    max_count = max(counts.values())

    # prepare tensor
    weights = []
    for i in range(len(index_to_label)):
        label = index_to_label[i]
        weights.append(max_count / counts[label])

    return torch.tensor(weights, device=device)


def get_fscore_results(macro_scores, fscore_average, index_to_label=None, add_name=""):
    add_txt = add_name + "_" if add_name else add_name
    if fscore_average is None:
        # per label scores
        assert isinstance(macro_scores["fscore"], list)
        num_labels = len(macro_scores["fscore"])

        results = {}
        for i in range(num_labels):
            if index_to_label is not None:
                label = index_to_label[i]
            else:
                label = i
            results[f"{add_txt}fscore_macro_class_{label}"] = macro_scores["fscore"][i]
            results[f"{add_txt}precision_macro_class_{label}"] = macro_scores["precision"][i]
            results[f"{add_txt}recall_macro_class_{label}"] = macro_scores["recall"][i]
    else:
        results = {f"{add_txt}fscore_macro": macro_scores["fscore"],
                   f"{add_txt}precision_macro": macro_scores["precision"],
                   f"{add_txt}recall_macro": macro_scores["recall"]}

    return results


def find_nearest(num_labels, x):
    """
    Find the nearest value in the range of num_labels to x.
    """
    lab_arr = np.array(range(num_labels))
    idx = np.abs(lab_arr - x.item()).argmin()
    return lab_arr[idx]


class Encoders(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 feedforward: FeedForward,
                 embedder: TextFieldEmbedder,
                 cat_feats_embedder: TextFieldEmbedder,
                 # encoder: Seq2VecEncoder,
                 encoder_cat_feats: Seq2VecEncoder,
                 metrics: Set,
                 feat_type: Dict[str, bool] = None,
                 ablated_feat: str = None,
                 ablated_feat_type: str = None,
                 reduced_output_bert_dim: int = None,
                 logreg: bool = False):
        super().__init__(vocab)
        self.feat_type = feat_type or {"num": True}
        self.ablated_feat = ablated_feat
        self.ablated_feat_type = ablated_feat_type  # num, cat, text
        # self.txt_dim = encoder.get_output_dim()
        self.reduced_output_bert_dim = reduced_output_bert_dim
        self.logreg = logreg
        self.encoder_cat_feats = None
        encoder_out_dim = 0
        if self.feat_type["text"]:
            self.embedder = embedder
            # self.encoder = encoder
            # encoder_out_dim = encoder.get_output_dim() * (4 - (1 if self.ablated_feat is not None and self.ablated_feat_type == "text" else 0))
            encoder_out_dim = embedder.get_output_dim() * (
                    4 - (1 if self.ablated_feat is not None and self.ablated_feat_type == "text" else 0))
            if self.reduced_output_bert_dim is not None:
                self.reduce_dim = torch.nn.Linear(encoder_out_dim, self.reduced_output_bert_dim)
                encoder_out_dim = self.reduced_output_bert_dim
                self.nonlinearity = torch.tanh
            else:
                self.reduce_dim = lambda x: x
                self.nonlinearity = lambda x: x

        cat_encoder_out_dim = 0
        if self.feat_type["cat"]:
            self.cat_feats_embedder = cat_feats_embedder
            self.encoder_cat_feats = encoder_cat_feats
            self.reduce_dim = lambda x: x
            self.nonlinearity = lambda x: x
            cat_encoder_out_dim = encoder_cat_feats.get_output_dim() * (
                    3 - (1 if self.ablated_feat is not None and self.ablated_feat_type == "cat" else 0))
            # cat_encoder_out_dim = cat_feats_embedder.get_output_dim() * (
            #        3 - (1 if self.ablated_feat is not None and self.ablated_feat_type == "cat" else 0))

        feedforward_out_dim = 0
        if self.feat_type["num"]:
            if self.logreg:
                self.feedforward = lambda x: x
                feedforward_out_dim = feedforward.get_input_dim()
            else:
                self.feedforward = feedforward
                feedforward_out_dim = feedforward.get_output_dim()
        self.num_labels = vocab.get_vocab_size("labels")
        if "labels" in vocab._index_to_token:
            self.index_to_label = vocab._index_to_token["labels"]
        else:
            self.index_to_label = INV_MAPPING
            # self.index_to_label = None

        self.classifier_input_dim = feedforward_out_dim + encoder_out_dim + cat_encoder_out_dim
        self.layer_norm = LayerNorm(self.classifier_input_dim)

        # metrics
        self.metrics = metrics
        if "accuracy" in metrics:
            self.accuracy = CategoricalAccuracy()
        if "top_accuracy" in metrics:
            self.top_accuracy = CategoricalAccuracy()
        if "fscore_macro" in metrics:
            self.fscore_average = "macro"
            self.fscore_macro = FBetaMeasure(average=self.fscore_average)
        if "fscore_all" in metrics:
            self.fscore_all = FBetaMeasure(average=None)
        if "top_fscore_macro" in metrics:
            self.fscore_average = "macro"
            self.top_fscore_macro = FBetaMeasure(average=self.fscore_average)
        if "top_fscore_all" in metrics:
            self.top_fscore_all = FBetaMeasure(average=None)
        if "multilabel_fscore_macro" in self.metrics:
            self.fscore_average = "macro"
            self.multilabel_fscore_macro = FBetaMultiLabelMeasure(average=self.fscore_average)
        if "multilabel_fscore_all" in self.metrics:
            self.multilabel_fscore_all = FBetaMultiLabelMeasure(average=None)
        if "mae" in metrics:
            self.mae = MeanAbsoluteError()
        if "top_mae" in metrics:
            self.top_mae = MeanAbsoluteError()

    def encode_text_list(self, text_list, concatenate=True, categorical=False):
        if categorical:
            embedder = self.cat_feats_embedder
            encoder = self.encoder_cat_feats
        else:
            embedder = self.embedder
            encoder = self.encoder

        embedded_text = embedder(text_list, num_wrapping_dims=1)
        mask = util.get_text_field_mask(text_list, num_wrapping_dims=1)
        len_text_list = embedded_text.shape[1]
        enc_txt = []
        for i in range(len_text_list):
            enc_txt.append(encoder(embedded_text[:, i, :, :], mask[:, i, :]))
        if concatenate:
            enc_txt = torch.cat(enc_txt, dim=1)
        else:
            enc_txt = torch.stack(enc_txt)

        enc_txt = self.nonlinearity(self.reduce_dim(enc_txt))

        return enc_txt

    def encode(self, feat, text_list, cat_feats_list):
        inputs = []
        if self.feat_type["text"]:
            # text encoding:
            # Shape: (batch_size, text_list_len, num_tokens, embedding_dim)
            # enc_txt = self.encode_text_list(text_list)
            # inputs.append(enc_txt)
            embedded_feat = self.embedder(text_list, num_wrapping_dims=1)
            enc_txt = embedded_feat.flatten(1)
            inputs.append(enc_txt)

        if self.feat_type["cat"]:
            # categorical feature encoding:
            embedded_cat_feat = self.cat_feats_embedder(cat_feats_list, num_wrapping_dims=1)
            if self.encoder_cat_feats is not None:
                mask = util.get_text_field_mask(cat_feats_list, num_wrapping_dims=1)
                len_cat_feats_list = embedded_cat_feat.shape[1]
                enc_cat_feats = []
                for i in range(len_cat_feats_list):
                    enc_cat_feats.append(self.encoder_cat_feats(embedded_cat_feat[:, i, :, :], mask[:, i, :]))
                enc_cat_feats = torch.cat(enc_cat_feats, dim=1)
            else:
                enc_cat_feats = embedded_cat_feat.flatten(1)
            inputs.append(enc_cat_feats)

        if self.feat_type["num"]:
            # feature encoding:
            if self.logreg:
                enc = feat
            else:
                enc = self.feedforward(feat)
            inputs.append(enc)

        classifier_inputs = torch.cat(inputs, 1)

        return classifier_inputs

    def update_metrics(self, logits, label, only=None):
        if "accuracy" in self.metrics:
            if only is not None:
                if "accuracy" in only:
                    self.accuracy(logits, label)
            else:
                self.accuracy(logits, label)
        if "top_accuracy" in self.metrics:
            if only is not None:
                if "top_accuracy" in only:
                    self.top_accuracy(logits, label)
            else:
                self.top_accuracy(logits, label)
        if "fscore_macro" in self.metrics:
            if only is not None:
                if "fscore_macro" in only:
                    self.fscore_macro(logits, label)
            else:
                self.fscore_macro(logits, label)
        if "fscore_all" in self.metrics:
            if only is not None:
                if "fscore_all" in only:
                    self.fscore_all(logits, label)
            else:
                self.fscore_all(logits, label)
        if "top_fscore_macro" in self.metrics:
            if only is not None:
                if "top_fscore_macro" in only:
                    self.top_fscore_macro(logits, label)
            else:
                self.top_fscore_macro(logits, label)
        if "top_fscore_all" in self.metrics:
            if only is not None:
                if "top_fscore_all" in only:
                    self.top_fscore_all(logits, label)
            else:
                self.top_fscore_all(logits, label)
        if "multilabel_fscore_macro" in self.metrics:
            if only is not None:
                if "multilabel_fscore_macro" in only:
                    self.multilabel_fscore_macro(logits, label)
            else:
                self.multilabel_fscore_macro(logits, label)
        if "multilabel_fscore_all" in self.metrics:
            if only is not None:
                if "multilabel_fscore_all" in only:
                    self.multilabel_fscore_all(logits, label)
            else:
                self.multilabel_fscore_all(logits, label)
        if "mae" in self.metrics:
            if label.ndim != logits.ndim and logits.shape[1] > 1:
                new_logits = logits.argmax(dim=1)
            else:
                new_logits = logits

            if only is not None:
                if "mae" in only:
                    self.mae(new_logits, label)
            else:
                self.mae(new_logits, label)
        if "top_mae" in self.metrics:
            if only is not None and "top_mae" not in only:
                pass
            else:
                # if label.ndim != 2:
                #    new_label = label.unsqueeze(1)
                # else:
                #    new_label = label
                if label.shape != logits.shape:
                    new_logits = logits.argmax(dim=1)
                else:
                    new_logits = logits

                if only is not None:
                    if "top_mae" in only:
                        self.top_mae(new_logits, label)
                else:
                    self.top_mae(new_logits, label)

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        results = {}
        if "accuracy" in self.metrics:
            results["accuracy"] = self.accuracy.get_metric(reset)
        if "top_accuracy" in self.metrics:
            results["top_accuracy"] = self.top_accuracy.get_metric(reset)
        if "fscore_macro" in self.metrics:
            macro_scores = self.fscore_macro.get_metric(reset)
            results.update(get_fscore_results(macro_scores, self.fscore_average))
        if "fscore_all" in self.metrics:
            all_scores = self.fscore_all.get_metric(reset)
            results.update(get_fscore_results(all_scores, None, self.index_to_label))
        if "top_fscore_macro" in self.metrics:
            if self.top_fscore_macro._total_sum is not None:
                macro_scores = self.top_fscore_macro.get_metric(reset)
                results.update(get_fscore_results(macro_scores, self.fscore_average, add_name="top"))
        if "top_fscore_all" in self.metrics:
            if self.top_fscore_all._total_sum is not None:
                all_scores = self.top_fscore_all.get_metric(reset)
                results.update(get_fscore_results(all_scores, None, self.index_to_label_top, add_name="top"))
        if "multilabel_fscore_macro" in self.metrics:
            if self.multilabel_fscore_macro._total_sum is not None:
                macro_scores = self.multilabel_fscore_macro.get_metric(reset)
                results.update(get_fscore_results(macro_scores, self.fscore_average))
        if "multilabel_fscore_all" in self.metrics:
            if self.multilabel_fscore_all._total_sum is not None:
                all_scores = self.multilabel_fscore_all.get_metric(reset)
                results.update(get_fscore_results(all_scores, None, self.index_to_label))
        if "mae" in self.metrics:
            if self.mae._total_count != 0:
                mae = self.mae.get_metric(reset)
                if isinstance(mae, dict):
                    score = mae["mae"]
                    if isinstance(score, float):
                        mae_score = score
                    else:
                        mae_score = score.item()
                else:
                    mae_score = mae.item()
                results["mae"] = mae_score
        if "top_mae" in self.metrics:
            if self.top_mae._total_count != 0:
                mae = self.top_mae.get_metric(reset)
                if isinstance(mae, dict):
                    if isinstance(mae["mae"], float):
                        mae_score = mae["mae"]
                    else:
                        mae_score = mae["mae"].item()
                else:
                    mae_score = mae.item()
                results["top_mae"] = mae_score
        if "loss_grade" in self.metrics:
            results["loss_grade"] = self.loss_grade.item()
        if "loss_reasons" in self.metrics:
            results["loss_reasons"] = self.loss_reasons.item()

        return results


@Model.register('classifier')
class Classifier(Encoders):
    def __init__(self, reduced_output_bert_dim=None, **kwargs):
        kwargs["metrics"] = {"accuracy", "fscore_macro", "fscore_all", "mae"}
        super().__init__(reduced_output_bert_dim=reduced_output_bert_dim, **kwargs)

        self.classifier = torch.nn.Linear(self.classifier_input_dim, self.num_labels)

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs)

        probs = torch.nn.functional.softmax(logits, dim=-1)
        output = {'probs': probs, 'logits': logits}
        if label is not None:
            self.update_metrics(logits, label)
            # output['loss'] = torch.nn.functional.cross_entropy(logits, label, weight=self.pos_weight.to(logits.device))
            output['loss'] = torch.nn.functional.cross_entropy(logits, label)
        return output


@Model.register('smoothed-classifier')
class SmoothedClassifier(Encoders):
    def __init__(self, reduced_output_bert_dim=None, label_smoothing=0.1, **kwargs):
        kwargs["metrics"] = {"accuracy", "fscore_macro", "fscore_all", "mae"}
        super().__init__(reduced_output_bert_dim=reduced_output_bert_dim, **kwargs)
        self.classifier = torch.nn.Linear(self.classifier_input_dim, self.num_labels)
        self.criterion = LabelSmoothingLoss(self.num_labels, label_smoothing)

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs)

        probs = torch.nn.functional.softmax(logits, dim=-1)
        output = {'probs': probs, 'logits': logits}
        if label is not None:
            self.update_metrics(logits, label)
            output['loss'] = self.criterion(logits, label)
        return output


@Model.register('attentive_classifier')
class AttentiveClassifier(Encoders):
    def __init__(self, weighted=True, leaveout_studies=False, **kwargs):
        kwargs["metrics"] = {"accuracy", "fscore_macro", "fscore_all", "mae"}
        super().__init__(**kwargs)

        # update classifier dim
        self.classifier_input_dim += self.encoder_cat_feats.get_output_dim() * (
                1 + 8 + 8)  # + methods + all RoB judgements + all RoB supports
        self.layer_norm = LayerNorm(self.classifier_input_dim)

        # use attention weights or not (simple average)
        self.weighted = weighted
        # if True, will ignore any primary study information, but still run only on those instances for which PS info was successfully extracted.
        self.leaveout_studies = leaveout_studies
        self.classifier = torch.nn.Linear(self.classifier_input_dim, self.num_labels)
        # self.cos = torch.nn.CosineSimilarity(dim=2, eps=1e-6)
        if self.weighted:
            self.attention_layer_population = BilinearAttention(self.encoder_cat_feats.get_output_dim(),
                                                                self.encoder_cat_feats.get_output_dim(),
                                                                activation=Activation.by_name("relu")(),
                                                                normalize=False)
            self.attention_layer_intervention = BilinearAttention(self.encoder_cat_feats.get_output_dim(),
                                                                  self.encoder_cat_feats.get_output_dim(),
                                                                  activation=Activation.by_name("relu")(),
                                                                  normalize=False)
            self.attention_layer_outcome = BilinearAttention(self.encoder_cat_feats.get_output_dim(),
                                                             self.encoder_cat_feats.get_output_dim(),
                                                             activation=Activation.by_name("relu")(), normalize=False)

            # additional loss to control peakiness of attention distribution
            self.entropy_loss = HLoss()
            # importance of entropy loss
            self.gamma = 0.01

    def encode_query(self, query):
        """
        A query is typically a pico description.
        """
        # text encoding:
        # Shape: (batch_size, text_list_len, num_tokens, embedding_dim)
        embedded = self.cat_feats_embedder(query)
        mask = util.get_text_field_mask(query)
        enc = self.encoder_cat_feats(embedded, mask)

        return enc

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                population: torch.Tensor = None,
                intervention: torch.Tensor = None,
                comparator: torch.Tensor = None,
                outcome: torch.Tensor = None,
                population_studies_list: Dict[str, torch.Tensor] = None,
                intervention_studies_list: Dict[str, torch.Tensor] = None,
                outcome_studies_list: Dict[str, torch.Tensor] = None,
                methods_studies_list: Dict[str, torch.Tensor] = None,
                alloc_studies_list: Dict[str, torch.Tensor] = None,
                incom_studies_list: Dict[str, torch.Tensor] = None,
                other_studies_list: Dict[str, torch.Tensor] = None,
                outco_studies_list: Dict[str, torch.Tensor] = None,
                partb_studies_list: Dict[str, torch.Tensor] = None,
                blind_studies_list: Dict[str, torch.Tensor] = None,
                rands_studies_list: Dict[str, torch.Tensor] = None,
                selec_studies_list: Dict[str, torch.Tensor] = None,
                alloc_studies_supp_list: Dict[str, torch.Tensor] = None,
                incom_studies_supp_list: Dict[str, torch.Tensor] = None,
                other_studies_supp_list: Dict[str, torch.Tensor] = None,
                outco_studies_supp_list: Dict[str, torch.Tensor] = None,
                partb_studies_supp_list: Dict[str, torch.Tensor] = None,
                blind_studies_supp_list: Dict[str, torch.Tensor] = None,
                rands_studies_supp_list: Dict[str, torch.Tensor] = None,
                selec_studies_supp_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        if self.weighted:
            # 1) encode pico from SoF
            query_enc_population = self.encode_query(population)  # batch_size * enc_dim
            query_enc_intervention = self.encode_query(intervention)  # batch_size * enc_dim
            # query_enc_comparator = self.encode_query(comparator)  # batch_size * enc_dim
            query_enc_outcome = self.encode_query(outcome)  # batch_size * enc_dim
            # 2) encode pico texts from included studies for that SR
            population_studies_enc = self.encode_text_list(population_studies_list, concatenate=False,
                                                           categorical=True)  # n_studies * batch_size * enc_dim
            intervention_studies_enc = self.encode_text_list(intervention_studies_list, concatenate=False,
                                                             categorical=True)  # n_studies * batch_size * enc_dim
            outcome_studies_enc = self.encode_text_list(outcome_studies_list, concatenate=False,
                                                        categorical=True)  # n_studies * batch_size * enc_dim
            # 3) attention weights based on compatibility between 1) and 2)
            # alphas_population = torch.softmax(self.cos(query_enc_population.unsqueeze(0), population_studies_enc), 0)  # n_studies * batch_size
            # alphas_intervention = torch.softmax(self.cos(query_enc_intervention.unsqueeze(0), intervention_studies_enc), 0)  # n_studies * batch_size
            # alphas_outcome = torch.softmax(self.cos(query_enc_outcome.unsqueeze(0), outcome_studies_enc), 0)  # n_studies * batch_size

            alphas_population = self.attention_layer_population(query_enc_population,
                                                                population_studies_enc.permute(1, 0, 2)).permute(1,
                                                                                                                 0)  # n_studies * batch_size
            alphas_intervention = self.attention_layer_intervention(query_enc_intervention,
                                                                    intervention_studies_enc.permute(1, 0, 2)).permute(
                1, 0)  # n_studies * batch_size
            alphas_outcome = self.attention_layer_outcome(query_enc_outcome,
                                                          outcome_studies_enc.permute(1, 0, 2)).permute(1,
                                                                                                        0)  # n_studies * batch_size
            alphas = torch.softmax(alphas_population + alphas_intervention + alphas_outcome,
                                   0)  # n_studies * batch_size
        else:
            alphas = None

        # encode methods from individual studies
        methods_studies_enc = self.encode_text_list(methods_studies_list, concatenate=False,
                                                    categorical=True)  # n_studies * batch_size * enc_dim

        # encode RoB criteria
        alloc_studies_enc = self.encode_text_list(alloc_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        incom_studies_enc = self.encode_text_list(incom_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        other_studies_enc = self.encode_text_list(other_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        outco_studies_enc = self.encode_text_list(outco_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        partb_studies_enc = self.encode_text_list(partb_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        blind_studies_enc = self.encode_text_list(blind_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        rands_studies_enc = self.encode_text_list(rands_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        selec_studies_enc = self.encode_text_list(selec_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        alloc_studies_supp_enc = self.encode_text_list(alloc_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        incom_studies_supp_enc = self.encode_text_list(incom_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        other_studies_supp_enc = self.encode_text_list(other_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        outco_studies_supp_enc = self.encode_text_list(outco_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        partb_studies_supp_enc = self.encode_text_list(partb_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        blind_studies_supp_enc = self.encode_text_list(blind_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        rands_studies_supp_enc = self.encode_text_list(rands_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        selec_studies_supp_enc = self.encode_text_list(selec_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim

        # apply attention weights to the study methods
        if self.weighted:
            methods_studies_aggregate = torch.sum(alphas.unsqueeze(2) * methods_studies_enc,
                                                  dim=0)  # batch_size * enc_dim
        else:
            methods_studies_aggregate = torch.mean(methods_studies_enc, dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                methods_studies_aggregate = torch.rand_like(methods_studies_aggregate)

        if self.weighted:
            # apply attention weights to the RoB criteria
            alloc_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * alloc_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            incom_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * incom_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            other_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * other_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            outco_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * outco_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            partb_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * partb_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            blind_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * blind_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            rands_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * rands_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            selec_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * selec_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            alloc_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * alloc_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            incom_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * incom_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            other_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * other_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            outco_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * outco_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            partb_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * partb_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            blind_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * blind_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            rands_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * rands_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            selec_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * selec_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
        else:
            alloc_studies_weighted_aggregate = torch.mean(alloc_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                alloc_studies_weighted_aggregate = torch.rand_like(alloc_studies_weighted_aggregate)

            incom_studies_weighted_aggregate = torch.mean(incom_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                incom_studies_weighted_aggregate = torch.rand_like(incom_studies_weighted_aggregate)

            other_studies_weighted_aggregate = torch.mean(other_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                other_studies_weighted_aggregate = torch.rand_like(other_studies_weighted_aggregate)

            outco_studies_weighted_aggregate = torch.mean(outco_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                outco_studies_weighted_aggregate = torch.rand_like(outco_studies_weighted_aggregate
                                                                   )
            partb_studies_weighted_aggregate = torch.mean(partb_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                partb_studies_weighted_aggregate = torch.rand_like(partb_studies_weighted_aggregate)

            blind_studies_weighted_aggregate = torch.mean(blind_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                blind_studies_weighted_aggregate = torch.rand_like(blind_studies_weighted_aggregate)

            rands_studies_weighted_aggregate = torch.mean(rands_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                rands_studies_weighted_aggregate = torch.rand_like(rands_studies_weighted_aggregate)

            selec_studies_weighted_aggregate = torch.mean(selec_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                selec_studies_weighted_aggregate = torch.rand_like(selec_studies_weighted_aggregate)

            alloc_studies_supp_weighted_aggregate = torch.mean(alloc_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                alloc_studies_supp_weighted_aggregate = torch.rand_like(alloc_studies_supp_weighted_aggregate)

            incom_studies_supp_weighted_aggregate = torch.mean(incom_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                incom_studies_supp_weighted_aggregate = torch.rand_like(incom_studies_supp_weighted_aggregate)

            other_studies_supp_weighted_aggregate = torch.mean(other_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                other_studies_supp_weighted_aggregate = torch.rand_like(other_studies_supp_weighted_aggregate)

            outco_studies_supp_weighted_aggregate = torch.mean(outco_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                outco_studies_supp_weighted_aggregate = torch.rand_like(outco_studies_supp_weighted_aggregate)

            partb_studies_supp_weighted_aggregate = torch.mean(partb_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                partb_studies_supp_weighted_aggregate = torch.rand_like(partb_studies_supp_weighted_aggregate)

            blind_studies_supp_weighted_aggregate = torch.mean(blind_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                blind_studies_supp_weighted_aggregate = torch.rand_like(blind_studies_supp_weighted_aggregate)

            rands_studies_supp_weighted_aggregate = torch.mean(rands_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                rands_studies_supp_weighted_aggregate = torch.rand_like(rands_studies_supp_weighted_aggregate)

            selec_studies_supp_weighted_aggregate = torch.mean(selec_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                selec_studies_supp_weighted_aggregate = torch.rand_like(selec_studies_supp_weighted_aggregate)

        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = torch.cat([classifier_inputs,
                                       methods_studies_aggregate,
                                       alloc_studies_weighted_aggregate,
                                       incom_studies_weighted_aggregate,
                                       other_studies_weighted_aggregate,
                                       outco_studies_weighted_aggregate,
                                       partb_studies_weighted_aggregate,
                                       blind_studies_weighted_aggregate,
                                       rands_studies_weighted_aggregate,
                                       selec_studies_weighted_aggregate,
                                       alloc_studies_supp_weighted_aggregate,
                                       incom_studies_supp_weighted_aggregate,
                                       other_studies_supp_weighted_aggregate,
                                       outco_studies_supp_weighted_aggregate,
                                       partb_studies_supp_weighted_aggregate,
                                       blind_studies_supp_weighted_aggregate,
                                       rands_studies_supp_weighted_aggregate,
                                       selec_studies_supp_weighted_aggregate
                                       ], 1)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs)

        probs = torch.nn.functional.softmax(logits, dim=-1)
        if self.weighted:
            output = {'probs': probs, 'logits': logits, 'alphas': alphas.permute((1, 0)),
                      'alphas_population': alphas_population.permute((1, 0)),
                      'alphas_intervention': alphas_intervention.permute((1, 0)),
                      'alphas_outcome': alphas_outcome.permute((1, 0))}
        else:
            output = {'probs': probs, 'logits': logits}
        if label is not None:
            self.update_metrics(logits, label)
            crossentropy_loss = torch.nn.functional.cross_entropy(logits, label)
            if self.weighted:
                entropy_loss = self.entropy_loss(alphas_population) + self.entropy_loss(
                    alphas_intervention) + self.entropy_loss(alphas_outcome)
                logger.info(f"Crossentropy_loss: {crossentropy_loss}")
                logger.info(f"Entropy_loss: {entropy_loss}")
                output['loss'] = (1 - self.gamma) * crossentropy_loss + self.gamma * entropy_loss
                # output['loss'] = entropy_loss
            else:
                output['loss'] = crossentropy_loss
        return output


@Model.register('classifier_multilabel')
class ClassifierMultilabel(Encoders):
    def __init__(self, other_metrics=None, **kwargs):
        metrics = {"multilabel_fscore_macro", "multilabel_fscore_all"}
        if other_metrics is not None:
            metrics.update(other_metrics)
        kwargs["metrics"] = metrics
        reason_weights_f = kwargs["reason_weights_f"]
        del kwargs["reason_weights_f"]
        super().__init__(**kwargs)
        self.classifier = torch.nn.Linear(self.classifier_input_dim, self.num_labels)
        pos_weight = get_reason_weights(self.index_to_label, device=self.classifier.weight.device.type,
                                        f=reason_weights_f)
        self.loss_f = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight)

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs)

        output = {'logits': logits}
        if label is not None:
            self.update_metrics(logits, label)
            # BCE loss requires labels as floats:
            output['loss'] = self.loss_f(logits, label.type_as(logits))
        return output


@Model.register('hierarchical_classifier')
class HierarchicalClassifier(ClassifierMultilabel):
    """
    Extends the multilabel (Reasons) classifier with an additional output layer for final GRADE prediction.
    """

    def __init__(self, multitask=False, **kwargs):
        # metrics for the top output layer (GRADE)
        metrics = {"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"}
        super().__init__(other_metrics=metrics, **kwargs)

        # if multitask, the logits from the first (reasons) classifier are not fed to the top (GRADE) classifier
        self.multitask = multitask
        if multitask:
            classifier_top_input_dim = 0
        else:
            # multilabel (reasons) output dim
            classifier_top_input_dim = self.classifier.out_features

        # final classifier
        num_labels_top = kwargs["vocab"].get_vocab_size("top_labels")
        self.classifier_top = torch.nn.Linear(classifier_top_input_dim + self.classifier_input_dim, num_labels_top)

        self.index_to_label_top = self.vocab._index_to_token["top_labels"]

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None,
                top_label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        # Shape: (batch_size, num_labels)
        logits = self.classifier(classifier_inputs)
        if self.multitask:
            logits_top = self.classifier_top(classifier_inputs)
        else:
            logits_top = self.classifier_top(torch.cat([logits, classifier_inputs], 1))

        probs_top = torch.nn.functional.softmax(logits_top, dim=-1)

        output = {'logits': logits, 'probs_top': probs_top, 'loss': 0}

        # multilabel output layer (reasons)
        if label is not None:
            self.update_metrics(logits, label, only={"multilabel_fscore_macro", "multilabel_fscore_all"})
            # BCE loss requires labels as floats:
            output['loss'] += self.loss_f(logits, label.type_as(logits))

        # top output layer (GRADE)
        if top_label is not None:
            self.update_metrics(logits_top, top_label,
                                only={"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"})
            output['loss'] += torch.nn.functional.cross_entropy(logits_top, top_label)

        return output


@Model.register('hierarchical_alternating_classifier')
class HierarchicalAlternatingClassifier(ClassifierMultilabel):
    """
    Extends the multilabel (Reasons) classifier with an additional output layer for final GRADE prediction. We alternate
    between predicting the GRADE score (and taking the loss) based on predicted reasons; and predicting reasons (and
    taking the loss) based on the predicted GRADE score.
    """

    def __init__(self, multitask=False, **kwargs):
        # metrics for the top output layer (GRADE)
        metrics = {"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"}
        super().__init__(other_metrics=metrics, **kwargs)

        # if multitask, the logits from the first (reasons) classifier are not fed to the top (GRADE) classifier
        self.multitask = multitask
        if multitask:
            classifier_top_input_dim = 0
        else:
            # multilabel (reasons) output dim
            classifier_top_input_dim = self.classifier.out_features

        # final classifier
        num_labels_top = kwargs["vocab"].get_vocab_size("top_labels")
        # self.classifier_top = torch.nn.Linear(classifier_top_input_dim + self.classifier_input_dim, num_labels_top)
        self.classifier_top = torch.nn.Linear(self.classifier_input_dim, num_labels_top)
        self.pre_grade_linear = torch.nn.Linear(self.classifier_input_dim + classifier_top_input_dim,
                                                self.classifier_input_dim)
        self.pre_reasons_linear = torch.nn.Linear(self.classifier_input_dim + self.classifier_top.out_features,
                                                  self.classifier_input_dim)
        self.index_to_label_top = self.vocab._index_to_token["top_labels"]

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None,
                top_label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)
        output = {'probs_top': None, 'logits_alternate': None, 'loss': 0}

        # random switch to alternate between predicting GRADE/reasons at the final layer
        if random.randint(0, 1):
            # Shape: (batch_size, num_labels)
            logits = self.classifier(classifier_inputs)
            if self.multitask:
                logits_top = self.classifier_top(classifier_inputs)
            else:
                logits_top = self.classifier_top(self.pre_grade_linear(torch.cat([logits, classifier_inputs], 1)))

            probs_top = torch.nn.functional.softmax(logits_top, dim=-1)
            output['probs_top'] = probs_top

            # multilabel output layer (reasons)
            # if label is not None:
            #    self.update_metrics(logits, label, only={"multilabel_fscore_macro", "multilabel_fscore_all"})
            #    # BCE loss requires labels as floats:
            #    output['loss'] += self.loss_f(logits, label.type_as(logits))

            # top output layer (GRADE)
            if top_label is not None:
                self.update_metrics(logits_top, top_label,
                                    only={"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"})
                output['loss'] += torch.nn.functional.cross_entropy(logits_top, top_label)

        # alternate direction:
        # Shape: (batch_size, num_labels)
        else:
            logits_top = self.classifier_top(classifier_inputs)
            if self.multitask:
                logits = self.classifier(classifier_inputs)
            else:
                logits = self.classifier(self.pre_reasons_linear(torch.cat([logits_top, classifier_inputs], 1)))

            output['logits_alternate'] = logits
            if label is not None:
                self.update_metrics(logits, label, only={"multilabel_fscore_macro", "multilabel_fscore_all"})
                # BCE loss requires labels as floats:
                output['loss'] += self.loss_f(logits, label.type_as(logits))

        return output


@Model.register('hierarchical_interactive_classifier')
class HierarchicalInteractiveClassifier(ClassifierMultilabel):
    """
    Extends the multilabel (Reasons) classifier with an additional output layer for final GRADE prediction. In contrast
    to HierarchicalClassifier, allows interaction between Reasons and GRADE.
    """

    def __init__(self, multitask=False, **kwargs):
        # metrics for the top output layer (GRADE)
        metrics = {"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae", "loss_grade", "loss_reasons"}
        super().__init__(other_metrics=metrics, **kwargs)

        # num of grade labels
        num_labels_top = kwargs["vocab"].get_vocab_size("top_labels")

        self.out_features = 12
        self.interaction_layer = torch.nn.Bilinear(num_labels_top, self.num_labels, self.out_features)
        # BilinearAttention(self.encoder_cat_feats.get_output_dim(), self.encoder_cat_feats.get_output_dim(), activation=Activation.by_name("relu")(), normalize=False)
        self.interaction_nonlinearity = torch.tanh
        interaction_output_dim = self.interaction_layer.out_features
        self.classifier_grade = torch.nn.Bilinear(self.classifier_input_dim, interaction_output_dim, num_labels_top)
        self.classifier_reasons = torch.nn.Bilinear(self.classifier_input_dim, interaction_output_dim, self.num_labels)
        # self.dummy_logits_grade = Parameter(torch.rand((classifier_inputs.shape[0], self.classifier_grade.out_features), device=self.interaction_layer.weight.device))
        # self.dummy_logits_reasons = Parameter(torch.rand((classifier_inputs.shape[0], self.classifier_reasons.out_features), device=self.interaction_layer.weight.device))
        self.dummy_logits_grade = Parameter(torch.rand((self.classifier_grade.out_features),
                                                       device=self.interaction_layer.weight.device))
        self.dummy_logits_reasons = Parameter(
            torch.rand((self.classifier_reasons.out_features), device=self.interaction_layer.weight.device))

        self.index_to_label_top = self.vocab._index_to_token["top_labels"]

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None,
                top_label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        # uninformed interaction outputs
        pre_interaction_outputs = self.interaction_nonlinearity(
            self.interaction_layer(self.dummy_logits_grade.repeat(classifier_inputs.shape[0], 1),
                                   self.dummy_logits_reasons.repeat(classifier_inputs.shape[0], 1)))
        # get independent grade logits
        pre_logits_grade = self.classifier_grade(classifier_inputs, pre_interaction_outputs)
        # get independent reasons logits
        pre_logits_reasons = self.classifier_reasons(classifier_inputs, pre_interaction_outputs)

        interaction_outputs = self.interaction_nonlinearity(
            self.interaction_layer(pre_logits_grade, pre_logits_reasons))
        # get grade logits based on interaction with reasons
        logits_grade = self.classifier_grade(classifier_inputs, interaction_outputs)
        probs_grade = torch.nn.functional.softmax(logits_grade, dim=-1)

        # get reasons logits based on interaction with grade
        logits_reasons = self.classifier_reasons(classifier_inputs, interaction_outputs)
        probs_reasons = torch.nn.functional.softmax(logits_reasons, dim=-1)

        output = {'logits_grade': logits_grade, 'pre_logits_grade': pre_logits_grade, 'logits_reasons': logits_reasons,
                  'pre_logits_reasons': pre_logits_reasons, 'probs_grade': probs_grade, 'probs_reasons': probs_reasons}

        # multilabel output layer (reasons)
        if label is not None:
            # BCE loss requires labels as floats:
            self.loss_reasons = self.loss_f(logits_reasons, label.type_as(logits_reasons))
            if "loss" not in output:
                output['loss'] = 0
            output['loss'] += self.loss_reasons
            output['loss_reasons'] = self.loss_reasons
            self.update_metrics(logits_reasons, label, only={"multilabel_fscore_macro", "multilabel_fscore_all"})

        # top output layer (GRADE)
        if top_label is not None:
            self.loss_grade = torch.nn.functional.cross_entropy(logits_grade, top_label)
            if "loss" not in output:
                output['loss'] = 0
            output['loss'] += self.loss_grade
            output['loss_grade'] = self.loss_grade
            self.update_metrics(logits_grade, top_label,
                                only={"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"})

        return output


@Model.register('attentive_classifier_multilabel')
class AttentiveClassifierMultilabel(Encoders):
    def __init__(self, weighted=True, leaveout_studies=False, other_metrics=None, **kwargs):
        metrics = {"multilabel_fscore_macro", "multilabel_fscore_all"}
        if other_metrics is not None:
            metrics.update(other_metrics)
        kwargs["metrics"] = metrics
        super().__init__(**kwargs)

        # update classifier dim
        self.classifier_input_dim += self.encoder_cat_feats.get_output_dim() * (
                1 + 8 + 8)  # + methods + all RoB judgements + all RoB supports
        self.layer_norm = LayerNorm(self.classifier_input_dim)

        # use attention weights or not (simple average)
        self.weighted = weighted
        # if True, will ignore any primary study information, but still run only on those instances for which PS info was successfully extracted.
        self.leaveout_studies = leaveout_studies
        self.classifier = torch.nn.Linear(self.classifier_input_dim, self.num_labels)
        self.cos = torch.nn.CosineSimilarity(dim=2, eps=1e-6)
        pos_weight = get_reason_weights(self.index_to_label, device=self.classifier.weight.device.type)
        self.loss_f = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight)

    def encode_query(self, query):
        """
        A query is typically a pico description.
        """
        # text encoding:
        # Shape: (batch_size, text_list_len, num_tokens, embedding_dim)
        embedded = self.cat_feats_embedder(query)
        mask = util.get_text_field_mask(query)
        enc = self.encoder_cat_feats(embedded, mask)

        return enc

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                population: torch.Tensor = None,
                intervention: torch.Tensor = None,
                comparator: torch.Tensor = None,
                outcome: torch.Tensor = None,
                population_studies_list: Dict[str, torch.Tensor] = None,
                intervention_studies_list: Dict[str, torch.Tensor] = None,
                outcome_studies_list: Dict[str, torch.Tensor] = None,
                methods_studies_list: Dict[str, torch.Tensor] = None,
                alloc_studies_list: Dict[str, torch.Tensor] = None,
                incom_studies_list: Dict[str, torch.Tensor] = None,
                other_studies_list: Dict[str, torch.Tensor] = None,
                outco_studies_list: Dict[str, torch.Tensor] = None,
                partb_studies_list: Dict[str, torch.Tensor] = None,
                blind_studies_list: Dict[str, torch.Tensor] = None,
                rands_studies_list: Dict[str, torch.Tensor] = None,
                selec_studies_list: Dict[str, torch.Tensor] = None,
                alloc_studies_supp_list: Dict[str, torch.Tensor] = None,
                incom_studies_supp_list: Dict[str, torch.Tensor] = None,
                other_studies_supp_list: Dict[str, torch.Tensor] = None,
                outco_studies_supp_list: Dict[str, torch.Tensor] = None,
                partb_studies_supp_list: Dict[str, torch.Tensor] = None,
                blind_studies_supp_list: Dict[str, torch.Tensor] = None,
                rands_studies_supp_list: Dict[str, torch.Tensor] = None,
                selec_studies_supp_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        if self.weighted:
            # 1) encode pico from SoF
            query_enc_population = self.encode_query(population)  # batch_size * enc_dim
            query_enc_intervention = self.encode_query(intervention)  # batch_size * enc_dim
            # query_enc_comparator = self.encode_query(comparator)  # batch_size * enc_dim
            query_enc_outcome = self.encode_query(outcome)  # batch_size * enc_dim
            # 2) encode pico texts from included studies for that SR
            population_studies_enc = self.encode_text_list(population_studies_list, concatenate=False,
                                                           categorical=True)  # n_studies * batch_size * enc_dim
            intervention_studies_enc = self.encode_text_list(intervention_studies_list, concatenate=False,
                                                             categorical=True)  # n_studies * batch_size * enc_dim
            outcome_studies_enc = self.encode_text_list(outcome_studies_list, concatenate=False,
                                                        categorical=True)  # n_studies * batch_size * enc_dim
            # 3) attention weights based on compatibility between 1) and 2)
            alphas_population = torch.softmax(self.cos(query_enc_population.unsqueeze(0), population_studies_enc),
                                              0)  # n_studies * batch_size
            alphas_intervention = torch.softmax(self.cos(query_enc_intervention.unsqueeze(0), intervention_studies_enc),
                                                0)  # n_studies * batch_size
            alphas_outcome = torch.softmax(self.cos(query_enc_outcome.unsqueeze(0), outcome_studies_enc),
                                           0)  # n_studies * batch_size
            alphas = torch.softmax(alphas_population + alphas_intervention + alphas_outcome, 0)
        else:
            alphas = None

        # encode methods from individual studies
        methods_studies_enc = self.encode_text_list(methods_studies_list, concatenate=False,
                                                    categorical=True)  # n_studies * batch_size * enc_dim

        # encode RoB criteria
        alloc_studies_enc = self.encode_text_list(alloc_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        incom_studies_enc = self.encode_text_list(incom_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        other_studies_enc = self.encode_text_list(other_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        outco_studies_enc = self.encode_text_list(outco_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        partb_studies_enc = self.encode_text_list(partb_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        blind_studies_enc = self.encode_text_list(blind_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        rands_studies_enc = self.encode_text_list(rands_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        selec_studies_enc = self.encode_text_list(selec_studies_list, concatenate=False,
                                                  categorical=True)  # n_studies * batch_size * enc_dim
        alloc_studies_supp_enc = self.encode_text_list(alloc_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        incom_studies_supp_enc = self.encode_text_list(incom_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        other_studies_supp_enc = self.encode_text_list(other_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        outco_studies_supp_enc = self.encode_text_list(outco_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        partb_studies_supp_enc = self.encode_text_list(partb_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        blind_studies_supp_enc = self.encode_text_list(blind_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        rands_studies_supp_enc = self.encode_text_list(rands_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim
        selec_studies_supp_enc = self.encode_text_list(selec_studies_supp_list, concatenate=False,
                                                       categorical=True)  # n_studies * batch_size * enc_dim

        # apply attention weights to the study methods
        if self.weighted:
            methods_studies_aggregate = torch.sum(alphas.unsqueeze(2) * methods_studies_enc,
                                                  dim=0)  # batch_size * enc_dim
        else:
            methods_studies_aggregate = torch.mean(methods_studies_enc, dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                methods_studies_aggregate = torch.rand_like(methods_studies_aggregate)

        if self.weighted:
            # apply attention weights to the RoB criteria
            alloc_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * alloc_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            incom_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * incom_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            other_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * other_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            outco_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * outco_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            partb_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * partb_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            blind_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * blind_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            rands_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * rands_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            selec_studies_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * selec_studies_enc,
                                                         dim=0)  # batch_size * enc_dim
            alloc_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * alloc_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            incom_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * incom_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            other_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * other_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            outco_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * outco_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            partb_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * partb_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            blind_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * blind_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            rands_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * rands_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
            selec_studies_supp_weighted_aggregate = torch.sum(alphas.unsqueeze(2) * selec_studies_supp_enc,
                                                              dim=0)  # batch_size * enc_dim
        else:
            alloc_studies_weighted_aggregate = torch.mean(alloc_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                alloc_studies_weighted_aggregate = torch.rand_like(alloc_studies_weighted_aggregate)

            incom_studies_weighted_aggregate = torch.mean(incom_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                incom_studies_weighted_aggregate = torch.rand_like(incom_studies_weighted_aggregate)

            other_studies_weighted_aggregate = torch.mean(other_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                other_studies_weighted_aggregate = torch.rand_like(other_studies_weighted_aggregate)

            outco_studies_weighted_aggregate = torch.mean(outco_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                outco_studies_weighted_aggregate = torch.rand_like(outco_studies_weighted_aggregate
                                                                   )
            partb_studies_weighted_aggregate = torch.mean(partb_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                partb_studies_weighted_aggregate = torch.rand_like(partb_studies_weighted_aggregate)

            blind_studies_weighted_aggregate = torch.mean(blind_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                blind_studies_weighted_aggregate = torch.rand_like(blind_studies_weighted_aggregate)

            rands_studies_weighted_aggregate = torch.mean(rands_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                rands_studies_weighted_aggregate = torch.rand_like(rands_studies_weighted_aggregate)

            selec_studies_weighted_aggregate = torch.mean(selec_studies_enc,
                                                          dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                selec_studies_weighted_aggregate = torch.rand_like(selec_studies_weighted_aggregate)

            alloc_studies_supp_weighted_aggregate = torch.mean(alloc_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                alloc_studies_supp_weighted_aggregate = torch.rand_like(alloc_studies_supp_weighted_aggregate)

            incom_studies_supp_weighted_aggregate = torch.mean(incom_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                incom_studies_supp_weighted_aggregate = torch.rand_like(incom_studies_supp_weighted_aggregate)

            other_studies_supp_weighted_aggregate = torch.mean(other_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                other_studies_supp_weighted_aggregate = torch.rand_like(other_studies_supp_weighted_aggregate)

            outco_studies_supp_weighted_aggregate = torch.mean(outco_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                outco_studies_supp_weighted_aggregate = torch.rand_like(outco_studies_supp_weighted_aggregate)

            partb_studies_supp_weighted_aggregate = torch.mean(partb_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                partb_studies_supp_weighted_aggregate = torch.rand_like(partb_studies_supp_weighted_aggregate)

            blind_studies_supp_weighted_aggregate = torch.mean(blind_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                blind_studies_supp_weighted_aggregate = torch.rand_like(blind_studies_supp_weighted_aggregate)

            rands_studies_supp_weighted_aggregate = torch.mean(rands_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                rands_studies_supp_weighted_aggregate = torch.rand_like(rands_studies_supp_weighted_aggregate)

            selec_studies_supp_weighted_aggregate = torch.mean(selec_studies_supp_enc,
                                                               dim=0)  # batch_size * enc_dim
            if self.leaveout_studies:
                selec_studies_supp_weighted_aggregate = torch.rand_like(selec_studies_supp_weighted_aggregate)

        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = torch.cat([classifier_inputs,
                                       methods_studies_aggregate,
                                       alloc_studies_weighted_aggregate,
                                       incom_studies_weighted_aggregate,
                                       other_studies_weighted_aggregate,
                                       outco_studies_weighted_aggregate,
                                       partb_studies_weighted_aggregate,
                                       blind_studies_weighted_aggregate,
                                       rands_studies_weighted_aggregate,
                                       selec_studies_weighted_aggregate,
                                       alloc_studies_supp_weighted_aggregate,
                                       incom_studies_supp_weighted_aggregate,
                                       other_studies_supp_weighted_aggregate,
                                       outco_studies_supp_weighted_aggregate,
                                       partb_studies_supp_weighted_aggregate,
                                       blind_studies_supp_weighted_aggregate,
                                       rands_studies_supp_weighted_aggregate,
                                       selec_studies_supp_weighted_aggregate
                                       ], 1)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs)
        if self.weighted:
            output = {'logits': logits, 'alphas': alphas.permute((1, 0)),
                      'alphas_population': alphas_population.permute((1, 0)),
                      'alphas_intervention': alphas_intervention.permute((1, 0)),
                      'alphas_outcome': alphas_outcome.permute((1, 0))}
        else:
            output = {'logits': logits}
        if label is not None:
            self.update_metrics(logits, label)
            output['loss'] = self.loss_f(logits, label.type_as(logits))
        return output


@Model.register('ordinal_regressor')
class OrdinalRegressor(Encoders):
    def __init__(self, **kwargs):
        kwargs["metrics"] = {"accuracy", "fscore_macro", "fscore_all", "mae"}
        super().__init__(**kwargs)

        self.num_labels = 4
        self.classifier = torch.nn.Linear(self.classifier_input_dim, 1, bias=False)
        self.linear_1_bias = Parameter(torch.zeros(self.num_labels - 1).float())
        self.importance_weights = torch.ones(self.num_labels - 1).float()  # equal weight for all labels
        self.a = 0

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs) + self.linear_1_bias
        probs = torch.sigmoid(logits)
        output = {'probs': probs}
        if label is not None:
            pred_levels = probs > 0.5
            pred_labels = torch.sum(pred_levels, dim=1)
            # broadcast:
            pred_probs = torch.zeros((pred_levels.shape[0], self.num_labels))
            pred_probs[range(pred_levels.shape[0]), pred_labels] = 1.
            gold_labels = torch.sum(label, dim=1).type(torch.LongTensor)
            self.update_metrics(pred_probs, gold_labels)
            output['loss'] = ordinal_loss2(logits, label, self.importance_weights)
        return output


@Model.register('regressor')
class Regressor(Encoders):
    """
    Simple regression
    """

    def __init__(self, other_metrics=None, **kwargs):
        # kwargs["metrics"] = {"accuracy", "fscore_macro", "fscore_all", "mae"}
        metrics = {"mae", "fscore_macro", "fscore_all"}
        if other_metrics is not None:
            metrics.update(other_metrics)
        kwargs["metrics"] = metrics
        super().__init__(**kwargs)
        self.num_labels = 4
        self.classifier = torch.nn.Linear(self.classifier_input_dim, 1)

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        logits = self.classifier(classifier_inputs)
        output = {'probs': logits}
        if label is not None:
            # broadcast:
            pred_probs = torch.zeros((logits.shape[0], self.num_labels)).to(logits.device)
            pred_labels = [find_nearest(self.num_labels, x) for x in logits]
            output["pred_labels"] = pred_labels
            pred_probs[range(logits.shape[0]), pred_labels] = 1.
            gold_labels = torch.sum(label, dim=1).int()
            self.update_metrics(pred_probs, gold_labels, only={"fscore_macro", "fscore_all"})
            self.update_metrics(logits, label, only="mae")
            output['loss'] = mse_loss(logits, label)
        return output


@Model.register('hierarchical_regressor_classifier')
class HierarchicalRegressorClassifier(Regressor):
    """
    Extends the regressor (to predict the num of reasons) with an additional output layer for final GRADE prediction.
    """

    def __init__(self, multitask=False, **kwargs):
        # metrics for the top output layer (GRADE)
        metrics = {"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"}
        super().__init__(other_metrics=metrics, **kwargs)

        # if multitask, the logits from the first (reasons) classifier are not fed to the top (GRADE) classifier
        self.multitask = multitask
        if multitask:
            classifier_top_input_dim = 0
        else:
            # multilabel (reasons) output dim
            classifier_top_input_dim = self.classifier.out_features

        # final classifier
        num_labels_top = kwargs["vocab"].get_vocab_size("top_labels")
        self.classifier_top = torch.nn.Linear(classifier_top_input_dim + self.classifier_input_dim, num_labels_top)

        self.index_to_label_top = self.vocab._index_to_token["top_labels"]

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None,
                top_label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        # Shape: (batch_size, num_labels)
        logits = self.classifier(classifier_inputs)
        if self.multitask:
            logits_top = self.classifier_top(classifier_inputs)
        else:
            logits_top = self.classifier_top(torch.cat([logits, classifier_inputs], 1))

        probs_top = torch.nn.functional.softmax(logits_top, dim=-1)

        output = {'logits': logits, 'probs_top': probs_top, 'loss': 0}

        # multilabel output layer (reasons)
        if label is not None:
            # broadcast:
            pred_probs = torch.zeros((logits.shape[0], self.num_labels)).to(logits.device)
            pred_labels = [find_nearest(self.num_labels, x) for x in logits]
            output["pred_labels"] = pred_labels
            pred_probs[range(logits.shape[0]), pred_labels] = 1.
            gold_labels = torch.sum(label, dim=1).int()
            self.update_metrics(pred_probs, gold_labels, only={"fscore_macro", "fscore_all"})
            self.update_metrics(logits, label, only="mae")
            output['loss'] += mse_loss(logits, label)
        # top output layer (GRADE)
        if top_label is not None:
            self.update_metrics(logits_top, top_label,
                                only={"top_fscore_macro", "top_fscore_all", "top_accuracy", "top_mae"})
            output['loss'] += torch.nn.functional.cross_entropy(logits_top, top_label)

        return output


@Model.register('hierarchical_regressor_classifier_multilabel')
class HierarchicalRegressorRegressor(Regressor):
    """
    Extends the regressor (to predict the num of reasons) with an additional output layer for final multilabel (reasons) prediction.
    """

    def __init__(self, multitask=False, **kwargs):
        # metrics for the top output layer (GRADE)
        metrics = {"multilabel_fscore_macro", "multilabel_fscore_all"}
        super().__init__(other_metrics=metrics, **kwargs)

        # if multitask, the logits from the first (num reasons) regressor are not fed to the top (reasons) classifier
        self.multitask = multitask
        if multitask:
            classifier_top_input_dim = 0
        else:
            # num reasons output dim
            classifier_top_input_dim = self.classifier.out_features

        # final classifier
        num_labels_top = kwargs["vocab"].get_vocab_size("labels")
        self.classifier_top = torch.nn.Linear(classifier_top_input_dim + self.classifier_input_dim, num_labels_top)

        self.index_to_label_top = self.vocab._index_to_token["labels"]
        pos_weight = get_reason_weights(self.index_to_label_top, device=self.classifier.weight.device.type)
        self.loss_f = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight)

    def forward(self,
                feat: torch.Tensor = None,
                text_list: Dict[str, torch.Tensor] = None,
                cat_feats_list: Dict[str, torch.Tensor] = None,
                label: torch.Tensor = None,
                top_label: torch.Tensor = None) -> Dict[str, torch.Tensor]:
        classifier_inputs = self.encode(feat, text_list, cat_feats_list)
        classifier_inputs = self.layer_norm(classifier_inputs)

        # Shape: (batch_size, num_labels)
        logits = self.classifier(classifier_inputs)
        # oracle:
        # logits = label
        if self.multitask:
            logits_top = self.classifier_top(classifier_inputs)
        else:
            logits_top = self.classifier_top(torch.cat([logits, classifier_inputs], 1))

        output = {'logits': logits, 'loss': 0}

        if label is not None:
            # broadcast:
            pred_probs = torch.zeros((logits.shape[0], self.num_labels)).to(logits.device)
            pred_labels = [find_nearest(self.num_labels, x) for x in logits]
            output["pred_labels"] = pred_labels
            pred_probs[range(logits.shape[0]), pred_labels] = 1.
            gold_labels = torch.sum(label, dim=1).int()
            self.update_metrics(pred_probs, gold_labels, only={"fscore_macro", "fscore_all"})
            self.update_metrics(logits, label, only="mae")
            output['loss'] += mse_loss(logits, label)
        # multilabel output layer (reasons)
        if top_label is not None:
            self.update_metrics(logits_top, top_label,
                                only={"multilabel_fscore_macro", "multilabel_fscore_all"})
            output['loss'] += self.loss_f(logits_top, top_label.type_as(logits_top))

        return output
