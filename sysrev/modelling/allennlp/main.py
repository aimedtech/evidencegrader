import json
import os
import shutil
import sys
from collections import defaultdict
from pathlib import Path

import fire
from allennlp.commands import main

from sysrev.dataset_construction.util.util import load_json
from sysrev.modelling.allennlp.util import spawn_config_files, accumulate_metrics, report_metrics, cleanup_large_files


def prepare_directories(master_config_file, dataset_reader_file, serialization_base_dir):
    serialization_par_dir = os.path.join(serialization_base_dir, Path(master_config_file).stem)
    Path(serialization_par_dir).mkdir(exist_ok=True)

    # archive dataset_reader.py
    shutil.copy(dataset_reader_file, serialization_par_dir)
    shutil.copy(master_config_file, serialization_par_dir)

    return serialization_par_dir


def run_single_config(config_file, serialization_dir, metrics, recover, cuda_device):
    overrides = json.dumps({"trainer.cuda_device": cuda_device})

    if recover:
        if Path(f"{serialization_dir}/metrics.json").exists():
            return
    else:
        shutil.rmtree(serialization_dir, ignore_errors=True)

    # Assemble the command into sys.argv to run allennlp
    sys.argv = [
        "allennlp",
        "train",
        config_file,
        "-s", serialization_dir,
        "-o", overrides
    ]

    main()
    accumulate_metrics(metrics, serialization_dir)
    cleanup_large_files(serialization_dir)


def run_all_configs(master_config_file, serialization_par_dir, recover, n_folds, cuda_device):
    metrics = defaultdict(list)

    # not using cross-validation
    if n_folds is None:
        run_single_config(master_config_file, serialization_par_dir, metrics, recover, cuda_device)
        report_metrics(metrics)
    # using cross-validation
    else:
        # now create config file for each cross-validation iteration
        for config_file in spawn_config_files(master_config_file, n_folds=n_folds):
            serialization_dir = f"{serialization_par_dir}{Path(config_file).stem}"
            run_single_config(config_file, serialization_dir, metrics, recover, cuda_device)

        report_metrics(metrics)


def run(master_config_file, dataset_reader_file, serialization_base_dir, recover, n_folds, cuda_device, **kwargs):
    serialization_par_dir = prepare_directories(master_config_file, dataset_reader_file, serialization_base_dir)
    run_all_configs(master_config_file, serialization_par_dir, recover, n_folds, cuda_device)


def get_config(main_config_file):
    main_config = load_json(main_config_file)

    # path to master config file, ie file that contains all necessary parameters, and a path indication for train/dev/test,
    master_config_file = main_config["master_config_file"]

    # path to the file containing different reader classes
    dataset_reader_file = main_config["dataset_reader_file"]

    serialization_base_dir = main_config["serialization_base_dir"]

    # whether to recover from previous runs
    recover = main_config["recover"]

    # number of folds to use for cross-validation or None if not using cross-validation
    n_folds = main_config["n_folds"]

    # to run on CPU, use overrides to set the cuda device to -1
    cuda_device = main_config["cuda_device"]

    return {"master_config_file": master_config_file,
            "dataset_reader_file": dataset_reader_file,
            "serialization_base_dir": serialization_base_dir,
            "recover": recover,
            "n_folds": n_folds,
            "cuda_device": cuda_device}


if __name__ == "__main__":
    """
    Example usage:
    python main.py configs/mains/some_config.json
    """

    config = fire.Fire(get_config)
    run(**config)
