import os

from allennlp.models.archival import load_archive
from sysrev.dataset_construction.util.util import load_json, read_csv
from sysrev.modelling.allennlp.configs.constants import NUMERICAL_FEATS, CATEGORICAL_FEATS
from sysrev.modelling.allennlp.my_project import ClassifierPredictor, HierachicalInteractivePredictor


def load_config(f):
    config = load_json(f)
    archive_file = config["archive_file"]
    input_file = config["input_file"]
    output_file = config["output_file"]
    output_dir = os.path.dirname(output_file)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    return archive_file, input_file, output_file


def build_instance(predictor, ex):
    feat_field, text_list_field, cat_feats_list_field = predictor._dataset_reader.get_fields(ex)
    return predictor._dataset_reader.fields_to_instance(text_list_field, feat_field, cat_feats_list_field)


def get_example(row):
    return dict(filter(lambda x: x[0] in NUMERICAL_FEATS + CATEGORICAL_FEATS, row.items()))


def get_examples(input_file, n=None):
    """
    :param input_file: a csv file
    :param n: number of rows to read, all if None
    :return:
    """
    # read with csv module
    reader = read_csv(input_file)
    for c, row in enumerate(reader):
        if n and c == n:
            break
    yield get_example(row)


def get_predictor(archive_file):
    print(f"Loading predictor from: {archive_file}")
    archive = load_archive(archive_file + "model.tar.gz")

    cuda_device = -1
    predictor = ClassifierPredictor.from_archive(archive)

    return predictor


def get_hierarchical_predictor(archive_file):
    """
    Load a single hierarchical predictor from a config file's archive paths.
    Note that this predictor serves for both GRADE and reasons.
    """
    print(f"Loading predictor from: {archive_file}")
    archive = load_archive(archive_file + "model.tar.gz")

    cuda_device = -1
    predictor = HierachicalInteractivePredictor.from_archive(archive)

    return predictor


def main():
    f = "configs/predict_app.json"
    archive_file, input_file, output_file = load_config(f)
    predictor = get_predictor(archive_file)

    print("Loading example.")
    ex = get_examples(input_file)
    print(ex)

    print("Building instance.")
    instance = build_instance(predictor, ex)

    print("Predicting.")
    pred = predictor.predict_instance(instance)
    print(pred["predicted"])


if __name__ == "__main__":
    main()
