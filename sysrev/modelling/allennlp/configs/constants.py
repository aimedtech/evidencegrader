BINARY_MAPPING1 = {"not-high": 0, "high": 1}
BINARY_MAPPING2 = {"not-high": 0, "high-mod": 1}
INV_BINARY_MAPPING2 = {0: "not-high", 1: "high-mod"}
MAPPING = {"very low": 0, "low": 1, "moderate": 2, "high": 3}
ROB_MAPPING = {"Low risk": 1, "Unclear risk": 0, "High risk": 0}
INV_ROB_MAPPING = {1: "Low risk", 0: "High/unclear risk"}
NUM_REASONS_MAPPING = {0: 0, 1: 1, 2: 2, 3: 3}
INV_MAPPING = {0: "very low", 1: "low", 2: "moderate", 3: "high"}
INV_NUM_REASONS_MAPPING = {0: 0, 1: 1, 2: 2, 3: 3}
NUMERICAL_FEATS = ["n_participants", "n_studies", "year", "n_sofs", "n_outcomes",
                   "ci_lower", "ci_upper", "relative_effect", "n_included_studies", "n_excluded_studies",
                   "n_ongoing_studies",
                   "n_additional_studies", "n_other_studies"
                   ]
NUMERICAL_FEATS_PER_OUTCOME = ["val_chi2", "val_df", "val_i2", "val_i2_q", "val_p_chi2", "val_p_q", "val_p_z", "val_q",
                               "val_tau2", "val_z", 'val_n_high_risk_alloc', 'val_n_high_risk_incom',
                               'val_n_high_risk_other', 'val_n_high_risk_outco', 'val_n_high_risk_partb',
                               'val_n_high_risk_blind', 'val_n_high_risk_rands', 'val_n_high_risk_selec',
                               'val_n_low_risk_alloc', 'val_n_low_risk_incom', 'val_n_low_risk_other',
                               'val_n_low_risk_outco', 'val_n_low_risk_partb', 'val_n_low_risk_blind',
                               'val_n_low_risk_rands', 'val_n_low_risk_selec', 'val_n_unclear_risk_alloc',
                               'val_n_unclear_risk_incom', 'val_n_unclear_risk_other', 'val_n_unclear_risk_outco',
                               'val_n_unclear_risk_partb', 'val_n_unclear_risk_blind', 'val_n_unclear_risk_rands',
                               'val_n_unclear_risk_selec', 'val_prop_high_risk_alloc', 'val_prop_high_risk_incom',
                               'val_prop_high_risk_other', 'val_prop_high_risk_outco', 'val_prop_high_risk_partb',
                               'val_prop_high_risk_blind', 'val_prop_high_risk_rands', 'val_prop_high_risk_selec']
CATEGORICAL_FEATS = ["review_type", "type_effect", "topics"]
TEXTUAL_FEATS = ["full_abstract_txt", "abstract_conclusion_txt", "plain_language_summary_txt",
                 "authors_conclusions_txt"]
