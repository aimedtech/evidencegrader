import numpy as np
from scipy.stats import describe
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.preprocessing import MultiLabelBinarizer

from sysrev.dataset_construction.util.util import read_csv, load_json


def get_labels(p, pred_only=False):
    y = []
    with open(p) as fh:
        for c, l in enumerate(fh):
            if pred_only:
                y_pred = l.strip()
                y.append(y_pred)
            else:
                y_pred, y_true = l.strip().split()[:2]
                y.append((y_pred, y_true))

    return y


def combine_preds_from_binary_classes(serialization_paths):
    for k in range(10):
        paths_probs = []
        for path in serialization_paths:
            p = path.replace("FOLD", str(k))
            paths_probs.append(get_labels(f"{p}/test_predictions"))

        preds, golds = [], []
        for path_probs in zip(*paths_probs):
            preds.append((np.array([i[0] for i in path_probs]) != "other") * 1.)
            golds.append((np.array([i[1] for i in path_probs]) != "other") * 1.)

        yield golds, preds


def read_preds(generic_serialization_path, test_path):
    for k in range(10):
        p = generic_serialization_path.replace("FOLD", str(k))
        preds = get_labels(f"{p}/test_predictions", pred_only=True)
        preds = [sorted([p.replace("-", " ") for p in eval(pred)]) for pred in preds]

        golds = []
        reader = read_csv(test_path.replace("/k/", f"/{k}/"))
        for row in reader:
            if row["Reasons"] is not None:
                golds.append(sorted(eval(row["Reasons"]).keys()))
            else:
                golds.append([])
        mlb = MultiLabelBinarizer()
        mlb_fitted = mlb.fit(golds)
        golds = mlb_fitted.transform(golds)
        preds = mlb_fitted.transform(preds)

        yield golds, preds


def calculate_metrics(golds_preds_per_fold):
    accs = []
    f1_scores_micro = []
    f1_scores_macro = []
    for golds, preds in golds_preds_per_fold:
        accs.append(accuracy_score(golds, preds))
        f1_scores_micro.append(f1_score(golds, preds, average="micro"))
        f1_scores_macro.append(f1_score(golds, preds, average="macro"))

    print(f"acc: {describe(accs)}")
    print(f"f1 micro: {describe(f1_scores_micro)}")
    print(f"f1 macro: {describe(f1_scores_macro)}")
    # print(multilabel_confusion_matrix(golds, preds, labels=["rob", "imp", "inc", "ind", "pub"]))


config = load_json("configs/eval_direct_reasons.json")
generic_serialization_path = config["generic_serialization_path"]
test_path = config["test_path"]

if "REASON" in generic_serialization_path:
    reasons = ["rob", "imp", "inc", "ind", "pub"]
    serialization_paths = [generic_serialization_path.replace("REASON", r) for r in reasons]

    # combine test predictions from all binary classes for REASONs, and write to disk
    golds_preds_per_fold = combine_preds_from_binary_classes(serialization_paths, generic_serialization_path)
else:
    golds_preds_per_fold = read_preds(generic_serialization_path, test_path)
calculate_metrics(golds_preds_per_fold)
