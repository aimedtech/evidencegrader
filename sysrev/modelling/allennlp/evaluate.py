import os
import sys
import tarfile
from collections import defaultdict
from pathlib import Path

from allennlp.commands import main

from sysrev.dataset_construction.util.util import load_json
from sysrev.modelling.allennlp.util import spawn_config_files, accumulate_metrics, report_metrics

config = load_json("configs/evaluate.json")
master_config_file = config["master_config_file"]
dataset_reader_file = config["dataset_reader_file"]
saved_base_dir = config["saved_base_dir"]

metrics = defaultdict(list)
serialization_par_dir = f"{saved_base_dir}/{Path(master_config_file).stem}/"
# overrides = json.dumps({"trainer": {"cuda_device": 3}})
cuda_device = -1

# create config file for each cross-validation iteration
for i, config_file in enumerate(spawn_config_files(master_config_file, n_folds=10)):
    # Use overrides to train on CPU.
    input_file = config["input_file"].replace("/k/", f"/{i}/")
    serialization_dir = f"{serialization_par_dir}{Path(config_file).stem}/"
    tar = tarfile.open(f"{serialization_dir}/model.tar.gz", "r:gz")
    tar.extract("weights.th", path=serialization_dir)
    tar.close()
    os.rename(f'{serialization_dir}weights.th', f'{serialization_dir}best.th')

    # Assemble the command into sys.argv
    sys.argv = [
        "allennlp",  # command name, not used by main
        "evaluate",
        serialization_dir,
        input_file,
        "--cuda-device", str(cuda_device),
        "--include-package", "my_project",
        "--output-file", serialization_dir + "metrics_test_mental"  # ,
        # "-o", overrides
    ]

    main()
    accumulate_metrics(metrics, serialization_dir, metric_prefix_name=False)
    os.remove(f'{serialization_dir}best.th')

report_metrics(metrics)
