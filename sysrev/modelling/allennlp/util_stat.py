import numpy as np

from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, \
    recall_score


def get_pearson_i(l):
    """
    Calculates Pearson's measure of skewness of a distribution
    :param l: an iterable
    :return: a scalar, indicating either a positive or negative skew
    """
    if len(l) == 0:
        return None

    return (3 * (np.mean(l) - np.median(l))) / np.std(l)


# from https://github.com/HanXudong/fairlib
def confusion_matrix_based_scores(cnf):
    """Calculate confusion matrix based scores.

    Implementation from https://stackoverflow.com/a/43331484
    See https://en.wikipedia.org/wiki/Confusion_matrix for different scores

    Args:
        cnf (np.array): a confusion matrix.

    Returns:
        dict: a set of metrics for each class, indexed by the metric name.
    """
    FP = cnf.sum(axis=0) - np.diag(cnf) + 1e-5
    FN = cnf.sum(axis=1) - np.diag(cnf) + 1e-5
    TP = np.diag(cnf) + 1e-5
    TN = cnf.sum() - (FP + FN + TP) + 1e-5

    # Sensitivity, hit rate, recall, or true positive rate
    TPR = TP / (TP + FN)
    # Specificity or true negative rate
    TNR = TN / (TN + FP)
    # Precision or positive predictive value
    PPV = TP / (TP + FP)
    # Negative predictive value
    NPV = TN / (TN + FN)
    # Fall out or false positive rate
    FPR = FP / (FP + TN)
    # False negative rate
    FNR = FN / (TP + FN)
    # False discovery rate
    FDR = FP / (TP + FP)

    # Overall accuracy
    ACC = (TP + TN) / (TP + FP + FN + TN)

    # Positive Prediction Rates
    PPR = (TP + FP) / (TP + FP + FN + TN)

    return {
        "TPR": TPR,
        "TNR": TNR,
        "PPV": PPV,
        "NPV": NPV,
        "FPR": FPR,
        "FNR": FNR,
        "FDR": FDR,
        "ACC": ACC,
        "PPR": PPR,
    }


def power_mean(series, p, axis=0):
    """calculate the generalized mean of a given list.

    Args:
        series (list): a list of numbers.
        p (int): power of the generalized mean aggregation
        axis (int, optional): aggregation along which dim of the input. Defaults to 0.

    Returns:
        np.array: aggregated scores.
    """
    if p > 50:
        return np.max(series, axis=axis)
    elif p < -50:
        return np.min(series, axis=axis)
    else:
        total = np.mean(np.power(series, p), axis=axis)
        return np.power(total, 1 / p)


def Aggregation_GAP(distinct_groups, all_scores, metric="TPR", group_agg_power=None, class_agg_power=2):
    """Aggregate fairness metrics at the group level and class level.

    Args:
        distinct_groups (list): a list of distinc labels of protected groups.
        all_scores (dict): confusion matrix based scores for each protected group and all.
        metric (str, optional): fairness metric. Defaults to "TPR".
        group_agg_power (int, optional): generalized mean aggregation power at the group level. Use absolute value aggregation if None. Defaults to None.
        class_agg_power (int, optional): generalized mean aggregation power at the class level. Defaults to 2.

    Returns:
        np.array: aggregated fairness score.
    """
    group_scores = []
    for gid in distinct_groups:
        # Save the TPR direct to the list
        group_scores.append(all_scores[gid][metric])
    # n_class * n_groups
    Scores = np.stack(group_scores, axis=1)
    # Calculate GAP (n_class * n_groups) - (n_class * 1)
    score_gaps = Scores - all_scores["overall"][metric].reshape(-1, 1)
    # Sum over gaps of all protected groups within each class
    if group_agg_power is None:
        score_gaps = np.sum(abs(score_gaps), axis=1)
    else:
        score_gaps = power_mean(score_gaps, p=group_agg_power, axis=1)
    # Aggregate gaps of each class, RMS by default
    score_gaps = power_mean(score_gaps, class_agg_power)

    return score_gaps


def get_perf_scores(y_true, y_pred):
    return {
        "precision_macro": precision_score(y_true, y_pred, average="macro"),
        "recall_macro": recall_score(y_true, y_pred, average="macro"),
        "macro_fscore": f1_score(y_true, y_pred, average="macro"),
    }


def gap_eval_scores(y_pred, y_true, protected_attribute, metrics=["TPR", "FPR", "PPR"]):
    """fairness evaluation

    Args:
        y_pred (np.array): model predictions.
        y_true (np.array): target labels.
        protected_attribute (np.array): protected labels.
        metrics (list, optional): a list of metric names that will be considered for fairness evaluation. Defaults to ["TPR","FPR","PPR"].

    Returns:
        tuple: (fairness evaluation results, confusion matrices, perf scores per group)
    """
    y_pred = np.array(y_pred)
    y_true = np.array(y_true)
    protected_attribute = np.array(protected_attribute)

    # performance evaluation
    eval_scores = {
        "accuracy": accuracy_score(y_true, y_pred),
        "macro_fscore": f1_score(y_true, y_pred, average="macro"),
        "macro_P": precision_score(y_true, y_pred, average="macro"),
        "macro_R": recall_score(y_true, y_pred, average="macro"),
        "micro_fscore": f1_score(y_true, y_pred, average="micro"),
    }

    all_scores = {}
    perf_scores = {}
    confusion_matrices = {}
    # Overall evaluation
    distinct_labels = list(set(y_true))
    overall_confusion_matrix = confusion_matrix(y_true=y_true, y_pred=y_pred, labels=distinct_labels)
    confusion_matrices["overall"] = overall_confusion_matrix
    all_scores["overall"] = confusion_matrix_based_scores(overall_confusion_matrix)
    perf_scores["overall"] = get_perf_scores(y_true, y_pred)

    # Group scores
    distinct_groups = set(protected_attribute)
    for gid in distinct_groups:
        group_identifier = (protected_attribute == gid)
        group_confusion_matrix = confusion_matrix(y_true=y_true[group_identifier], y_pred=y_pred[group_identifier],
                                                  labels=distinct_labels)
        confusion_matrices[gid] = group_confusion_matrix
        all_scores[gid] = confusion_matrix_based_scores(group_confusion_matrix)
        perf_scores[gid] = get_perf_scores(y_true[group_identifier], y_pred[group_identifier])

    for _metric in metrics:
        eval_scores["{}_GAP".format(_metric)] = Aggregation_GAP(distinct_groups=distinct_groups, all_scores=all_scores,
                                                                metric=_metric, group_agg_power=2, class_agg_power=2)

    return eval_scores, confusion_matrices, perf_scores
