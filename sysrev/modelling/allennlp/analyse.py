import os.path
from collections import defaultdict, Counter

import numpy as np
import scipy
from sklearn.metrics import f1_score, auc, precision_score, recall_score

from sysrev.dataset_construction.util.util import load_json
from sysrev.modelling.allennlp.util_stat import get_pearson_i, gap_eval_scores

GRADE_MAP = {
    "high": 0,
    "moderate": 1,
    "low": 2,
    "very low": 3
}


def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    accuracy = np.trace(cm) / np.sum(cm).astype('float')
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()


def load_preds(path_to_predictions):
    preds, golds, rests = [], [], []
    with open(path_to_predictions) as fh:
        for l in fh:
            split_l = l.strip().split("\t")
            if len(split_l) == 2:
                pred, gold = split_l
            else:
                pred, gold, *rest = split_l
                rests.append(rest)

            preds.append(pred)
            golds.append(gold)

    return preds, golds, rests


class ConsistencyAnalysis:
    def __init__(self, rob_f, imp_f, inc_f, ind_f, pub_f, bi_overall_f, overall_f, multilab_reasons_f):
        # files with predictions for each downgrading criterion, each from a different model that makes a binary prediction
        self.rob_f = rob_f
        self.imp_f = imp_f
        self.inc_f = inc_f
        self.ind_f = ind_f
        self.pub_f = pub_f

        # binary overall GRADE predictions
        self.bi_overall_f = bi_overall_f
        # 4-tier overall GRADE predictions
        self.overall_f = overall_f
        # multilabelling predictions for downgrading reasons
        self.multilab_reasons_f = multilab_reasons_f

        self.overall_consistent_map = {
            ("high-mod", "moderate"): 1,
            ("high-mod", "high"): 1,
            ("not-high", "low"): 1,
            ("not-high", "very low"): 1
        }

        self.overall_multilab_reasons_consistent_map = GRADE_MAP

        self.bi_overall_multilab_reasons_consistent_map = {
            "high-mod": {0, 1},
            "not-high": {1, 2}
        }

    def load_preds_only(self, path_to_predictions, evaluate=False):
        # loads a prediction file
        preds = []
        with open(path_to_predictions) as fh:
            for l in fh:
                label = l.strip()
                if label:
                    preds.append(eval(label) if evaluate else label)

        return preds

    def prepare_data(self):
        # load all prediction files
        preds = {"rob": self.load_preds_only(self.rob_f),
                 "imp": self.load_preds_only(self.imp_f),
                 "inc": self.load_preds_only(self.inc_f),
                 "ind": self.load_preds_only(self.ind_f),
                 "pub": self.load_preds_only(self.pub_f),
                 "bi_overall": self.load_preds_only(self.bi_overall_f),
                 "overall": self.load_preds_only(self.overall_f),
                 "multilab_reasons": self.load_preds_only(self.multilab_reasons_f, evaluate=True)}

        # should be same n. of predictions for each model
        assert len(set(map(len, preds.values()))) == 1

        return preds

    def consistencies(self, preds):
        print(self.overall_consistency(preds["bi_overall"], preds["overall"]))

        print(self.overall_multilab_reasons_consistency(preds["overall"], preds["multilab_reasons"]))

        print(self.bi_overall_multilab_reasons_consistency(preds["bi_overall"], preds["multilab_reasons"]))

        bi_reasons = zip(preds["rob"], preds["imp"], preds["inc"], preds["ind"], preds["pub"])
        print(self.overall_bi_reasons_consistency(preds["overall"], bi_reasons))
        bi_reasons = zip(preds["rob"], preds["imp"], preds["inc"], preds["ind"], preds["pub"])
        print(self.bi_overall_bi_reasons_consistency(preds["bi_overall"], bi_reasons))
        bi_reasons = zip(preds["rob"], preds["imp"], preds["inc"], preds["ind"], preds["pub"])
        print(self.bi_reasons_multilab_reasons_consistency(bi_reasons, preds["multilab_reasons"]))

    def overall_consistency(self, bi_overall, overall):
        """
        Consistency between overall GRADE score and binary GRADE score
        """

        def is_consistent(preds):
            return self.overall_consistent_map.get((preds[0], preds[1]), 0)

        scores = list(map(is_consistent, zip(bi_overall, overall)))

        return sum(scores) / len(scores)

    def overall_multilab_reasons_consistency(self, overall, multilab_reasons):
        """
        Consistency between overall GRADE score and assigned downgrading reasons.
        If e.g. GRADE="low", there should two downgrading reasons.
        """

        def is_consistent(preds):
            n_reasons = len(preds[1])
            # n_reasons = np.random.randint(0,4)
            expected_n = self.overall_multilab_reasons_consistent_map[preds[0]]
            # expected_n = np.random.randint(0, 4)
            return int(n_reasons == expected_n)

        scores = list(map(is_consistent, zip(overall, multilab_reasons)))

        return sum(scores) / len(scores)

    def bi_overall_multilab_reasons_consistency(self, bi_overall, multilab_reasons):
        """
        Consistency between binary overall GRADE score and assigned downgrading reasons.
        If e.g. GRADE="high-mod", there should 0 or 1 downgrading reason.
        """

        def is_consistent(preds):
            n_reasons = len(preds[1])
            # n_reasons = np.random.randint(0, 4)
            expected_n = self.bi_overall_multilab_reasons_consistent_map[preds[0]]
            # expected_n = np.random.randint(0, 4)
            return int(n_reasons in expected_n)

        scores = list(map(is_consistent, zip(bi_overall, multilab_reasons)))

        return sum(scores) / len(scores)

    def overall_bi_reasons_consistency(self, overall, bi_reasons):
        """
        Consistency between overall GRADE score and downgrading reasons from binary classification models.
        If e.g. GRADE="low", there should two downgrading reasons.
        """

        def is_consistent(preds):
            n_reasons = sum(np.array(preds[1]) != "other")
            # n_reasons = np.random.randint(0,4)
            expected_n = self.overall_multilab_reasons_consistent_map[preds[0]]
            # expected_n = np.random.randint(0, 4)
            return int(n_reasons == expected_n)

        scores = list(map(is_consistent, zip(overall, bi_reasons)))

        return sum(scores) / len(scores)

    def bi_overall_bi_reasons_consistency(self, bi_overall, bi_reasons):
        """
        Consistency between binary overall GRADE score and downgrading reasons from binary classification models.
        If e.g. GRADE="high-mod", there should 0 or 1 downgrading reasons.
        """

        def is_consistent(preds):
            n_reasons = sum(np.array(preds[1]) != "other")
            # n_reasons = np.random.randint(0,4)
            expected_n = self.bi_overall_multilab_reasons_consistent_map[preds[0]]
            # expected_n = np.random.randint(0, 4)
            return int(n_reasons in expected_n)

        scores = list(map(is_consistent, zip(bi_overall, bi_reasons)))

        return sum(scores) / len(scores)

    def bi_reasons_multilab_reasons_consistency(self, bi_reasons, multilab_reasons):
        """
        Consistency between multilabel reasons and downgrading reasons from binary classification models.
        """

        def is_consistent(preds):
            n_reasons = len(preds[1])
            n_bi_reasons = sum(np.array(preds[0]) != "other")
            return int(n_reasons == n_bi_reasons)

        scores = list(map(is_consistent, zip(bi_reasons, multilab_reasons)))

        return sum(scores) / len(scores)


class EnsembleOverKFolds(ConsistencyAnalysis):
    def load_probs_only(self, path_to_predictions):
        # loads a prediction file
        probs_all = []
        with open(path_to_predictions) as fh:
            for l in fh:
                if l.strip():
                    pred, gold, probs, *_ = l.strip().split("\t")
                    if probs:
                        probs_all.append(eval(probs))
        return probs_all if len(probs_all) == 6 else probs_all[1:]

    def prepare_data(self):
        # load all prediction files
        preds = {"rob": self.load_probs_only(self.rob_f),
                 "imp": self.load_probs_only(self.imp_f),
                 "inc": self.load_probs_only(self.inc_f),
                 "ind": self.load_probs_only(self.ind_f),
                 "pub": self.load_probs_only(self.pub_f),
                 "bi_overall": self.load_probs_only(self.bi_overall_f),
                 "overall": self.load_probs_only(self.overall_f),
                 "multilab_reasons": self.load_probs_only(self.multilab_reasons_f)}

        return preds

    def prepare_label_vocab(self):
        d = {"rob": self.get_label_vocab(self.rob_f),
             "imp": self.get_label_vocab(self.imp_f),
             "inc": self.get_label_vocab(self.inc_f),
             "ind": self.get_label_vocab(self.ind_f),
             "pub": self.get_label_vocab(self.pub_f),
             "bi_overall": self.get_label_vocab(self.bi_overall_f),
             "overall": self.get_label_vocab(self.overall_f),
             "multilab_reasons": self.get_label_vocab(self.multilab_reasons_f)}

        return d

    def get_label_vocab(self, f):
        label_f = "{}{}".format(os.path.dirname(f), "/vocabulary/labels.txt")
        labels = {}
        with open(label_f) as fh:
            idx = 0
            for l in fh:
                if l.strip():
                    labels[idx] = l.strip()
                    idx += 1
        return labels


def prepare_data_all_folds(config_f):
    """
    CogTale data
    """
    config = load_json(config_f)
    preds_folds = defaultdict(list)
    labels = {}

    for k in range(10):
        c = EnsembleOverKFolds(
            rob_f=config["rob_f"].replace("_k", f"_{k}"),
            imp_f=config["imp_f"].replace("_k", f"_{k}"),
            inc_f=config["inc_f"].replace("_k", f"_{k}"),
            ind_f=config["ind_f"].replace("_k", f"_{k}"),
            pub_f=config["pub_f"].replace("_k", f"_{k}"),
            bi_overall_f=config["bi_overall_f"].replace("_k", f"_{k}"),
            overall_f=config["overall_f"].replace("_k", f"_{k}"),
            multilab_reasons_f=config["multilab_reasons_f"].replace("_k", f"_{k}")
        )

        for k, v in c.prepare_data().items():
            preds_folds[k].append(v)
            labels_for_k = c.prepare_label_vocab()[k]
            if k not in labels:
                labels[k] = labels_for_k
            else:
                assert labels[k] == labels_for_k

    return preds_folds, labels


class EnsembleOverTaskVariations(ConsistencyAnalysis):
    def unify_data(self):
        preds = self.prepare_data()
        bi_reasons = zip(preds["rob"], preds["imp"], preds["inc"], preds["ind"], preds["pub"])
        ns_bi_reasons = [sum(np.array(pred) != "other") for pred in bi_reasons]
        ns_overall = [self.overall_multilab_reasons_consistent_map[pred] for pred in preds["overall"]]
        _ns_bi_overall = [self.bi_overall_multilab_reasons_consistent_map[pred] for pred in preds["bi_overall"]]
        ns_bi_overall = []
        for pred_overall, pred_bi_overall in zip(ns_overall, _ns_bi_overall):
            if pred_overall in pred_bi_overall:
                ns_bi_overall.append(pred_overall)
            else:
                ns_bi_overall.append(np.random.choice(list(pred_bi_overall)))
        ns_multilab_reasons = [len(pred) for pred in preds["multilab_reasons"]]
        unified_preds = {"bi_reasons": ns_bi_reasons, "bi_overall": ns_bi_overall, "overall": ns_overall,
                         "multilab_reasons": ns_multilab_reasons}

        return [Counter(k).most_common(1).pop()[0] for k in zip(*unified_preds.values())], ns_overall


def get_entire_band(probs_high, pos_golds, outfile, suffix=""):
    if outfile is not None:
        outfile = outfile + suffix
    from sklearn.preprocessing import KBinsDiscretizer

    probs_high = np.array(probs_high)

    discretiser = KBinsDiscretizer(20, strategy="quantile")
    discretiser.fit(probs_high.reshape(-1, 1))
    rel_freqs = {}
    accs = {}
    confs = {}
    sizes = {}
    for edge_left, edge_right in zip(discretiser.bin_edges_[0], discretiser.bin_edges_[0][1:]):
        norm = len(pos_golds[(edge_left <= probs_high) & (probs_high < edge_right)])
        if norm == 0:
            continue
        rel_freqs[(edge_left, edge_right)] = sum(pos_golds[(edge_left <= probs_high) & (
                probs_high < edge_right)]) / norm
        norm = len(probs_high[(edge_left <= probs_high) & (probs_high < edge_right)])
        if norm == 0:
            continue
        confs[(edge_left, edge_right)] = sum(probs_high[(edge_left <= probs_high) & (
                probs_high < edge_right)]) / norm
        sizes[(edge_left, edge_right)] = len(
            probs_high[(edge_left <= probs_high) & (probs_high < edge_right)])
    if outfile is not None:
        # pickle.dump(accs, open(outfile, "wb"))
        write_accs_plain(rel_freqs, outfile + ".txt")

    return rel_freqs, confs, sizes


def calculate_ece(accs, confs, sizes):
    ece = np.sum(
        np.abs(np.array(list(accs.values())) - np.array(list(confs.values()))) *
        (np.array(list(sizes.values())) / np.sum(list(sizes.values())))
    )

    return ece


def calculate_mce(accs, confs):
    mce = np.max(np.abs(np.array(list(accs.values())) - np.array(list(confs.values()))))

    return mce


def write_accs_plain(accs, outfile):
    with open(outfile, "w") as fh_out:
        fh_out.write("edge_left\tedge_right\tmean_bin\tacc\n")
        for (edge_left, edge_right), acc in accs.items():
            fh_out.write("{}\t{}\t{}\t{}\n".format(edge_left, edge_right, np.mean([edge_left, edge_right]), acc))


def calculate_brier(probs_high, golds_all, to_onehot=True):
    from sklearn.metrics import mean_squared_error

    if to_onehot:
        golds = (np.array(golds_all) == "high-mod").astype(int)
    else:
        golds = golds_all

    return mean_squared_error(golds, probs_high)


def get_risk_coverage_auc_per_band(probs, pos_golds, pos_preds, band):
    if isinstance(probs, list):
        probs = np.array(probs)
    n_preds = len(probs)
    coverages, risks = [], []

    # probability threshold (for positive band)
    if band == "positive":
        for tau in [i / 100 for i in list(range(50, 101))]:
            norm = sum(probs >= tau)
            if norm != 0:
                acc = sum(pos_preds[probs >= tau] == pos_golds[probs >= tau]) / norm
                risks.append(1 - acc)
                coverages.append(sum(probs >= tau) / n_preds)
    elif band == "negative":
        for tau in [1 - (i / 100) for i in list(range(50, 101))]:
            norm = sum(probs < tau)
            if norm != 0:
                acc = sum(pos_preds[probs < tau] == pos_golds[probs < tau]) / norm
                risks.append(1 - acc)
                coverages.append(sum(probs < tau) / n_preds)
    else:
        # for tau in [i / 100 for i in list(range(0, 101))]:
        # prob threshold increases for both left (neg) and right (pos) predictions
        # when tau==0.5, coverage==1 as all preds are either >=0.5, or <0.5
        # when tau==1., coverage->0 as only the predictions whose p==1.

        for tau in [i / 100 for i in list(range(50, 101))]:
            inv_tau = 1 - tau
            norm = sum(probs >= tau) + sum(probs < inv_tau)
            if norm != 0:
                acc = (sum(pos_preds[probs >= tau] == pos_golds[probs >= tau]) + sum(
                    pos_preds[probs < inv_tau] == pos_golds[probs < inv_tau])) / norm
                risks.append(1 - acc)
                coverages.append((sum(probs >= tau) + sum(probs < inv_tau)) / n_preds)

    aurc = auc(coverages, risks)
    return aurc, coverages, risks
    # plt.plot(coverages, risks);plt.xlim(0,1);plt.ylim(0,1);plt.show()


def get_risk_coverage_auc(probs_high, decisions, pos_neg_split=True):
    probs_high = np.array(probs_high)
    decisions = np.array(decisions)

    if pos_neg_split:
        # positive decision band
        probs_high_positive_band = probs_high[probs_high > 0.5]
        decisions_positive_band = decisions[probs_high > 0.5]
        if len(decisions_positive_band) < 2:
            aurc_pos = None
        else:
            aurc_pos, _, _ = get_risk_coverage_auc_per_band(probs_high_positive_band, decisions_positive_band,
                                                            "positive")

        # negative decision band
        probs_high_negative_band = probs_high[probs_high < 0.5]
        decisions_negative_band = decisions[probs_high < 0.5]
        if len(decisions_negative_band) < 2:
            aurc_neg = None
        else:
            aurc_neg, _, _ = get_risk_coverage_auc_per_band(probs_high_negative_band, decisions_negative_band,
                                                            "negative")

        return aurc_pos, aurc_neg
    else:
        aurc, _, _ = get_risk_coverage_auc_per_band(probs_high, decisions, "entire")

        return aurc


def get_prob_high_idx(f):
    with open(f) as fh:
        labels = [l.strip() for l in fh]
    assert len(labels) == 2

    if "other" in labels:
        return labels.index("other"), "other"
    elif "high-mod" in labels:
        return labels.index("high-mod"), "high-mod"
    else:
        raise IndexError


def get_label_ratio(golds, criterion):
    c = Counter(golds)
    n_golds = sum(c.values())
    if n_golds == 0:
        return None
    if criterion == "binary GRADE":
        pos_label = "high-mod"
    elif criterion in {"imprecision", "risk of bias", "indirectness", "inconsistency",
                       "publication bias"}:
        pos_label = "other"
    elif criterion in {"Random sequence generation", "Allocation concealment", "Blinding of participants and personnel",
                       "Blinding of outcome assessment"}:
        pos_label = 1

    return c[pos_label] / n_golds


def analyse_confidence(generic_path, criterion="", with_temp_scaling=False, with_label_smoothing=False):
    config = load_json("configs/analyse.json")
    evidencegrader_acc_per_confidence_base_name = config["evidencegrader_acc_per_confidence_base_name"]
    evidencegrader_metrics_per_topics_base_name = config["evidencegrader_metrics_per_topics_base_name"]
    evidencegrader_aurc_raw_base_name = config["evidencegrader_aurc_raw_base_name"]

    # per topic:
    data_per_topic = {}
    # overall:
    probs_high = []
    probs_high_pos = []
    probs_high_neg = []
    golds_all = []
    preds_all = []
    decisions = []  # whether correct or not
    smoothed = "_smoothed" if with_label_smoothing else ""

    for fold_n in range(10):
        path = generic_path.replace("FOLD", str(fold_n))
        preds, golds, rests = load_preds(path + "/test_predictions_extended" + ('_ts' if with_temp_scaling else ''))
        prob_high_idx, prob_high_label = get_prob_high_idx(path + "/vocabulary/labels.txt")
        for c, rest in enumerate(rests):
            # for binary GRADE model, prob of high quality of evidence
            # for binary criteria model, prob of absence of criterion (higher quality)
            # prob_high = normalise_0_to_1(eval(rest[0])[prob_high_idx])
            prob_high = eval(rest[0])[prob_high_idx]
            golds_all.append(golds[c])
            preds_all.append(preds[c])
            decision = 1 if golds[c] == preds[c] else 0
            # overall:
            probs_high.append(prob_high)
            if prob_high >= 0.5:
                probs_high_pos.append(prob_high)
            else:
                probs_high_neg.append(prob_high)
            decisions.append(decision)
            # per topic:
            topics = eval(rest[1])
            for topic in topics:
                if topic not in data_per_topic:
                    data_per_topic[topic] = defaultdict(list)
                # prob_high = normalise_0_to_1(eval(rest[0])[prob_high_idx])
                prob_high = eval(rest[0])[prob_high_idx]
                data_per_topic[topic]["probs_high"].append(prob_high)
                if prob_high >= 0.5:
                    data_per_topic[topic]["probs_high_pos"].append(prob_high)
                else:
                    data_per_topic[topic]["probs_high_neg"].append(prob_high)
                data_per_topic[topic]["decisions"].append(1 if golds[c] == preds[c] else 0)
                data_per_topic[topic]["golds_all"].append(golds[c])
                data_per_topic[topic]["preds_all"].append(preds[c])

    # overall
    ece_scores = {}

    pos_golds = np.array(golds_all) == prob_high_label
    pos_preds = np.array(preds_all) == prob_high_label
    accs, confs, sizes = get_entire_band(probs_high, pos_golds, outfile=evidencegrader_acc_per_confidence_base_name + (
                                             '_ts' if with_temp_scaling else '') + smoothed + "_" + criterion)
    ece = calculate_ece(accs, confs, sizes)
    mce = calculate_mce(accs, confs)
    print("Expected Calibration Error (ECE) for entire band:  " + str(round(ece, 2)))
    print("Maximum Calibration Error (MCE) for entire band:  " + str(round(mce, 2)))
    ece_scores["overall"] = ece

    aurc_raw = []  # for plotting risk-coverage curves

    aurc, coverages, risks = get_risk_coverage_auc_per_band(probs_high, pos_golds, pos_preds, band="entire")
    print("AURC: " + str(aurc))

    for i in range(len(coverages)):
        aurc_raw.append((criterion, "all", coverages[i], risks[i]))

    brier_scores = {}
    brier = calculate_brier(probs_high, golds_all)
    print("Brier Score:  " + str(round(brier, 2)))
    brier_scores["overall"] = brier

    print("f1 macro: " + str(f1_score(golds_all, preds_all, average='macro')))
    print("P macro: " + str(precision_score(golds_all, preds_all, average='macro')))
    print("R macro: " + str(recall_score(golds_all, preds_all, average='macro')))

    error_props = {}
    f1_scores = {}
    entropies = {}
    variances = {}
    variances_from_point_five = {}
    mean_from_point_five = {}
    median_from_point_five = {}
    iqr_from_point_five = {}
    pearson_i_from_point_five = {}
    entropies_pos = {}
    variances_pos = {}
    entropies_neg = {}
    variances_neg = {}
    totals = {}
    aurcs = {}
    label_ratios = {}

    fair_eval_d = {"y_pred": [], "y_true": [], "protected_attribute": []}

    # per topic
    for _topic, d in data_per_topic.items():
        topic = _topic.replace("_", " ")
        if len(d["probs_high"]) < 20:
            continue

        pos_golds = np.array(d["golds_all"]) == prob_high_label
        pos_preds = np.array(d["preds_all"]) == prob_high_label
        label_ratios[topic] = get_label_ratio(d["golds_all"], criterion)
        accs, confs, sizes = get_entire_band(d["probs_high"], pos_golds, suffix=topic,
                                             outfile=evidencegrader_acc_per_confidence_base_name + (
                                                 '_ts' if with_temp_scaling else '') + smoothed + "_" + criterion)
        ece = calculate_ece(accs, confs, sizes)
        mce = calculate_mce(accs, confs)
        # print("Expected Calibration Error (ECE) for entire band (" + topic + "):  "+ str(round(ece, 2)))
        ece_scores[topic] = ece

        aurcs[topic], coverages, risks = get_risk_coverage_auc_per_band(d["probs_high"], pos_golds, pos_preds,
                                                                        band="entire")
        for i in range(len(coverages)):
            aurc_raw.append((criterion, topic, coverages[i], risks[i]))
        brier = calculate_brier(d["probs_high"], d["golds_all"])
        brier_scores[topic] = brier

        error_props[topic] = 1 - sum(d["decisions"]) / len(d["decisions"])
        f1_scores[topic] = f1_score(d["golds_all"], d["preds_all"], average="macro")
        entropies[topic] = scipy.stats.entropy(d["probs_high"])
        variances[topic] = np.var(d["probs_high"])
        probs_high_np_from_point_five = np.abs(np.array(d["probs_high"]) - 0.5)
        variances_from_point_five[topic] = np.mean(probs_high_np_from_point_five ** 2)
        mean_from_point_five[topic] = np.mean(probs_high_np_from_point_five)
        median_from_point_five[topic] = np.median(probs_high_np_from_point_five)
        iqr_from_point_five[topic] = scipy.stats.iqr(probs_high_np_from_point_five)
        pearson_i_from_point_five[topic] = get_pearson_i(probs_high_np_from_point_five)

        entropies_pos[topic] = scipy.stats.entropy(d["probs_high_pos"])
        variances_pos[topic] = np.var(d["probs_high_pos"])
        entropies_neg[topic] = scipy.stats.entropy(d["probs_high_neg"])
        variances_neg[topic] = np.var(d["probs_high_neg"])
        totals[topic] = len(d["decisions"])

        fair_eval_d["y_pred"].extend(d["preds_all"])
        fair_eval_d["y_true"].extend(d["golds_all"])
        fair_eval_d["protected_attribute"].extend([_topic] * len(d["preds_all"]))

    # fairness eval:
    fair_eval_scores, fair_confusion_matrices, _ = gap_eval_scores(fair_eval_d["y_pred"], fair_eval_d["y_true"],
                                                                   fair_eval_d["protected_attribute"])
    print(fair_eval_scores)

    with open(
            evidencegrader_metrics_per_topics_base_name + (
                    '_ts' if with_temp_scaling else '') + smoothed + criterion + ".csv",
            "w") as fh_out:
        fh_out.write(
            "topic\ttotal\taurc\tlabel_ratio\terror_prop\tf1\tvariance_from_point_five\tmean_from_point_five\tmedian_from_point_five\tiqr_from_point_five\tpearson_i_from_point_five\tentropy\tvariance\tentropy_pos\tvariance_pos\tentropy_neg\tvariance_neg\tbrier\tece\n")
        for topic, error_prop in error_props.items():
            aurc = aurcs[topic] if aurcs[topic] is not None else "NA"
            fh_out.write(
                "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(topic,
                                                                                                      totals[topic],
                                                                                                      aurc,
                                                                                                      label_ratios[
                                                                                                          topic],
                                                                                                      error_prop,
                                                                                                      f1_scores[topic],
                                                                                                      variances_from_point_five[
                                                                                                          topic],
                                                                                                      mean_from_point_five[
                                                                                                          topic],
                                                                                                      median_from_point_five[
                                                                                                          topic],
                                                                                                      iqr_from_point_five[
                                                                                                          topic],
                                                                                                      pearson_i_from_point_five[
                                                                                                          topic],
                                                                                                      entropies[topic],
                                                                                                      variances[topic],
                                                                                                      entropies_pos[
                                                                                                          topic],
                                                                                                      variances_pos[
                                                                                                          topic],
                                                                                                      entropies_neg[
                                                                                                          topic],
                                                                                                      variances_neg[
                                                                                                          topic],
                                                                                                      brier_scores[
                                                                                                          topic],
                                                                                                      ece_scores[
                                                                                                          topic]))

    with open(
            evidencegrader_aurc_raw_base_name + (
                    '_ts' if with_temp_scaling else '') + smoothed + criterion + ".csv",
            "w") as fh_out:
        fh_out.write("criterion\ttopic\tcoverage\trisk\n")
        for criterion, topic, coverage, risk in aurc_raw:
            fh_out.write("{}\t{}\t{}\t{}\n".format(criterion, topic, coverage, risk))

    return brier_scores, ece_scores


def consistency_analysis_main():
    config = load_json("configs/consistency.json")
    consistency_analysis = ConsistencyAnalysis(
        rob_f=config["rob_f"],
        imp_f=config["imp_f"],
        inc_f=config["inc_f"],
        ind_f=config["ind_f"],
        pub_f=config["pub_f"],
        overall_f=config["overall_f"],
        multilab_reasons_f=config["multilab_reasons_f"]
    )
    consistency_data = consistency_analysis.prepare_data()
    consistency_analysis.consistencies(consistency_data)


def ensemble_over_kfolds():
    ensemble_preds_per_task = {}
    preds_folds, labels = prepare_data_all_folds()
    for task, probs in preds_folds.items():
        preds_array = np.array(probs)  # k * n_inst * n_labels
        mean_preds_array = np.mean(preds_array, 0)  # n_inst * n_labels
        if task == "multilab_reasons":
            preds_per_insts = []
            for inst in mean_preds_array:
                label_ids = np.where(inst > 0)[0]
                preds_per_insts.append(list(map(lambda x: labels[task][x], label_ids)))
            ensemble_preds_per_task[task] = preds_per_insts
        else:
            label_ids = np.argmax(mean_preds_array, 1)  # n_inst
            ensemble_preds_per_task[task] = list(map(lambda x: labels[task][x], label_ids))

    for task, preds in ensemble_preds_per_task.items():
        print(task)
        for pred in preds:
            print(pred)
        print()


def analyse_evidencegrader(with_temp_scaling=False, with_label_smoothing=False):
    smoothed = "_smoothed" if with_label_smoothing else ""
    saved_dir = load_json("configs/analyse.json")["saved_dir"]
    criteria = ["binary GRADE", "imprecision", "risk of bias", "indirectness", "inconsistency", "publication bias"]
    for criterion in criteria:
        print("*****************")
        print(criterion)
        print(smoothed)

        if criterion == "binary GRADE":
            generic_path = saved_dir + "/bi_class2_nonaugm" + smoothed + "_all/bi_class2_nonaugm" + smoothed + "_all_FOLD/"
        elif criterion == "imprecision":
            generic_path = saved_dir + "/bi_class_imp_nonaugm" + smoothed + "_all/bi_class_imp_nonaugm" + smoothed + "_all_FOLD/"
        elif criterion == "risk of bias":
            generic_path = saved_dir + "/bi_class_rob_nonaugm" + smoothed + "_all/bi_class_rob_nonaugm" + smoothed + "_all_FOLD/"
        elif criterion == "indirectness":
            generic_path = saved_dir + "/bi_class_ind_nonaugm" + smoothed + "_all/bi_class_ind_nonaugm" + smoothed + "_all_FOLD/"
        elif criterion == "inconsistency":
            generic_path = saved_dir + "/bi_class_inc_nonaugm" + smoothed + "_all/bi_class_inc_nonaugm" + smoothed + "_all_FOLD/"
        elif criterion == "publication bias":
            generic_path = saved_dir + "/bi_class_pub_nonaugm" + smoothed + "_all/bi_class_pub_nonaugm" + smoothed + "_all_FOLD/"

        print(analyse_confidence(generic_path=generic_path, criterion=criterion, with_temp_scaling=with_temp_scaling,
                                 with_label_smoothing=with_label_smoothing))
        print()


if __name__ == "__main__":
    analyse_evidencegrader(with_temp_scaling=False, with_label_smoothing=False)
    consistency_analysis_main()
    ensemble_over_kfolds()
