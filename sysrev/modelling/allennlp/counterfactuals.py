import pandas as pd
from allennlp.data import DataLoader
from allennlp.data.data_loaders import MultiProcessDataLoader

from sysrev.modelling.allennlp.my_project.dataset_reader import NUMERICAL_FEATS


def build_data_loaders(reader, train_data_path: str) -> DataLoader:
    train_loader = MultiProcessDataLoader(reader, train_data_path, batch_size=1, shuffle=False)

    return train_loader


def get_dataframe(data_dir, archive):
    # prepare data:
    reader = archive.dataset_reader
    reader.n_numerical = 13
    vocab = archive.model.vocab
    loader = build_data_loaders(reader, data_dir + "train.csv")
    loader.index_with(vocab)
    # create a dict of data
    training_data = {k: [] for k in NUMERICAL_FEATS + ["label"]}
    idx_to_name = dict(zip(range(len(NUMERICAL_FEATS)), NUMERICAL_FEATS))
    for i in loader:
        arr = i["feat"].flatten(0).cpu().detach().numpy()
        for idx, el in enumerate(arr):
            training_data[idx_to_name[idx]].append(el)
        label = vocab._index_to_token["labels"][i["label"].item()]
        training_data["label"].append(label)
    # convert dict to pandas data frame
    dataframe = pd.DataFrame.from_dict(training_data)

    return dataframe
    # predictor = ClassifierPredictor(archive.model, reader)
