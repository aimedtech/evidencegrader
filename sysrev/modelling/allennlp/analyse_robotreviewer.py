import os
from collections import defaultdict, Counter

import numpy as np
import scipy
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

from sysrev.dataset_construction.util.util import load_json, read_csv
from sysrev.modelling.allennlp.analyse import calculate_ece, calculate_mce, calculate_brier, get_entire_band, \
    get_label_ratio, get_risk_coverage_auc_per_band
from sysrev.modelling.allennlp.util_stat import get_pearson_i, gap_eval_scores


def get_gold(pmid, criterion, pmid2gold):
    full2abbr = {"Allocation concealment": "allo_conceal",
                 "Blinding of participants and personnel": "part_blinding",
                 "Blinding of outcome assessment": "outcome_blinding",
                 "Random sequence generation": "rand_seq_gen"}

    assert pmid2gold[pmid]
    # take first annotation (only important if there are many)
    # if no annotation for the criterion exists, we return None
    label = pmid2gold[pmid][0].get(full2abbr[criterion], None)
    if label is None:
        return None
    else:
        return {"Unclear risk": 0, "High risk": 0, "Low risk": 1}[label]


def load_preds_robotreviewer(path, criterion):

    def get_pred(prob_str, threshold=0.5):
        prob = eval(prob_str)

        return int(prob >= threshold)  # low risk if 1, high/unclear if 0

    config = load_json("configs/analyse.json")
    pmid2gold_f = config["pmid2gold_f"]
    pmid2topics_f = config["pmid2topics_f"]
    pmid2gold = load_json(pmid2gold_f)
    pmid2topics = load_json(pmid2topics_f)

    preds, golds, rests = [], [], []
    for row in read_csv(path):
        if row["domain"] != criterion:
            continue
        pmid = os.path.splitext(os.path.basename(row["filename"]))[0]
        label_gold = get_gold(pmid, criterion, pmid2gold)
        if label_gold is None:
            continue
        golds.append(label_gold)
        preds.append(get_pred(row["prob"]))
        rests.append((row["prob"], pmid2topics[pmid]))

    return preds, golds, rests


def evaluate_robotreviewer(path, criterion):
    """
    :param path: output csv file with RR predictions on pdfs, normally a path on the doe server
    :param criterion: any of:
               Random sequence generation,
               Allocation concealment,
               Blinding of participants and personnel,
               Blinding of outcome assessment
    """
    # overall:
    preds, golds, _ = load_preds_robotreviewer(path, criterion)
    print("# preds: " + str(len(preds)))
    gold_counter = Counter(golds)
    print(gold_counter[1] / sum(gold_counter.values()))
    print("acc: " + str(accuracy_score(golds, preds)))
    print("f1 macro: " + str(f1_score(golds, preds, average='macro')))
    print("precision macro: " + str(precision_score(golds, preds, average='macro')))
    print("recall macro: " + str(recall_score(golds, preds, average='macro')))
    print()


def analyse_confidence_robotreviewer(path, criterion, with_temp_scaling=False):
    """
    :param path: output csv file with RR predictions on pdfs, normally a path on the doe server
    :param criterion: any of:
               Random sequence generation,
               Allocation concealment,
               Blinding of participants and personnel,
               Blinding of outcome assessment
    """
    basename = os.path.basename(path)
    config = load_json("configs/analyse.json")
    base_dataset_dir = config["base_dataset_dir"]
    robotreviewer_metrics_per_topics_base_name = config["robotreviewer_metrics_per_topics_base_name"]
    robotreviewer_aurc_raw_base_name = config["robotreviewer_aurc_raw_base_name"]

    # per topic:
    data_per_topic = {}
    # overall:
    probs_low = []
    probs_low_pos = []
    probs_low_neg = []
    decisions = []  # whether correct or not
    preds, golds, rests = load_preds_robotreviewer(path, criterion)
    for c, rest in enumerate(rests):  # rest[0] is prob, rest[1] is topics
        prob_low_risk = eval(rest[0])
        decision = 1 if golds[c] == preds[c] else 0
        # overall:
        probs_low.append(prob_low_risk)
        if prob_low_risk >= 0.5:
            probs_low_pos.append(prob_low_risk)
        else:
            probs_low_neg.append(prob_low_risk)
        decisions.append(decision)
        # per topic:
        topics = rest[1]
        for topic in topics:
            if topic not in data_per_topic:
                data_per_topic[topic] = defaultdict(list)
            data_per_topic[topic]["probs_low"].append(prob_low_risk)
            if prob_low_risk >= 0.5:
                data_per_topic[topic]["probs_low_pos"].append(prob_low_risk)
            else:
                data_per_topic[topic]["probs_low_neg"].append(prob_low_risk)
            data_per_topic[topic]["decisions"].append(1 if golds[c] == preds[c] else 0)
            data_per_topic[topic]["golds_all"].append(golds[c])
            data_per_topic[topic]["preds_all"].append(preds[c])

    # overall
    ece_scores = {}

    pos_golds = np.array(golds) == 1
    pos_preds = np.array(preds) == 1
    accs, confs, sizes = get_entire_band(probs_low, pos_golds,
                                         outfile=base_dataset_dir + basename + "_" + criterion)  # on rat
    ece = calculate_ece(accs, confs, sizes)
    mce = calculate_mce(accs, confs)
    print("Expected Calibration Error (ECE) for entire band:  " + str(round(ece, 2)))
    print("Maximum Calibration Error (MCE) for entire band:  " + str(round(mce, 2)))
    ece_scores["overall"] = ece

    aurc_raw = []  # for plotting risk-coverage curves

    # aurc = get_risk_coverage_auc(probs_low, decisions, pos_neg_split=False)
    aurc, coverages, risks = get_risk_coverage_auc_per_band(probs_low, pos_golds, pos_preds, band="entire")
    print("AURC: " + str(aurc))

    for i in range(len(coverages)):
        aurc_raw.append((criterion, "all", coverages[i], risks[i]))

    brier_scores = {}
    brier = calculate_brier(probs_low, golds, to_onehot=False)
    print("Brier Score:  " + str(round(brier, 2)))
    brier_scores["overall"] = brier

    error_props = {}
    totals = {}
    aurcs = {}
    f1_scores = {}
    entropies = {}
    variances = {}
    variances_from_point_five = {}
    mean_from_point_five = {}
    median_from_point_five = {}
    iqr_from_point_five = {}
    pearson_i_from_point_five = {}
    entropies_pos = {}
    variances_pos = {}
    entropies_neg = {}
    variances_neg = {}
    label_ratios = {}

    fair_eval_d = {"y_pred": [], "y_true": [], "protected_attribute": []}

    # per topic
    for topic, d in data_per_topic.items():
        if len(d["probs_low"]) < 20:
            continue
        pos_golds = np.array(d["golds_all"]) == 1
        pos_preds = np.array(d["preds_all"]) == 1
        label_ratios[topic] = get_label_ratio(d["golds_all"], criterion)
        accs, confs, sizes = get_entire_band(d["probs_low"], pos_golds, suffix=topic,
                                             outfile=base_dataset_dir + basename + "_" + criterion)  # on rat
        ece = calculate_ece(accs, confs, sizes)
        # print("Expected Calibration Error (ECE) for entire band (" + topic + "):  "+ str(round(ece, 2)))
        ece_scores[topic] = ece

        aurcs[topic], coverages, risks = get_risk_coverage_auc_per_band(d["probs_low"], pos_golds, pos_preds,
                                                                        band="entire")
        for i in range(len(coverages)):
            aurc_raw.append((criterion, topic, coverages[i], risks[i]))
        brier = calculate_brier(d["probs_low"], d["golds_all"], to_onehot=False)
        # print("Brier Score:  " + str(round(brier, 2)))
        brier_scores[topic] = brier

        f1_scores[topic] = f1_score(d["golds_all"], d["preds_all"], average="macro")
        entropies[topic] = scipy.stats.entropy(d["probs_low"])
        variances[topic] = np.var(d["probs_low"])
        probs_low_np_from_point_five = np.abs(np.array(d["probs_low"]) - 0.5)
        variances_from_point_five[topic] = np.mean(probs_low_np_from_point_five ** 2)
        mean_from_point_five[topic] = np.mean(probs_low_np_from_point_five)
        median_from_point_five[topic] = np.median(probs_low_np_from_point_five)
        iqr_from_point_five[topic] = scipy.stats.iqr(probs_low_np_from_point_five)
        pearson_i_from_point_five[topic] = get_pearson_i(probs_low_np_from_point_five)
        entropies_pos[topic] = scipy.stats.entropy(d["probs_low_pos"])
        variances_pos[topic] = np.var(d["probs_low_pos"])
        entropies_neg[topic] = scipy.stats.entropy(d["probs_low_neg"])
        variances_neg[topic] = np.var(d["probs_low_neg"])

        error_props[topic] = 1 - sum(d["decisions"]) / len(d["decisions"])
        totals[topic] = len(d["decisions"])

        fair_eval_d["y_pred"].extend(d["preds_all"])
        fair_eval_d["y_true"].extend(d["golds_all"])
        fair_eval_d["protected_attribute"].extend([topic] * len(d["preds_all"]))

    add_to_name = "svm_only" if "svm_only" in basename else ""

    # fairness eval:
    fair_eval_scores, fair_confusion_matrices, _ = gap_eval_scores(fair_eval_d["y_pred"], fair_eval_d["y_true"],
                                                                   fair_eval_d["protected_attribute"])
    print(fair_eval_scores)

    with open(
            robotreviewer_metrics_per_topics_base_name + (
                    '_ts' if with_temp_scaling else '') + add_to_name + criterion + ".csv",
            "w") as fh_out:
        fh_out.write(
            "topic\ttotal\taurc\tlabel_ratio\terror_prop\tf1\tvariance_from_point_five\tmean_from_point_five\tmedian_from_point_five\tiqr_from_point_five\tpearson_i_from_point_five\tentropy\tvariance\tentropy_pos\tvariance_pos\tentropy_neg\tvariance_neg\tbrier\tece\n")
        for topic, error_prop in error_props.items():
            aurc = aurcs[topic] if aurcs[topic] is not None else "NA"
            fh_out.write(
                "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(topic,
                                                                                                      totals[topic],
                                                                                                      aurc,
                                                                                                      label_ratios[
                                                                                                          topic],
                                                                                                      error_prop,
                                                                                                      f1_scores[topic],
                                                                                                      variances_from_point_five[
                                                                                                          topic],
                                                                                                      mean_from_point_five[
                                                                                                          topic],
                                                                                                      median_from_point_five[
                                                                                                          topic],
                                                                                                      iqr_from_point_five[
                                                                                                          topic],
                                                                                                      pearson_i_from_point_five[
                                                                                                          topic],
                                                                                                      entropies[topic],
                                                                                                      variances[topic],
                                                                                                      entropies_pos[
                                                                                                          topic],
                                                                                                      variances_pos[
                                                                                                          topic],
                                                                                                      entropies_neg[
                                                                                                          topic],
                                                                                                      variances_neg[
                                                                                                          topic],
                                                                                                      brier_scores[
                                                                                                          topic],
                                                                                                      ece_scores[
                                                                                                          topic]))
    with open(
            robotreviewer_aurc_raw_base_name + (
                    '_ts' if with_temp_scaling else '') + add_to_name + criterion + ".csv",  # on rat
            "w") as fh_out:
        fh_out.write("criterion\ttopic\tcoverage\trisk\n")
        for criterion, topic, coverage, risk in aurc_raw:
            fh_out.write("{}\t{}\t{}\t{}\n".format(criterion, topic, coverage, risk))

    return brier_scores, ece_scores


def analyse_robotreviewer(with_temp_scaling=False):
    config = load_json("configs/analyse.json")
    for criterion in ["Random sequence generation", "Allocation concealment", "Blinding of participants and personnel",
                      "Blinding of outcome assessment"]:
        print("*****************")
        print(criterion)
        path = config["path_robotreviewer_preds"]
        evaluate_robotreviewer(path, criterion=criterion)
        print(
            analyse_confidence_robotreviewer(path, criterion=criterion, with_temp_scaling=with_temp_scaling))
        print()


if __name__ == "__main__":
    analyse_robotreviewer()
