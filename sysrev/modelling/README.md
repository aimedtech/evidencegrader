# Automatic appraisal of quality of evidence in Cochrane systematic reviews

For training and evaluating a model, look at [modelling](allennlp/README.md).

For generating (experimental) counterfactual explanations, see [counterfactuals](counterfactuals/play.py).