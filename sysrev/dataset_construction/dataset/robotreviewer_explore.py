import argparse
from collections import Counter

from sysrev.dataset_construction.util.util import load_json


def label_distribution(d):
    criteria = {"allo_conceal", "blinding", "outcome_blinding", "rand_seq_gen"}
    stats = {criterion: Counter() for criterion in criteria}

    for pmid, annos in d.items():
        for anno in annos:
            if not anno:
                continue
            for criterion, label in anno.items():
                if criterion in criteria:
                    stats[criterion].update([label])

    return stats


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f',
                        help='input file: dataset in json format')
    args = parser.parse_args()

    d = load_json(args.input_f)
    print(label_distribution(d))
