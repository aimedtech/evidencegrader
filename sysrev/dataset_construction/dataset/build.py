import argparse
import csv
from collections import defaultdict, Counter
from pathlib import Path

import numpy as np

from sysrev.dataset_construction.parse.evaluation.evaluation import GRADE_TO_STEPS
from sysrev.dataset_construction.util.util import get_file_list, load_json, save_json


def get_valid_docids(fs):
    """
    :param fs: all parsed filenames
    :return: a set of docids+outcome identifiers that cover all grade=='high' instances and all other instances where correct number of
    downgrading reasons is given. Instances for which the number of reasons does not correspond to GRADE score are
    ignored.
    """
    docid_to_outcome_id = defaultdict(list)

    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is None:
                    continue
                if grade == "other":
                    continue
                if outcome["reasons"] is None:
                    if grade == "high":
                        docid_to_outcome_id[fh["docid"]].append(
                            {"outcome_id": outcome["id"], "table_id": sof["table_id"]})
                        continue
                    else:
                        continue
                if sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[grade]:
                    docid_to_outcome_id[fh["docid"]].append({"outcome_id": outcome["id"], "table_id": sof["table_id"]})

    return docid_to_outcome_id


def get_invalid_docids(fs):
    """
    :param fs: all parsed filenames
    :return: a set of docids+outcome identifiers that cover all instances where incorrect number of
    downgrading reasons is given (instances for which the number of reasons does not correspond to GRADE score)
    """
    docid_to_outcome_id = defaultdict(list)

    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is None:
                    continue
                if grade == "other":
                    continue
                if outcome["reasons"] is None:
                    if grade != "high":
                        docid_to_outcome_id[fh["docid"]].append(
                            {"outcome_id": outcome["id"], "table_id": sof["table_id"]})
                    else:
                        continue
                if sum(outcome["reasons"]["labels"].values()) != GRADE_TO_STEPS[grade]:
                    docid_to_outcome_id[fh["docid"]].append({"outcome_id": outcome["id"], "table_id": sof["table_id"]})

    return docid_to_outcome_id


def write_folds_meta(folds, output_dir):
    f_out = f"{output_dir}/fold_ids.json"
    save_json(folds, f_out)


def write_meta_invalid(folds, output_dir):
    f_out = f"{output_dir}/ids_invalid.json"
    save_json(folds, f_out)


def create_folds(docids_to_outcomeids, k=10):
    """
    Shuffle the docids and split them into k similarly-sized folds. This adheres to strong separation between documents
    (reviews), so no review occurs in >1 fold, although reviews are typically associated with several data instances
    (outcomes).

    :return: {fold_n: [{table_id: [outcome_id]}]}
    """
    docids = list(docids_to_outcomeids.keys())
    np.random.seed(4)
    np.random.shuffle(docids)
    docid_folds = np.array_split(docids, k)
    folds = {n: {} for n in range(k)}  # keep fold ids

    for n, fold in enumerate(docid_folds):
        print(sum([len(docids_to_outcomeids[docid]) for docid in fold]))
        for docid in fold:
            folds[n][docid] = docids_to_outcomeids[docid]

    return folds


def initialise_inst(fh):
    inst = fh.copy()
    del inst["sofs"]
    inst["sofs"] = []

    return inst


def fill_inst(inst, sof_to_outcomes, sofs):
    for s in sofs:
        if s["table_id"] in sof_to_outcomes:
            new_sof = s.copy()
            del new_sof["outcomes"]
            new_sof["outcomes"] = sof_to_outcomes[s["table_id"]]
            inst["sofs"].append(new_sof)


def write_folds_data(folds, input_dir, output_dir):
    dataset = {fold_n: [] for fold_n in folds.keys()}

    # iterate over doc, table and outcome ids in each fold
    for fold_n, ids in folds.items():
        for docid, outcome_id_list in ids.items():
            table_ids = {i["table_id"] for i in outcome_id_list}
            # open parsed doc
            fh = load_json(f"{input_dir}/{docid}.json")
            # prepopulate the new instance with everything from parsed, except for 'sofs'
            inst = initialise_inst(fh)
            # get relevant 'sofs' and outcomes
            sof_to_outcomes = defaultdict(list)
            for sof in fh["sofs"]:
                if sof["table_id"] not in table_ids:
                    continue
                outcome_ids = {i["outcome_id"] for i in outcome_id_list if i["table_id"] == sof["table_id"]}
                for outcome in sof["outcomes"]:
                    if outcome["id"] not in outcome_ids:
                        continue
                    sof_to_outcomes[sof["table_id"]].append(outcome)
            if sof_to_outcomes:
                fill_inst(inst, sof_to_outcomes, fh["sofs"])
                dataset[fold_n].append(inst)

    f_out = f"{output_dir}/dataset.json"
    save_json(dataset, f_out)


def write_invalid_data(docids, input_dir, output_dir):
    dataset = []

    # iterate over doc, table and outcome ids in each fold
    for docid, outcome_id_list in docids.items():
        table_ids = {i["table_id"] for i in outcome_id_list}
        # open parsed doc
        fh = load_json(f"{input_dir}/{docid}.json")
        # prepopulate the new instance with everything from parsed, except for 'sofs'
        inst = initialise_inst(fh)
        # get relevant 'sofs' and outcomes
        sof_to_outcomes = defaultdict(list)
        for sof in fh["sofs"]:
            if sof["table_id"] not in table_ids:
                continue
            outcome_ids = {i["outcome_id"] for i in outcome_id_list if i["table_id"] == sof["table_id"]}
            for outcome in sof["outcomes"]:
                if outcome["id"] not in outcome_ids:
                    continue
                sof_to_outcomes[sof["table_id"]].append(outcome)
        if sof_to_outcomes:
            fill_inst(inst, sof_to_outcomes, fh["sofs"])
            dataset.append(inst)

    f_out = f"{output_dir}/dataset_invalid.json"
    save_json(dataset, f_out)


def build_invalid(fs, input_dir, output_dir):
    docids_to_outcomeids = get_invalid_docids(fs)

    write_meta_invalid(docids_to_outcomeids, output_dir)  # write ids per fold
    write_invalid_data(docids_to_outcomeids, input_dir, output_dir)  # write actual data as one file


def build(fs, input_dir, output_dir):
    docids_to_outcomeids = get_valid_docids(fs)

    folds = create_folds(docids_to_outcomeids)  # {fold_n: [{table_id: [outcome_id]}]}
    write_folds_meta(folds, output_dir)  # write ids per fold
    write_folds_data(folds, input_dir, output_dir)  # write actual data as one file


def grade_per_year(fs, output_dir):
    counters = {}
    for f in fs:
        fh = load_json(f)
        year = fh["year"]
        if year not in counters:
            counters[year] = Counter()

        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is None:
                    continue
                if grade == "other":
                    continue
                counters[year].update([grade])

    # print(counters)
    with open(output_dir + "high_all", mode='w') as fh_out_high, \
            open(output_dir + "moderate_all", mode='w') as fh_out_moderate, \
            open(output_dir + "low_all", mode='w') as fh_out_low, \
            open(output_dir + "very_low_all", mode='w') as fh_out_very_low:

        writer_high = csv.writer(fh_out_high)
        writer_mod = csv.writer(fh_out_moderate)
        writer_low = csv.writer(fh_out_low)
        writer_vlow = csv.writer(fh_out_very_low)

        writer_high.writerow(("year", "n"))
        writer_mod.writerow(("year", "n"))
        writer_low.writerow(("year", "n"))
        writer_vlow.writerow(("year", "n"))

        for year, counter in sorted(counters.items()):
            writer_high.writerow((year, counter.get("high", 0)))
            writer_mod.writerow((year, counter.get("moderate", 0)))
            writer_low.writerow((year, counter.get("low", 0)))
            writer_vlow.writerow((year, counter.get("very low", 0)))


def simple_selector(fh, i):
    return fh[str(i)]


def split_folds(fh, selector=simple_selector):
    """
    :param fh: {fold_n: [inst]}
    :param selector: takes desired elements from fh at certain fold_n

    Organise the folds into train, dev and test sets. Dev set is always the fold prior to test. Both dev and test
    are one fold each.
    """
    k = max([int(i) for i in fh.keys()]) + 1
    fold_n_set = set(range(k))
    for fold_n in range(k):
        dev_fold = fold_n
        if fold_n == k - 1:  # last fold: take that fold as dev, and first fold as test
            test_fold = 0
        else:
            test_fold = fold_n + 1
        train_folds = sorted(fold_n_set.difference({dev_fold, test_fold}))

        trainset = []
        for i in train_folds:
            trainset.extend(selector(fh, i))
        devset = selector(fh, dev_fold)
        testset = selector(fh, test_fold)

        yield train_folds, dev_fold, test_fold, trainset, devset, testset


def write_splits(iter_splits, output_dir):
    for n, (train_folds, dev_fold, test_fold, trainset, devset, testset) in enumerate(iter_splits):
        dir_out = f"{output_dir}/splits/{n}/"
        if not Path(dir_out).exists():
            Path(dir_out).mkdir(parents=True)

        save_json(train_folds, f"{dir_out}train_folds.json")
        save_json(trainset, f"{dir_out}train.json")

        save_json(dev_fold, f"{dir_out}dev_fold.json")
        save_json(devset, f"{dir_out}dev.json")

        save_json(test_fold, f"{dir_out}test_fold.json")
        save_json(testset, f"{dir_out}test.json")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_dir',
                        help='input dir containing parsed json files')
    parser.add_argument('-metadata_f',
                        help='path to metadata.csv')
    parser.add_argument('-output_dir',
                        help='output dir containing parsed json files for the dataset')
    args = parser.parse_args()

    if not Path(args.output_dir).exists():
        Path(args.output_dir).mkdir()

    fs = get_file_list(args.input_dir)

    # grade_per_year(fs, args.output_dir)

    # prepare data and split into 10 folds, write to dataset.json
    build(fs, args.input_dir, args.output_dir)
    # prepare 'invalid' data (those with incorrect n of downgrading steps), write to dataset_invalid.json
    build_invalid(fs, args.input_dir, args.output_dir)

    # prepare train, dev and test splits for each fold set
    f_in = f"{args.output_dir}/dataset.json"
    fh = load_json(f_in)
    iter_splits = split_folds(fh)
    write_splits(iter_splits, args.output_dir)
