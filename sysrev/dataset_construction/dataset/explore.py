import argparse
import csv
import re
from collections import Counter, defaultdict
from pathlib import Path

import numpy as np
# from krippendorff import krippendorff
from scipy.stats import describe

from sysrev.dataset_construction.util.util import load_json, read_csv


# from sysrev.modelling.allennlp.my_project.dataset_reader import get_cochrane_id

def grade_per_year(fh, output_dir):
    counters = {}
    for fold_n, insts in fh.items():
        for inst in insts:
            year = inst["year"]
            if year not in counters:
                counters[year] = Counter()

            for sof in inst["sofs"]:
                for outcome in sof["outcomes"]:
                    counters[year].update([outcome["grade"]])

    # print(counters)
    with open(output_dir + "high", mode='w') as fh_out_high, \
            open(output_dir + "moderate", mode='w') as fh_out_moderate, \
            open(output_dir + "low", mode='w') as fh_out_low, \
            open(output_dir + "very_low", mode='w') as fh_out_very_low:

        writer_high = csv.writer(fh_out_high)
        writer_mod = csv.writer(fh_out_moderate)
        writer_low = csv.writer(fh_out_low)
        writer_vlow = csv.writer(fh_out_very_low)

        writer_high.writerow(("year", "n"))
        writer_mod.writerow(("year", "n"))
        writer_low.writerow(("year", "n"))
        writer_vlow.writerow(("year", "n"))

        for year, counter in sorted(counters.items()):
            writer_high.writerow((year, counter.get("high", 0)))
            writer_mod.writerow((year, counter.get("moderate", 0)))
            writer_low.writerow((year, counter.get("low", 0)))
            writer_vlow.writerow((year, counter.get("very low", 0)))


def grade_per_fold(fh, output_dir):
    counters = {}
    for fold_n, insts in fh.items():
        for inst in insts:
            if fold_n not in counters:
                counters[fold_n] = Counter()

            for sof in inst["sofs"]:
                for outcome in sof["outcomes"]:
                    counters[fold_n].update([outcome["grade"]])

    # print(counters)
    with open(output_dir + "high", mode='w') as fh_out_high, \
            open(output_dir + "moderate", mode='w') as fh_out_moderate, \
            open(output_dir + "low", mode='w') as fh_out_low, \
            open(output_dir + "very_low", mode='w') as fh_out_very_low:

        writer_high = csv.writer(fh_out_high)
        writer_mod = csv.writer(fh_out_moderate)
        writer_low = csv.writer(fh_out_low)
        writer_vlow = csv.writer(fh_out_very_low)

        writer_high.writerow(("fold_n", "n"))
        writer_mod.writerow(("fold_n", "n"))
        writer_low.writerow(("fold_n", "n"))
        writer_vlow.writerow(("fold_n", "n"))

        for year, counter in sorted(counters.items()):
            writer_high.writerow((year, counter.get("high", 0)))
            writer_mod.writerow((year, counter.get("moderate", 0)))
            writer_low.writerow((year, counter.get("low", 0)))
            writer_vlow.writerow((year, counter.get("very low", 0)))


def rob_studies_ids(fh, pmids_to_rob_judgs=None, ids_ignore_f="/home/simon/Apps/robotreviewer/PMIDs_RR.csv",
                    doi2pmid=None):
    """
    Obtain a mapping between pmid and RoB annotations. Ignore those pmids that occur in 'ids_ignore', i.e. those used
    in the development of RobotReviewer.
    """
    ids_ignore = {row["PMIDs"] for row in read_csv(ids_ignore_f)}
    rob_criteria = ['allo_conceal', 'blinding', 'incomplete_out_data', 'other_bias', 'outcome_blinding',
                    'part_blinding',
                    'rand_seq_gen', 'selective_reporting']

    if pmids_to_rob_judgs is None:
        pmids_to_rob_judgs = defaultdict(list)

    for fold_n, insts in fh.items():
        for inst in insts:
            studies = inst["studies"]["included"]
            for study in studies:
                for ref in study["references"]:
                    if ref["pmid"] is not None:
                        pmid = ref["pmid"]
                    elif ref["doi"] is not None:
                        pmid = doi2pmid.get(ref["doi"], None)
                    else:
                        pmid = None
                    if pmid is None or pmid in ids_ignore:
                        continue
                    pmids_to_rob_judgs[pmid].append(dict())
                    for rob_criterion in rob_criteria:
                        judg = study["study_chars"]["rob"][rob_criterion]["judgement"]
                        if judg is not None:
                            pmids_to_rob_judgs[pmid][-1][rob_criterion] = judg

    c = Counter()
    for pmid, judgs in pmids_to_rob_judgs.items():
        c[len(judgs)] += 1
    print(f"Counter for the number of annotations (>1) per study: {c}")

    return pmids_to_rob_judgs


def rob_studies(fh):
    rob_criteria = ['allo_conceal', 'blinding', 'incomplete_out_data', 'other_bias', 'outcome_blinding',
                    'part_blinding',
                    'rand_seq_gen', 'selective_reporting']
    for rob_criterion in rob_criteria:
        gs_citation_to_rob_judgs = defaultdict(list)
        for fold_n, insts in fh.items():
            for inst in insts:
                studies = inst["studies"]["included"]
                for study in studies:
                    study_refs = []
                    for ref in study["references"]:
                        if ref["gs_citation"] is not None:
                            study_refs.append(ref["gs_citation"])
                    study_refs = sorted(study_refs)
                    if not study_refs:
                        continue
                    judg = study["study_chars"]["rob"][rob_criterion]["judgement"]
                    # gs_citation_to_allo_conceal[str(study_refs)].append((judg, inst["doi"]))
                    if judg is not None:
                        gs_citation_to_rob_judgs[str(study_refs)].append(judg)

        rob_annos = {gs_citations: judgs for gs_citations, judgs in gs_citation_to_rob_judgs.items() if len(judgs) > 1}

        labels = {judg for judgs in rob_annos.values() for judg in judgs}

        print(rob_criterion)
        print(f"Number of annotations (>1): {len(rob_annos)}")
        c = Counter()
        for gs_citations, judgs in rob_annos.items():
            c[len(judgs)] += 1
        print(f"Counter for the number of annotations (>1) per study: {c}")

        reliability_data = [[] for _ in range(max(c.keys()))]
        for gs_citations, judgs in rob_annos.items():
            for i in range(len(reliability_data)):
                try:
                    judg = judgs[i]
                except IndexError:
                    judg = np.nan
                reliability_data[i].append(judg)

        assert len(reliability_data[0]) == len(reliability_data[1]) == len(reliability_data[2]) == len(
            reliability_data[3])
        alpha = krippendorff.alpha(reliability_data=reliability_data, level_of_measurement='ordinal',
                                   value_domain=list(labels))
        print(f"Krippendorff's alpha: {round(alpha, 2)}\n\n")


def studies_statistics(fh):
    studies_per_docid = defaultdict(set)
    studies_titles_per_docid = defaultdict(set)
    years = set()
    sofs_len = list()
    grade_faceted_topic = {}
    count_faceted_topic = Counter()
    grade_faceted_I = {}
    grade_faceted_O = {}
    topic_freq = Counter()
    inst_total = 0
    for fold_n, insts in fh.items():
        for inst in insts:
            years.add(inst["year"])
            studies = inst["studies"]["included"]
            sofs_len.append(len(inst["sofs"]) if inst["sofs"] is not None else 0)
            for study in studies:
                studies_per_docid[inst["docid"]].add(study["id"])
                studies_titles_per_docid[inst["docid"]].add(study["study_chars"]["title"])
            for sof in inst["sofs"]:
                for outcome in sof["outcomes"]:
                    if outcome["O"] is not None:
                        if outcome["O"].lower() not in grade_faceted_O:
                            grade_faceted_O[outcome["O"].lower()] = defaultdict(int)
                        grade_faceted_O[outcome["O"].lower()][outcome["grade"]] += 1
                    if sof["I"] is not None:
                        if sof["I"].lower() not in grade_faceted_I:
                            grade_faceted_I[sof["I"].lower()] = defaultdict(int)
                        grade_faceted_I[sof["I"].lower()][outcome["grade"]] += 1

                    for topic in inst["topics"]:
                        if topic not in grade_faceted_topic:
                            grade_faceted_topic[topic] = defaultdict(int)
                        count_faceted_topic[topic] += 1
                        grade_faceted_topic[topic][outcome["grade"]] += 1
                        grade_faceted_topic[topic][outcome["grade"] + "_n_participants"] += outcome["n_participants"] if \
                            outcome["n_participants"] is not None else 0

                    topic_freq[str(set(sorted(inst["topics"])))] += 1
                    inst_total += 1

    study_lens = []
    for studies in studies_per_docid.values():
        study_lens.append(len(studies))

    print(describe(study_lens))

    studies_titles = {study for studies in studies_titles_per_docid.values() for study in studies}
    print(len(studies_titles))

    print(np.array(list(years)).min())

    print(describe(sofs_len))
    # print(grade_faceted_topic)
    """
    # for grade_faceted in [grade_faceted_I, grade_faceted_O, grade_faceted_topic]:
    for grade_faceted in [grade_faceted_topic]:
        print("facet\tprop_high\tprop_low\ttotal\ttotal_n_participants")
        for facet, grades in grade_faceted.items():
            total = sum([v for k, v in grades.items() if not k.endswith('_n_participants')])
            total_n_participants = sum([v for k, v in grades.items() if k.endswith('_n_participants')])
            if total < 20:
                continue
            n_high, n_mod, n_low, n_vlow = (0,) * 4
            print_string = facet
            if "high" in grades:
                n_high = grades["high"]
            if "moderate" in grades:
                n_mod = grades["moderate"]
            print_string += f"\t{round((n_high + n_mod) / total, 2)}"
            if "low" in grades:
                n_low = grades["low"]
            if "very low":
                n_vlow = grades["very low"]
            print_string += f'\t{round((n_low + n_vlow) / total, 2)}'
            print_string += f'\t{total}'
            print_string += f'\t{total_n_participants / total}'
            print(print_string)
        print()
    """
    print(count_faceted_topic.most_common())
    print(inst_total)


def get_cochrane_id(txt):
    found = re.findall("CD\d+", txt)
    return found[0] if found else None


def get_linked_data(inst, linked_data):
    cochrane_id = get_cochrane_id(inst["doi"])

    return linked_data[cochrane_id] if cochrane_id in linked_data else None


class LinkedDatum:
    def __init__(self, l):
        self.cochrane_url = l["cochrane_url"]
        self.cochrane_id = get_cochrane_id(self.cochrane_url)
        self.concept_url = l["concept_url"]
        self.concept_name = l["concept_name"]
        self.ontology = l["ontology"]
        self.concept_type = l["concept_type"]

    def __eq__(self, other):
        if self.cochrane_id == other.cochrane_id and self.concept_url == other.concept_url:
            return True
        else:
            return False


def load_linked_data(linked_data_path):
    source_data = read_csv(linked_data_path,
                           keys=["cochrane_url", "concept_url", "concept_name", "ontology", "concept_type"])
    linked_data = {}
    for l in source_data:
        linked_datum = LinkedDatum(l)
        if linked_datum.cochrane_id not in linked_data:
            linked_data[linked_datum.cochrane_id] = []
        if linked_datum not in linked_data[linked_datum.cochrane_id]:
            linked_data[linked_datum.cochrane_id].append(linked_datum)

    return linked_data


def studies_statistics_cochrane_linked(fh,
                                       linked_data_path="/home/simon/Apps/SysRev/data/PICO_annotation_per_review_2020-11-30.csv"):
    studies_per_docid = defaultdict(set)
    studies_titles_per_docid = defaultdict(set)
    grade_faceted = {}

    linked_data = load_linked_data(linked_data_path)
    for fold_n, insts in fh.items():
        for inst in insts:
            linked_data_for_inst = get_linked_data(inst, linked_data)
            if linked_data_for_inst is not None:
                for sof in inst["sofs"]:
                    for outcome in sof["outcomes"]:
                        for datum in linked_data_for_inst:
                            if datum.concept_type not in grade_faceted:
                                grade_faceted[datum.concept_type] = {}
                            if datum.concept_name not in grade_faceted[datum.concept_type]:
                                grade_faceted[datum.concept_type][datum.concept_name] = defaultdict(int)
                            grade_faceted[datum.concept_type][datum.concept_name][outcome["grade"]] += 1

    print("facet\tprop_high\tprop_low\ttotal")
    for facet_type, facet_data in grade_faceted.items():
        print("\n\n" + facet_type)
        for facet, grades in facet_data.items():
            if facet_type == "Outcome" and sum(grades.values()) < 300:
                continue
            if sum(grades.values()) < 100:
                continue
            n_high, n_mod, n_low, n_vlow = (0,) * 4
            print_string = facet
            if "high" in grades:
                n_high = grades["high"]
            if "moderate" in grades:
                n_mod = grades["moderate"]
            print_string += f"\t{round((n_high + n_mod) / sum(grades.values()), 2)}"
            if "low" in grades:
                n_low = grades["low"]
            if "very low":
                n_vlow = grades["very low"]
            print_string += f'\t{round((n_low + n_vlow) / sum(grades.values()), 2)}'
            print_string += f'\t{sum(grades.values())}'
            print(print_string)
        print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f',
                        help='input file: dataset in json format')
    parser.add_argument('-output_dir', required=False,
                        help='output dir to which potential output files are written to.')
    args = parser.parse_args()
    if args.output_dir is not None:
        if not Path(args.output_dir).exists():
            Path(args.output_dir).mkdir()

    fh = load_json(args.input_f)
    # rob_studies(fh)
    # pmids_to_rob_judgs = rob_studies_ids(fh)
    # fh2 = load_json(args.input_f2)
    # pmids_to_rob_judgs = rob_studies_ids(fh2, pmids_to_rob_judgs=pmids_to_rob_judgs)

    studies_statistics(fh)
    # studies_statistics_cochrane_linked(fh)

    # grade_per_year(fh, args.output_dir)
    # grade_per_fold(fh, args.output_dir)

    # {
    # "grade": "",
    # "reasons": "",
    # "Parsed": {"title": "",
    #            "authors": "",
    #            "doi": "",
    #            "review_type": ""}
    # "SoF": {"table_id": "",
    #         "P": "",
    #         "I": "",
    #         "C": "",
    #         "setting": "",
    #         "absolute_effect_header": "",
    #         "abbrs": "",
    #         "table_comment": "",
    #         "title"}
    # "Outcome": {"O": "",
    #             "ci_lower": "",
    #             "ci_upper": "",
    #             "comment": "",
    #             "n_participants": "",
    #             "n_studies": "",
    #             "relative_effect": "",
    #             "type_effect": ""}
    # "Studies": {"included": [{"id":"",
    #                           "status": "",
    #                           "title": "",
    #                           "references": [{"id": "",
    #                                           "gs_citation": ""}],
    #                           "study_chars": [{"study_id": "",
    #                                            "char": {"methods": "",
    #                                                     "participants": "",
    #                                                     "interventions": "",
    #                                                     "outcomes": "",
    #                                                     "notes": "",
    #                                                     "other": ""}],
    #                                            "RoB": {}
#
# excluded
# additional
# ongoing
# other
