import argparse
import csv
import sys
from pathlib import Path

import numpy as np


# from sysrev.dataset_construction.util.util import load_json


def parse_error(labels):
    return bool(set(labels) - {"indirectness", "imprecision", "inconsistency", "risk of bias", "publication bias"})


def iter_all(fold_n, insts, tenth_part=10):
    for inst in insts:
        for sof in inst["sofs"]:
            for outcome in sof["outcomes"]:
                # remove the few wrongly parsed instance:
                if parse_error(outcome["reasons"]["labels"]):
                    continue
                # sample if necessary
                if np.random.randint(1, 11) <= tenth_part:
                    yield {
                        "fold_n": fold_n,
                        "title": inst["title"],
                        "authors": inst["authors"],
                        "doi": inst["doi"],
                        "full_abstract_txt": inst["texts"]["full_abstract_txt"],
                        "abstract_conclusion_txt": inst["texts"]["abstract_conclusion_txt"],
                        "plain_language_summary_txt": inst["texts"]["plain_language_summary_txt"],
                        "authors_conclusions_txt": inst["texts"]["authors_conclusions_txt"],
                        "review_type": inst["review_type"],
                        "topics": inst["topics"],
                        "year": inst["year"],
                        "n_sofs": len(inst["sofs"]),
                        "P": sof["P"],
                        "I": sof["I"],
                        "C": sof["C"],
                        "sof_title": sof["title"],
                        "table_id": sof["table_id"],
                        "table_comment": sof["table_comment"],
                        "setting": sof["setting"],
                        "absolute_effect_header": sof["absolute_effect_header"],
                        "abbrs": sof["abbrs"],
                        "n_outcomes": len(sof["outcomes"]),
                        "O": outcome["O"],
                        "Grade": outcome["grade"],
                        "Reasons": outcome["reasons"]["labels"],
                        "reasons_raw": outcome["reasons"]["raw"],
                        "absolute_effect": outcome["absolute_effect"],
                        "ci_lower": outcome["ci_lower"],
                        "ci_upper": outcome["ci_upper"],
                        "comment": outcome["comment"],
                        "n_participants": outcome["n_participants"],
                        "n_studies": outcome["n_studies"],
                        "relative_effect": outcome["relative_effect"],
                        "type_effect": outcome["type_effect"],
                        "n_included_studies": len(inst["studies"]["included"]),
                        "n_excluded_studies": len(inst["studies"]["excluded"]),
                        "n_ongoing_studies": len(inst["studies"]["ongoing"]),
                        "n_additional_studies": len(inst["studies"]["additional"]),
                        "n_other_studies": len(inst["studies"]["other"])
                    }


def iter_picogr(fold_n, insts):
    for inst in insts:
        for sof in inst["sofs"]:
            for outcome in sof["outcomes"]:
                # remove the few wrongly parsed instance:
                if parse_error(outcome["reasons"]["labels"]):
                    continue
                yield {"fold_n": fold_n,
                       "P": sof["P"],
                       "I": sof["I"],
                       "C": sof["C"],
                       "O": sof["O"],
                       "Grade": outcome["grade"],
                       "Reasons": outcome["reasons"]["labels"],
                       }


def iter_picogr_folds(fh):
    """
    Get P, I, C, O, grade, reason
    """
    for fold_n, insts in fh.items():
        for fields in iter_picogr(fold_n, insts):
            yield fields


def write(insts, output_dir, output_f, max_idx=None):
    assert sys.version_info >= (3, 6), "Python versions <3.6 don't support dict insertion order."

    with open(output_dir + output_f, mode='w') as fh_out:
        writer = csv.writer(fh_out)
        for i, inst in enumerate(insts):
            if i == 0:  # csv header line
                writer.writerow(inst.keys())
            if max_idx is not None and i > max_idx:
                break
            writer.writerow(inst.values())


def write_tenth_parts(trainset, out_dir):
    # smaller parts of training data to produce a learning curve
    # get total len of the trainset
    c = 0
    for _ in iter_all(n, trainset):
        c += 1
    # ten approx. equally sized parts
    part_idxs = np.array_split(range(c), 10)

    for tenth_part in range(1, 11):
        max_idx = np.max(part_idxs[tenth_part - 1])
        write(iter_all(n, trainset), out_dir, "train_" + str(tenth_part) + "_10.csv", max_idx)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-input_f',
                       help='input file: dataset in json format')
    group.add_argument("-input_dir", help="directory containing splits")
    parser.add_argument('-output_dir',
                        help='output dir to which csv split data will be written')
    args = parser.parse_args()
    if not Path(args.output_dir).exists():
        Path(args.output_dir).mkdir()

    # re-write entire dataset.json as picogr.csv
    # fh = load_json(args.input_f)
    # insts = iter_picogr_folds(fh)
    # write(insts, args.output_dir, "picogr.csv")

    # prepare train, dev and test splits for each fold set
    k = 10
    for n in range(k):
        dir_in = "{}{}{}/".format(args.input_dir, "/splits/", n)
        trainset = load_json(dir_in + "train.json")
        devset = load_json(dir_in + "dev.json")
        testset = load_json(dir_in + "test.json")

        out_dir = "{}{}{}/".format(args.output_dir, "/splits/", n)
        if not Path(out_dir).exists():
            Path(out_dir).mkdir(parents=True)

        # write(iter_picogr(n, trainset), out_dir, "train.csv")
        # write(iter_picogr(n, devset), out_dir, "dev.csv")
        # write(iter_picogr(n, testset), out_dir, "test.csv")

        write(iter_all(n, trainset), out_dir, "train.csv")
        write(iter_all(n, devset), out_dir, "dev.csv")
        write(iter_all(n, testset), out_dir, "test.csv")

        # smaller parts of training data to produce a learning curve; only for the first CV iteration
        if n == 0:
            write_tenth_parts(trainset, out_dir)
