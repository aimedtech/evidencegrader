import argparse
import csv
import random
from pathlib import Path

from tqdm import tqdm

"""
Combine training data into a single, shuffled csv file.
"""


def create_one(output_dir, input_f_invalid, input_f_train):
    output_f = f"{output_dir}/train_split0_and_dataset_invalid_shuf.csv"

    rows = []
    with open(input_f_invalid) as fh:
        reader = csv.reader(fh)
        fieldnames_invalid = reader.__next__()
        for row in tqdm(reader):
            rows.append(row)

    with open(input_f_train) as fh:
        reader = csv.reader(fh)
        fieldnames = reader.__next__()
        assert fieldnames == fieldnames_invalid
        for row in tqdm(reader):
            rows.append(row)

    random.shuffle(rows)

    with open(output_f, "w") as fh_out:
        writer = csv.writer(fh_out)
        writer.writerow(fieldnames)
        for l in tqdm(rows):
            writer.writerow(l)


def get_rows(output_dir, input_f_invalid, filter_func):
    output_dir = f"{output_dir}/splits/"

    rows_invalid = []
    with open(input_f_invalid) as fh:
        reader = csv.DictReader(fh)
        fieldnames_invalid = reader.fieldnames
        for row in tqdm(reader):
            if filter_func(row):
                rows_invalid.append(list(row.values()))

    for k in range(10):
        curr_output_dir = f"{output_dir}{k}"

        rows = []
        with open(f"{curr_output_dir}/train.csv") as fh:
            reader = csv.reader(fh)
            fieldnames = reader.__next__()
            assert fieldnames == fieldnames_invalid
            for row in tqdm(reader):
                rows.append(row)

        all_rows = rows + rows_invalid
        random.shuffle(all_rows)

        yield all_rows, fieldnames, curr_output_dir


def write_rows(curr_output_dir, output_f_name, fieldnames, rows_per_fold):
    output_f = f"{curr_output_dir}/{output_f_name}.csv"
    with open(output_f, "w") as fh_out:
        writer = csv.writer(fh_out)
        writer.writerow(fieldnames)
        for l in tqdm(rows_per_fold):
            writer.writerow(l)


def create_all(output_dir, input_f_invalid, output_f_name="train_and_dataset_invalid_shuf", filter_func=lambda x: True):
    """
    :param filter_func: any function to accept or refuse a row from the invalid part of the dataset from being included.
    """
    # obtain the right rows and write them to the disk
    for rows_per_fold, fieldnames, curr_output_dir in get_rows(output_dir, input_f_invalid, filter_func):
        write_rows(curr_output_dir, output_f_name, fieldnames, rows_per_fold)


def balance_rows(rows, filter_func):
    # separate into positive and negative rows
    positive_rows, negative_rows = [], []
    for l in tqdm(rows):
        if filter_func(l):
            positive_rows.append(l)
        else:
            negative_rows.append(l)

    n_pos, n_neg = len(positive_rows), len(negative_rows)
    print(f"N positive: {n_pos}")
    print(f"N negative: {n_neg}")

    if n_neg < n_pos:
        print("N pos > n neg. Will not downsample.")
        return None

    # sub-sample negative rows
    random.shuffle(negative_rows)
    negative_rows = negative_rows[:n_pos]

    # concatenate all rows and shuffle
    all_rows = positive_rows + negative_rows
    random.shuffle(all_rows)

    return all_rows


def create_all_balanced(output_dir, input_f_invalid, output_f_name="train_and_dataset_invalid_balanced_shuf",
                        filter_func=lambda x: True):
    """
    For binary labels, will balance based on the filter func. E.g. it will makes sure that there is roughly the same
    number of 'incosistency' labels as there are 'other' labels.

    :param filter_func: any function to accept or refuse a row from the invalid part of the dataset from being included.
    """
    # write the rows
    for rows_per_fold, fieldnames, curr_output_dir in get_rows(output_dir, input_f_invalid, filter_func):
        rows_balanced = balance_rows(rows_per_fold, filter_func)
        if rows_balanced is not None:
            write_rows(curr_output_dir, output_f_name, fieldnames, rows_balanced)


def is_rob(row):
    return is_reason(row, "risk-of-bias")


def is_imp(row):
    return is_reason(row, "imprecision")


def is_ind(row):
    return is_reason(row, "indirectness")


def is_inc(row):
    return is_reason(row, "inconsistency")


def is_pub(row):
    return is_reason(row, "publication-bias")


def is_reason(row, reason_type):
    if isinstance(row, dict):
        reasons_dict = eval(row["Reasons"])
    else:
        assert isinstance(row, list)
        reasons_dict = eval(row[24])
        assert isinstance(reasons_dict, dict)
    for reason in reasons_dict.keys():
        if reason_type in reason.replace(" ", "-"):
            return True
    return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f_invalid',
                        help='input file: invalid data instances in csv format. This is the extra data with GRADE labels but inconsistently extracted downgrading reasons.')
    parser.add_argument('-input_f_train',
                        help='a training file in csv format, normally should be the file from a single fold.')
    args = parser.parse_args()
    output_dir = Path(args.input_f_invalid).parent

    # create_one(output_dir, args.input_f_invalid, args.input_f_train)

    # create_all(output_dir, args.input_f_invalid)

    # create_all(output_dir, args.input_f_invalid, output_f_name="train_and_rob_only_invalid_shuf", filter_func=is_rob)
    # create_all(output_dir, args.input_f_invalid, output_f_name="train_and_imp_only_invalid_shuf", filter_func=is_imp)
    # create_all(output_dir, args.input_f_invalid, output_f_name="train_and_ind_only_invalid_shuf", filter_func=is_ind)
    # create_all(output_dir, args.input_f_invalid, output_f_name="train_and_inc_only_invalid_shuf", filter_func=is_inc)
    # create_all(output_dir, args.input_f_invalid, output_f_name="train_and_pub_only_invalid_shuf", filter_func=is_pub)

    create_all_balanced(output_dir, args.input_f_invalid, output_f_name="train_and_rob_only_invalid_balanced_shuf",
                        filter_func=is_rob)
    create_all_balanced(output_dir, args.input_f_invalid, output_f_name="train_and_imp_only_invalid_balanced_shuf",
                        filter_func=is_imp)
    create_all_balanced(output_dir, args.input_f_invalid, output_f_name="train_and_ind_only_invalid_balanced_shuf",
                        filter_func=is_ind)
    create_all_balanced(output_dir, args.input_f_invalid, output_f_name="train_and_inc_only_invalid_balanced_shuf",
                        filter_func=is_inc)
    create_all_balanced(output_dir, args.input_f_invalid, output_f_name="train_and_pub_only_invalid_balanced_shuf",
                        filter_func=is_pub)
