import argparse
import csv
import itertools
import math
from collections import Counter, defaultdict
from pathlib import Path

import numpy as np
from scipy import stats

from sysrev.dataset_construction.util.util import get_records


# from sysrev.modelling.allennlp.my_project.dataset_reader import NUMERICAL_FEATS


def analyse_cor_matrices(f):
    # load a list of matrices (one per topic) that hold the correlations between pairs of features
    # the input file is obtained using /home/simon/Apps/SysRev/sysrev/dataset_construction/dataset/plots/feature_covariance.R
    from scipy.stats import describe
    cor_df = defaultdict(list)
    with open(f) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            feats = []
            for k, v in row.items():
                if k.startswith("CovMat"):
                    if v == "NA":
                        feats.append(np.NAN)
                    else:
                        feats.append(eval(v))
            cor_df[row["topic"]].append(feats)
    del (cor_df["Diagnosis"])  # all NaNs
    del (cor_df["Methodology"])  # all NaNs
    topic_names = list(cor_df.keys())
    cor_df = np.array(list(cor_df.values()))

    # write the correlation in the format needed for plotting in R
    feat_names = ["n_participants", "n_studies", "year", "n_sofs", "n_outcomes", "ci_lower", "ci_upper",
                  "relative_effect", "n_included_studies", "n_excluded_studies", "n_ongoing_studies", "n_other_studies"]
    assert cor_df.shape[1] == cor_df.shape[2]
    n_topics = cor_df.shape[0]
    n_feat_rows = cor_df.shape[1]
    n_feat_cols = cor_df.shape[2]
    with open("/home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/feat_cor_per_topic.csv", "w") as fh_out:
        writer = csv.writer(fh_out)
        # writer.writerow(["topic", "feat_pair", "cor_coef"])
        # writer.writerow(["topic"] + feat_names)
        for topic_n in range(n_topics):
            feat_combos = []
            ds = []
            for i in range(n_feat_rows):
                for j in range(n_feat_cols):
                    if j > i:
                        # writer.writerow([topic_names[topic_n], feat_names[i] + ':' + feat_names[j], cor_df[topic_n, i, j]])
                        feat_combos.append(feat_names[i] + ':' + feat_names[j])
                        ds.append(cor_df[topic_n, i, j])
            if topic_n == 0:
                writer.writerow(["topic"] + feat_combos)
            writer.writerow([topic_names[topic_n]] + ds)

    means = np.zeros((n_feat_rows, n_feat_cols))
    for i in range(n_feat_rows):
        for j in range(n_feat_cols):
            # describe each feat pair across topics
            means[i, j] = describe(cor_df[:, i, j], nan_policy="omit").mean
    # for each topic, get the total absolute deviation of feat correlations from the mean across all topics:
    topic_to_total_dev = Counter()
    for topic_n in range(n_topics):
        topic_to_total_dev[topic_names[topic_n]] = np.sum(np.triu(abs(cor_df[topic_n, :, :] - means)))
    print()


def get_feats_per_topic(f, f_out):
    def get_num_feats(row):
        num_feats = []
        for feat_name in NUMERICAL_FEATS:
            if not row[feat_name]:
                num_feats.append("NA")
            else:
                num_feats.append(eval(row[feat_name]))

        return num_feats

    all_feats = defaultdict(list)

    with open(f) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            topics = eval(row["topics"])
            for topic in topics:
                num_feats = get_num_feats(row)
                all_feats[topic].append(num_feats)

    with open(f_out, "w") as fh_out:
        writer = csv.DictWriter(fh_out, fieldnames=["topic"] + NUMERICAL_FEATS)

        writer.writeheader()
        for topic, feats_list in all_feats.items():
            for feats in feats_list:
                row = {**{'topic': topic}, **dict(zip(NUMERICAL_FEATS, feats))}
                writer.writerow(row)


def get_grade_and_reasons(f):
    c_grade = Counter()
    c_reasons = Counter()
    with open(f) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            c_grade[row["Grade"]] += 1
            for reason in eval(row["Reasons"]).keys():
                c_reasons[reason.replace(" ", "-")] += 1

    return c_grade, c_reasons


def get_reasons_from_predictions(f):
    c_reasons = Counter()
    with open(f) as fh:
        for reasons in predictions_reader(fh):
            c_reasons.update(reasons)

    return c_reasons


def dataset_reader(fh):
    reader = csv.DictReader(fh)
    for row in reader:
        reasons = [reason.replace(" ", "-") for reason in eval(row["Reasons"]).keys()]
        yield reasons


def predictions_reader(fh):
    for line in fh:
        reasons = [reason.replace(" ", "-") for reason in eval(line)]
        yield reasons


def get_reason_correlation_matrix(f, independent_counts, out_f, read_fun=dataset_reader):
    """

    :param f:
    :param independent_counts:
    :param out_f:
    :param read_fun: dataset_reader -> read from dataset; predictions_reader -> read model predictions file
    :return:
    """
    reason_pairs = Counter()
    n_reasons_dist = []
    with open(f) as fh:
        for reasons in read_fun(fh):
            if reasons:
                n_reasons_dist.append(len(reasons))
            for pair in itertools.combinations(reasons, r=2):
                reason_pairs[pair] += 1

    print(stats.describe(n_reasons_dist))

    joint_probs = {}
    for (r1, r2), count in reason_pairs.items():
        p = count / (independent_counts[r1] + independent_counts[r2])
        joint_probs[(r1, r2)] = p
    print("Joint probabilities:")
    print(joint_probs)

    independent_probs = {}
    total_count = sum(independent_counts.values())
    for reason, count in independent_counts.items():
        independent_probs[reason] = count / total_count
    print("Independent probabilities:")
    print(independent_probs)

    pmi = {}
    for (r1, r2), joint_prob in joint_probs.items():
        pmi[(r1, r2)] = math.log2(joint_prob / (independent_probs[r1] * independent_probs[r2]))
    print("PMI:")
    print(pmi)

    dir_name = Path(out_f).parent
    if not Path(dir_name).exists():
        Path(dir_name).mkdir(parents=True)
    with open(out_f, "w") as fh_out:
        fh_out.write("r1 r2 prob\n")
        for (r1, r2), joint_prob in joint_probs.items():
            fh_out.write(f"{r1} {r2} {joint_prob}\n")
    print(f"Written to: {out_f}")


def get_field(f):
    comments = []
    with open(f) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            comments.append(row["n_included_studies"] == row["n_studies"])
        print()


def get_unique_values(f):
    """
    get all unique values for each of these fields in the dataset:
    review_type, topics, abbrs, ci_lower, ci_upper, relative_effect, type_effect

    :param f:
    :return: dict of field name to set of unique values
    """
    unique = {}
    with open(f) as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            for field in ["review_type", "topics", "abbrs", "ci_lower", "ci_upper", "relative_effect", "type_effect"]:
                if field not in unique:
                    unique[field] = set()
                if field == "topics":
                    unique[field].update(eval(row[field]))
                else:
                    unique[field].add(row[field])
        print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f',
                        help='input file: dataset (in csv format) or model predictions file')
    args = parser.parse_args()

    get_unique_values(args.input_f)
    # get_records(args.input_f, "title", "Cognitive training for people with mild to moderate dementia")
    # analyse_cor_matrices('/home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/cor_matrices.csv')
    # get_feats_per_topic(args.input_f, "/home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/feats_per_topic.csv")
    # c_grade, c_reasons = get_grade_and_reasons(args.input_f)
    # print(c_grade)
    # print(c_reasons)
    # print(c_grade)
    # tot = sum(c_grade.values())
    # print(tot)
    # for r, v in c_reasons.items():
    #    print(f"non-{r}: {tot - v}")
    # get_reason_correlation_matrix(args.input_f, c_reasons, out_f="/scratch/ssuster/Apps/SysRev/data/dataset/plots/label_correlation/label_joint_probs_dev")
    # get_field(args.input_f)

    # c = 0
    # with open(args.input_f) as fh:
    #    for l in dataset_reader(fh):
    #        c+=1
    # print(c)

    # c_reasons = get_reasons_from_predictions(args.input_f)
    # get_reason_correlation_matrix(args.input_f, c_reasons, out_f="/scratch/ssuster/Apps/SysRev/data/dataset/plots/label_correlation/label_joint_probs_hierarchical_numcat", read_fun=predictions_reader)
