import argparse

import numpy as np

from sysrev.dataset_construction.dataset.build_csv import write
from sysrev.dataset_construction.util.util import read_csv


def create_data(dataset_path, output_dir, test_topic):
    train, dev, test = [], [], []

    for row in read_csv(dataset_path):
        topics = eval(row["topics"])
        if test_topic in topics:
            if np.random.random_sample() > 0.2:
                test.append(row)
            else:
                dev.append(row)
        else:
            train.append(row)

    header_row = row
    write([header_row] + train, output_dir, "train.csv")
    write([header_row] + dev, output_dir, "dev.csv")
    write([header_row] + test, output_dir, "test.csv")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f',
                        help='dataset in csv')
    parser.add_argument('-output_dir',
                        help='dataset dir in which we create train.csv, dev.csv and test.csv')
    args = parser.parse_args()

    test_topic = "Mental health"
    create_data(args.input_f, args.output_dir, test_topic)
