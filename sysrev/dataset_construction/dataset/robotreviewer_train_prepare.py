import argparse
import re
from collections import Counter, defaultdict
from pathlib import Path

from sysrev.dataset_construction.dataset.explore import get_cochrane_id
from sysrev.dataset_construction.util.util import load_json, read_csv, save_json


# from krippendorff import krippendorff


# from sysrev.modelling.allennlp.my_project.dataset_reader import get_cochrane_id


def get_protected_attribute(inst, pico_anno, attribute):
    review_id = get_cochrane_id(inst["doi"])
    review_data = pico_anno.get(review_id, None)
    if review_data is None:
        return None

    return review_data[attribute]


def rob_studies_ids(fh, pmids_to_rob_judgs=None, pmids_to_topics=None, pmids_to_age=None, pmids_to_sex=None,
                    pico_anno=None, doi2pmid=None):
    """
    Obtain a mapping between pmid and RoB annotations. Only include those pmids that occur in 'ids_include', i.e. those used
    in the development of RobotReviewer.
    """
    rob_criteria = ['allo_conceal', 'blinding', 'incomplete_out_data', 'other_bias', 'outcome_blinding',
                    'part_blinding',
                    'rand_seq_gen', 'selective_reporting']

    if pmids_to_rob_judgs is None:
        pmids_to_rob_judgs = defaultdict(list)

    if pmids_to_topics is None:
        pmids_to_topics = {}

    if pmids_to_age is None:
        pmids_to_age = {}

    if pmids_to_sex is None:
        pmids_to_sex = {}

    # fh should be a dict of {fold_n: data}, but for invalid json data, we only have list with no fold info.
    if isinstance(fh, list):
        fh = {"x": fh}

    for fold_n, insts in fh.items():
        for inst in insts:
            for studies in inst["studies"].values():
                for study in studies:
                    for ref in study["references"]:
                        if ref["pmid"] is not None:
                            pmid = ref["pmid"]
                        elif ref["doi"] is not None:
                            pmid = doi2pmid.get(ref["doi"], None)
                        else:
                            pmid = None
                        if pmid is None or not pmid:
                            continue
                        pmids_to_rob_judgs[pmid].append(dict())
                        pmids_to_topics[pmid] = inst["topics"]
                        if pico_anno is not None:
                            if (pmid not in pmids_to_age) or (pmids_to_age[pmid] is None):
                                pmids_to_age[pmid] = get_protected_attribute(inst, pico_anno, "Age")
                            if (pmid not in pmids_to_sex) or (pmids_to_sex[pmid] is None):
                                pmids_to_sex[pmid] = get_protected_attribute(inst, pico_anno, "Sex")
                        for rob_criterion in rob_criteria:
                            judg = study["study_chars"]["rob"][rob_criterion]["judgement"]
                            if judg is not None:
                                pmids_to_rob_judgs[pmid][-1][rob_criterion] = judg
                        if not pmids_to_rob_judgs[pmid][-1]:
                            del pmids_to_rob_judgs[pmid][-1]
                        if not pmids_to_rob_judgs[pmid]:
                            del pmids_to_rob_judgs[pmid]

    c = Counter()
    for pmid, judgs in pmids_to_rob_judgs.items():
        c[len(judgs)] += 1
    print(f"Counter for the number of annotations (>1) per study: {c}")

    return pmids_to_rob_judgs, pmids_to_topics, pmids_to_age, pmids_to_sex


def write_pdf_links(pmids, pmid2ftp, out_f):
    ftp_locations = []
    for pmid in pmids:
        if pmid in pmid2ftp:
            ftp_locations.append((pmid, f"https://ftp.ncbi.nlm.nih.gov/pub/pmc/{pmid2ftp[pmid]}"))

    with open(out_f, "w") as fh_out:
        for pmid, link in ftp_locations:
            fh_out.write(f"wget -O {pmid}.pdf {link}\n")


def save_ids(pmids, ids_output_f):
    with open(ids_output_f, "w") as fh_out:
        for pmid in pmids:
            fh_out.write(f"{pmid}\n")


def get_year_distribution(oa_non_comm_use_pdf_csv, pmids):
    years = []
    pmid2year = {}
    for row in read_csv(oa_non_comm_use_pdf_csv):
        match = re.search(" ([12]\d\d\d) ", row["Article Citation"])
        if match is not None:
            pmid2year[row["PMID"]] = match.group().strip()
    for pmid in pmids:
        if pmid in pmid2year:
            years.append(pmid2year[pmid])
    print()


def get_review_id(url):
    review_id = None
    for part in Path(url).parts:
        if part.startswith("CD"):
            review_id = part
            break

    return review_id


def load_pico_anno(f):
    pico_anno = {}

    for row in read_csv(f, keys=["review_url", "concept_url", "value", "ontology_url", "pico_type"]):
        review_id = get_review_id(row["review_url"])
        if review_id not in pico_anno:
            pico_anno[review_id] = {"Age": [], "Sex": []}
        if row["ontology_url"] == "http://data.cochrane.org/ontologies/core/Age":
            pico_anno[review_id]["Age"].append(row["value"])
        if row["ontology_url"] == "http://data.cochrane.org/ontologies/core/Sex":
            pico_anno[review_id]["Sex"].append(row["value"])

    # sex to singleton
    # set age to none when no annotation
    for k, v in pico_anno.items():
        if not v["Age"]:
            v["Age"] = None
        sexes = set(v["Sex"])
        if len(sexes - {"Male", "Female", "Male and Female"}) > 0:
            # there's only one annotation for Transgender Female, skipping
            continue
        if len(sexes) > 1:
            v["Sex"] = "Male and Female"
        elif len(sexes) == 0:
            v["Sex"] = None
        else:
            v["Sex"] = v["Sex"][0]

    return pico_anno


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f',
                        help='input file: dataset in json format. Is the newer Cochrane data.')
    parser.add_argument('-input_f2',
                        help='input file: dataset in json format (invalid). Is the newer Cochrane data.')
    parser.add_argument('-input_f3',
                        help='input file2: dataset in json format. Is the Cochrane data used for EvidenceGRADEr.')
    parser.add_argument('-input_f4',
                        help='input file2: dataset in json format (invalid). Is the Cochrane data used for EvidenceGRADEr.')
    parser.add_argument('-train_output_f', help='')
    parser.add_argument('-ids_output_f',
                        help="file to hold all pmids found in Cochrane reviews and in RobotReviewer development. It's just a version of train_output_f with keys only.")
    parser.add_argument('-topics_output_f', help='')
    parser.add_argument('-age_output_f', help='')
    parser.add_argument('-sex_output_f', help='')
    parser.add_argument('-pico_anno_f', help='Cochrane csv file containing concept annotations (incl. Age, Sex)')
    parser.add_argument('-pdf_links_output_f', help='')
    args = parser.parse_args()

    pmids_to_rob_judgs = None
    pmids_to_topics = None
    pmids_to_age = None
    pmids_to_sex = None

    pico_anno = load_pico_anno(args.pico_anno_f)
    doi2pmid = {row["DOI"]: row["PMID"] for row in read_csv("/home/simon/Downloads/PMC-ids.csv")}

    for input_f in [args.input_f, args.input_f2, args.input_f3, args.input_f4]:
        fh = load_json(input_f)
        pmids_to_rob_judgs, pmids_to_topics, pmids_to_age, pmids_to_sex = rob_studies_ids(fh,
                                                                                          pmids_to_rob_judgs=pmids_to_rob_judgs,
                                                                                          pmids_to_topics=pmids_to_topics,
                                                                                          pmids_to_age=pmids_to_age,
                                                                                          pmids_to_sex=pmids_to_sex,
                                                                                          pico_anno=pico_anno,
                                                                                          doi2pmid=doi2pmid)

    save_json(pmids_to_rob_judgs, args.train_output_f)
    save_json(pmids_to_topics, args.topics_output_f)
    save_json(pmids_to_age, args.age_output_f)
    save_json(pmids_to_sex, args.sex_output_f)

    # save pmids.txt which we use to scrape those pdfs using pubMunch that are open access
    save_ids(pmids_to_rob_judgs.keys(), args.ids_output_f)

    # get the wget links to download data from PMC Open Access Subset
    pmids = pmids_to_rob_judgs.keys()
    oa_non_comm_use_pdf_csv = "/home/simon/Apps/SysRevData/data/dataset/oa_non_comm_use_pdf.csv"
    pmid2ftp = {row["PMID"]: row["File"] for row in read_csv(oa_non_comm_use_pdf_csv)}
    pdf_links = write_pdf_links(pmids, pmid2ftp, args.pdf_links_output_f)

    # get_year_distribution(oa_non_comm_use_pdf_csv, pmids)
