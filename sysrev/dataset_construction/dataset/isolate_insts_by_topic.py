import argparse
import os

from sysrev.dataset_construction.dataset.build_csv import write
from sysrev.dataset_construction.util.util import read_csv


def create_data(generic_path, topic_constraint, n_folds=10):
    for i in range(n_folds):
        new_data = []
        path = generic_path.replace("FOLD", str(i))

        for c, row in enumerate(read_csv(path)):
            topics = eval(row["topics"])
            if topic_constraint in topics:
                new_data.append(row)

        print(f"orig size: {c}, new size: {len(new_data)}")
        header_row = row
        write([header_row] + new_data, os.path.dirname(path) + "/", "test_mental.csv")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-generic_input_dir',
                        help='path to datasets in csv, with fold number as a placeholder',
                        default="/home/simon/Apps/SysRev/data/derivations/all/splits/FOLD/test.csv")
    args = parser.parse_args()

    topic_constraint = "Mental health"
    create_data(args.generic_input_dir, topic_constraint)
