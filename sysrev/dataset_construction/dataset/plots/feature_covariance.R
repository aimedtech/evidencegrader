file1 = "/home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/feats_per_topic.csv"
f1 = read.table(file1, skip = 0, header = TRUE, sep = ",", na.strings = "NA", dec = ".", strip.white = TRUE)
d1 = data.frame(f1)
# remove this column as it's always 0
d1$n_additional_studies <- NULL

cor_matrices <- d1 %>%
  nest_by(topic) %>%
  summarise(CovMat = cor(data, method = "pearson", use = "pairwise.complete.obs")) %>%
  ungroup #%>%
#split(f = .$topic) %>%
#map(~ .x %>% select(-topic))
write.csv(cor_matrices, "/home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/cor_matrices.csv", col.names = c(c(""), colnames(d1)))
colnames(d1)

# Create /home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/feat_cor_per_topic.csv:
# Use analyse_cor_matrices() in /home/simon/Apps/SysRev/sysrev/dataset_construction/dataset/statistics.py, which
# takes cor_matrices.csv generated in the previous step as input

# Plot correlations between pairs of numerical features across topics and save to png
library(tidyverse)
library(ggrepel)

is_outlier <- function(x) {
  return(x < quantile(x, 0.25) - 1.5 * IQR(x) | x > quantile(x, 0.75) + 1.5 * IQR(x))
}

d2 %>%
  pivot_longer(names_to = "feat_pair", values_to = "cor_coef", -topic) %>%
  group_by(feat_pair) %>%
  mutate(outlier = if_else(is_outlier(cor_coef), topic, NA_character_)) %>%
  ggplot(aes(x = feat_pair, y = cor_coef)) +
  geom_boxplot() +
  geom_text_repel(aes(label = outlier, colour = topic), na.rm = TRUE, show.legend = F) +
  theme(plot.title = element_text(size = 18),
        axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1),
        legend.position = "none"
  )

outfile = "/home/simon/Apps/SysRev/data/dataset/plots/feature_covariance/feat_cor_per_topic.png"
ggsave(outfile)