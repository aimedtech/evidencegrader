# Build dataset

[build.py](build.py) builds the dataset in json format by combining individual review files from the parsing step; the
dataset is divided into 10 folds, and the train/dev/test splits are created.

- `-metadata_f`: path to metadata csv file created during scraping
- `-input_dir`: path to directory containing parsed files
- `-output_dir`: path to directory that will hold the created dataset

In addition to `dataset.json` file, the code will create a `dataset_invalid.json` file that contains the instances for
which the number of downgrading reasons/steps does not match the provided quality grade.

After running [build.py](build.py), re-write the data as csv files using [build_csv.py](build_csv.py). This makes it
easier to read with some data readers when training a model. The script takes the following arguments:

- `-input_dir`: the output directory used in build.py
- `-output_dir`: output directory to use for the generated csv files