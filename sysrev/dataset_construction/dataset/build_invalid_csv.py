import argparse
from pathlib import Path

from sysrev.dataset_construction.dataset.build_csv import iter_all, write
from sysrev.dataset_construction.util.util import load_json

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-input_f',
                       help='input file: invalid data instances in json format')
    parser.add_argument('-output_dir',
                        help='output dir to which csv data will be written')
    args = parser.parse_args()
    if not Path(args.output_dir).exists():
        Path(args.output_dir).mkdir()

    dataset = load_json(args.input_f)

    write(iter_all(None, dataset), args.output_dir, "dataset_invalid.csv")
