import csv
import json
import os

from sysrev.dataset_construction.dataset.build_csv import write


def get_file_list(topdir, identifiers=None, all_levels=False, ignore_hidden=False):
    """
    :param identifiers: a list of strings, any of which should be in the filename
    :param all_levels: get filenames recursively
    """
    if identifiers is None:
        identifiers = [""]
    filelist = []
    for root, dirs, files in os.walk(topdir):
        if not all_levels and (root != topdir):  # don't go deeper
            continue
        for filename in files:
            get = False
            for i in identifiers:
                if i in filename:
                    get = True
                if ignore_hidden and filename[0] == ".":
                    get = False
            if get:
                fullname = os.path.join(root, filename)
                filelist.append(fullname)

    return filelist


def get_dir_list(topdir, identifiers=None, all_levels=False, ignore_hidden=False):
    """
    :param identifiers: a list of strings, any of which should be in the filename
    :param all_levels: get filenames recursively
    """
    if identifiers is None:
        identifiers = [""]
    dirlist = []
    for root, dirs, files in os.walk(topdir):
        if not all_levels and (root != topdir):  # don't go deeper
            continue
        for dir in dirs:
            get = False
            for i in identifiers:
                if i in dir:
                    get = True
                if ignore_hidden and dir[0] == ".":
                    get = False
            if get:
                fullname = os.path.join(root, dir)
                dirlist.append(fullname)

    return dirlist


def save_json(obj, filename):
    with open(filename, "w") as out:
        json.dump(obj, out, separators=(',', ':'), indent=2, sort_keys=True)


def load_json(filename):
    with open(filename) as in_f:
        return json.load(in_f)


def read_csv(f, keys=None, delimiter=","):
    with open(f) as csvfile:
        if keys is not None:
            reader = csv.DictReader(csvfile, fieldnames=keys, delimiter=delimiter)
        else:
            reader = csv.DictReader(csvfile, delimiter=delimiter)
        for row in reader:
            yield row


def get_records(f, field, value):
    records = []
    for row in read_csv(f):
        if row[field] == value:
            records.append(row)

    write(records, "/home/simon/Apps/SysRev/data/derivations/all/", "bahar-fuchs-etal-2019.csv")
