The process of constructing the dataset for quality assessment of medical evidence is as follows:

1. [Scrape data from the Cochrane website](scrape/README.md)
2. [Parse the scraped data](parse/README.md)
3. [Build the dataset and data splits from the parsed data](dataset/README.md)
4. (optional) [Re-write the dataset as csv files](dataset/README.md)  