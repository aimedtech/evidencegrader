# Scraping systematic reviews from the Cochrane database.

This code requires the use of `selenium` package to simulate the browser. 

Download the chrome driver from https://www.selenium.dev/documentation/en/webdriver/driver_requirements/, unpack the zip into a directory `$DRIVER_DIR`.

Move the directory to `/usr/bin/` or:
```
export PATH=$PATH:$DRIVER_DIR >> ~/.bashrc
source ~/.bashrc
```

Scrape article bib entries for each topic into `OUTPUT_DIR`:
```
python3 scrape_topics.py $OUTPUT_DIR
```

This will produce one bib file per topic. Each bib file contains multiple bib entries.

Create metadata that includes information from bib entries:

```
python3 metadata.py -input_dir $OUTPUT_DIR
```

This will create a `metadata.csv` file in $OUTPUT_DIR. 

Now, scrape the articles (entire htmls):

```
python3 scrape_articles.py -metadata_f $OUTPUT_DIR/metadata.csv -output_dir $OUTPUT_DIR_ARTICLES
```

If the process exits due to encountering a `WebDriverException` (`unknown error`), simply re-start the process. It will continue from where it failed. Similarly, in a number of cases the error `An error has occurred because we were unable to send a cookie` might occur for the reference htmls. Just running the code again can fix this problem (it will leave the correctly scraped htmls intact).