import argparse
import os
from pathlib import Path

from selenium import webdriver
from tqdm import tqdm

from sysrev.dataset_construction.util.util import read_csv


def scrape_html(url):
    """
    Scrape article (systematic review) and refs from the Cochrane website.

    :param url: article url
    :return: article html, resolved article url
    """
    driver = webdriver.Chrome()
    driver.get(url)
    driver.implicitly_wait(100)

    html = driver.page_source
    current_url = driver.current_url
    driver.quit()

    return html, current_url


def scrape_refs_html(url):
    """
    Scrape article refs from the Cochrane website.

    :param url: article url
    :return: article html for refs
    """
    refs_url = url.replace("/full", "/references")
    driver = webdriver.Chrome()
    driver.get(refs_url)
    driver.implicitly_wait(100)

    refs_html = driver.page_source
    driver.quit()

    return refs_html


def had_errors(f_out_refs):
    with open(f_out_refs) as fh:
        txt = fh.read()
        result = txt.find("An error has occurred because we were unable to send a cookie")
    return False if result == -1 else True


def scrape_articles(metadata_f, output_dir, refs_output_dir):
    if not Path(output_dir).exists():
        os.makedirs(output_dir)

    if not Path(refs_output_dir).exists():
        os.makedirs(refs_output_dir)

    reader = read_csv(metadata_f)
    for row in tqdm(reader):
        f_out = f"{output_dir}/{row['docid']}.html"
        f_out_refs = f"{refs_output_dir}/{row['docid']}.html"
        if Path(f_out).exists():  # if html is done
            # redo if refs_html had errors
            if not had_errors(f_out_refs):
                continue

        url = row["url"]
        html, resolved_url = scrape_html(url)
        refs_html = scrape_refs_html(resolved_url)

        with open(f_out, "w") as fh_out, open(f_out_refs, "w") as fh_out_refs:
            fh_out.write(html)
            fh_out_refs.write(refs_html)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-metadata_f',
                        help='path to metadata.csv')
    parser.add_argument('-output_dir',
                        help='output dir for html files')
    parser.add_argument('-refs_output_dir',
                        help='output dir for reference html files')
    args = parser.parse_args()

    scrape_articles(args.metadata_f, args.output_dir, args.refs_output_dir)
