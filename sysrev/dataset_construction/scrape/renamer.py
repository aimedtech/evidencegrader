import os.path

from bs4 import BeautifulSoup

from sysrev.dataset_construction.util.util import read_csv, get_file_list


def get_metadata_doi2hex(f):
    d = {row["docid"]: row["doi"] for row in read_csv(f)}
    inv_d = {v: k for k, v in d.items()}

    return inv_d


doi2hex = get_metadata_doi2hex("/home/simon/Apps/SysRev/data/scraped/bib_entries/metadata.csv")
html_fs = get_file_list("/home/simon/Apps/SysRev/data/scraped/html", identifiers=["html"])
for f in html_fs:
    #
    with open(f) as fh:
        soup = BeautifulSoup(fh, "lxml")
        url = soup.find("meta", attrs={"name": "citation_doi"})
        if url is not None:
            doi = url["content"]
        else:
            doi = None
    if doi is not None:
        old_hex = os.path.splitext(os.path.basename(f))[0]
        if doi not in doi2hex:
            for k, v in doi2hex.items():
                if doi in k:
                    new_hex = v
                break
        else:
            new_hex = doi2hex[doi]
        new_f = f"{os.path.dirname(f)}/{new_hex}.html"
        os.rename(f, new_f)
        os.rename(f"/home/simon/Apps/SysRev/data/scraped/refs_html/{old_hex}.html",
                  f"/home/simon/Apps/SysRev/data/scraped/refs_html/{new_hex}.html")
