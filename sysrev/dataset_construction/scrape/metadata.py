import argparse
import csv
import os
import uuid
from collections import defaultdict
from pathlib import Path

from sysrev.dataset_construction.util.util import get_file_list, read_csv


class Metadatum:
    def __init__(self, bib_entry):
        self.docid = None
        self.title = None
        self.authors = None
        self.journal = None
        self.journal_number = None
        self.year = None
        self.publisher = None
        self.issn = None
        self.doi = None
        self.url = None
        self.keywords = []
        self.topic = []

        self.populate(bib_entry)

    def populate(self, bib_entry):
        self.docid = uuid.uuid4().hex
        self.title = self.cleanup(bib_entry["title"])
        self.authors = self.cleanup(bib_entry["author"])
        self.journal = self.cleanup(bib_entry["journal"])
        self.journal_number = self.cleanup(bib_entry["number"])
        self.year = self.cleanup(bib_entry["year"])
        self.publisher = self.cleanup(bib_entry["publisher"])
        self.issn = self.cleanup(bib_entry["ISSN"])
        self.doi = self.cleanup(bib_entry["DOI"])
        self.url = self.cleanup(bib_entry["URL"])
        if "keywords" in bib_entry:
            self.keywords = self.cleanup(bib_entry["keywords"]).split(";")
        self.topic = [bib_entry["topic"]]

    def add_topic(self, topic):
        self.topic.append(topic)

    def cleanup(self, bib_entry):
        bib_entry = bib_entry[bib_entry.find("{") + 1:].strip()
        if bib_entry.endswith(","):
            bib_entry = bib_entry[:-1]
        if bib_entry.endswith("}"):
            bib_entry = bib_entry[:-1]

        return bib_entry.strip()


def get_docid_to_topics(metadata_f):
    docid_to_topics = {}
    reader = read_csv(metadata_f)
    for row in reader:
        docid_to_topics[row["docid"]] = eval(row["topic"])

    return docid_to_topics


def get_docid_to_year(metadata_f):
    docid_to_year = {}
    reader = read_csv(metadata_f)
    for row in reader:
        docid_to_year[row["docid"]] = eval(row["year"])

    return docid_to_year


def get_docid_to_urls(metadata_f):
    docid_to_urls = defaultdict(list)
    reader = read_csv(metadata_f)
    for row in reader:
        docid_to_urls[row["docid"]].append(row["url"])

    return docid_to_urls


def get_title_to_docids(metadata_f):
    title_to_docids = defaultdict(list)
    reader = read_csv(metadata_f)
    for row in reader:
        title = row["title"].replace("‐", " ").replace("-", " ").replace("?", " ")
        title_to_docids[title].append(row["docid"])

    return title_to_docids


class Metadata(list):
    def add(self, metadatum):
        # check if metadatum with this doi already exists
        existing = self.get(metadatum.doi)
        if existing is None:
            self.append(metadatum)
        else:
            existing.add_topic(metadatum.topic.pop())

    def get(self, doi):
        for metadatum in self:
            if metadatum.doi == doi:
                return metadatum
        return None

    def write(self, output_dir):
        with open(output_dir + "metadata.csv", mode='w') as fh_out:
            writer = csv.writer(fh_out)
            writer.writerow(("docid", "title", "authors", "journal", "journal_number", "year", "publisher", "issn",
                             "doi", "url", "keywords", "topic"))
            for d in self:
                writer.writerow((d.docid, d.title, d.authors, d.journal, d.journal_number, d.year,
                                 d.publisher, d.issn, d.doi, d.url, d.keywords, d.topic))


def get_bib_entries(input_dir):
    fs = get_file_list(input_dir, identifiers=[".bib"])
    bib_entries = []
    for f in fs:
        topic = Path(f).stem

        with open(f) as fh:
            # read lines until end of record
            bib_entry = {}
            for l in fh:
                curr_l = l.strip()
                if not curr_l:
                    if bib_entry:
                        bib_entry["topic"] = topic
                        bib_entries.append(bib_entry)
                    bib_entry = {}
                    continue
                l_list = l.strip().split(" ", 1)
                k = l_list[0]
                if len(l_list) == 1:
                    v = None
                else:
                    v = l_list[1]
                bib_entry[k] = v

    return bib_entries


def prepare_metadata(bib_entries):
    metadata = Metadata()
    for bib_entry in bib_entries:
        metadatum = Metadatum(bib_entry)
        metadata.add(metadatum)

    return metadata


def two_metadata_fs_diff(m_f1, m_f2):
    """
    :param m_f1: original metadata file
    :param m_f2: a newer metadata file
    Writes a third metadata file that includes the entries in m_f2 that are not in m_f1
    """
    m1_dois = {row["doi"] for row in read_csv(m_f1)}
    m3_entries = []
    for row in read_csv(m_f2):
        if row["doi"] not in m1_dois:
            m3_entries.append(row)

    fieldnames = row.keys()
    m_f3_out = os.path.dirname(m_f2) + "/metadata_diff.csv"
    with open(m_f3_out, "w") as fh_out:
        writer = csv.DictWriter(fh_out, fieldnames=fieldnames)
        writer.writeheader()
        for row in m3_entries:
            writer.writerow(row)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_dir',
                        help='input dir for downloaded bib files.')
    # for two_metadata_fs_diff
    # parser.add_argument('-m_f1')
    # parser.add_argument('-m_f2')
    args = parser.parse_args()

    # two_metadata_fs_diff(args.m_f1, args.m_f2)
    bib_entries = get_bib_entries(args.input_dir)
    metadata = prepare_metadata(bib_entries)
    metadata.write(args.input_dir)
