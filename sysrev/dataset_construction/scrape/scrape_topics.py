import argparse
import os
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


def every_downloads_chrome(driver):
    if not driver.current_url.startswith("chrome://downloads"):
        driver.get("chrome://downloads/")
    return driver.execute_script("""
        return document.querySelector('downloads-manager')
        .shadowRoot.querySelector('#downloadsList')
        .items.filter(e => e.state === 'COMPLETE')
        .map(e => e.filePath || e.file_path || e.fileUrl || e.file_url);
        """)


def get_browser_options(output_dir):
    options = webdriver.ChromeOptions()
    options.add_experimental_option("prefs", {
        "download.default_directory": output_dir,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing.enabled": True
    })
    return options


def scrape_bib_entries(output_dir):
    options = get_browser_options(output_dir)
    # get topic names
    options.add_argument("--remote-debugging-port=9222")

    driver = webdriver.Chrome(options=options)
    # exploration starts with Cochrane topics
    topics_url = "https://www.cochranelibrary.com/cdsr/reviews/topics"
    driver.get(topics_url)
    driver.implicitly_wait(100)
    # get topic buttons so that we can navigate to each of them later
    buttons = driver.find_elements_by_class_name("btn-link")
    todo_topics = [button.text for button in buttons]
    todo_topics.reverse()
    driver.quit()

    while todo_topics:
        # scrape bib entries in each topic
        topic = todo_topics.pop()
        print(f"Topic: {topic}")

        driver = webdriver.Chrome(options=options)
        driver.implicitly_wait(100)
        # exploration starts with Cochrane topics
        topics_url = "https://www.cochranelibrary.com/cdsr/reviews/topics"
        driver.get(topics_url)
        # get topic button
        xpath = f"//button[text()='{topic}']"
        button = driver.find_element_by_xpath(xpath)
        # open topic
        button.click()
        time.sleep(15)
        # find select all checkbox
        select_all_checkbox = driver.find_element_by_id(
            "_scolarissearchresultsportlet_WAR_scolarissearchresults_selectAll")
        # select all articles
        select_all_checkbox.click()
        # click export selected citations
        export_button = driver.find_element_by_class_name("export-citation-for-selected-btn")
        export_button.click()

        # find bibtex button
        bibtext_button = driver.find_elements_by_class_name("btn-link")[-2]
        bibtext_button.click()
        assert bibtext_button.text == "BibteX"

        download_button = driver.find_elements_by_css_selector(".btn.primary.wide.pull-right")[-1]
        assert download_button.text == "Download"
        download_button.click()
        time.sleep(60)
        WebDriverWait(driver, 120, 1).until(every_downloads_chrome)
        driver.quit()

        # rename the bib file
        source_file = output_dir + "citation-export.bib"
        target_file = source_file.replace("citation-export", topic)
        os.rename(source_file, target_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-output_dir',
                        help='output dir for downloaded bib files.')
    args = parser.parse_args()

    scrape_bib_entries(args.output_dir)
