# DOI2PMID = {row["DOI"]: row["PMID"] for row in read_csv("/home/simon/Downloads/PMC-ids.csv")}
# DOI2PMCID = {row["DOI"]: row["PMCID"][3:] for row in read_csv("/home/simon/Downloads/PMC-ids.csv")}


class Char(object):
    def __init__(self):
        self.methods = None
        self.participants = None
        self.interventions = None
        self.outcomes = None
        self.notes = None
        self.other = None

    def parse(self, el):
        self.get_all(el)

    def get_all(self, el):
        other = []

        for row in el.table.find_all("tr"):
            cols = row.find_all("td")
            if len(cols) != 2:
                continue
            if cols[0].get_text(strip=True, separator=" ") == "Methods":
                self.methods = cols[1].get_text(strip=True, separator="\n")
            elif cols[0].get_text(strip=True, separator=" ") == "Participants":
                self.participants = cols[1].get_text(strip=True, separator="\n")
            elif cols[0].get_text(strip=True, separator=" ") == "Interventions":
                self.interventions = cols[1].get_text(strip=True, separator="\n")
            elif cols[0].get_text(strip=True, separator=" ") == "Outcomes":
                self.outcomes = cols[1].get_text(strip=True, separator="\n")
            elif cols[0].get_text(strip=True, separator=" ") == "Notes":
                self.notes = cols[1].get_text(strip=True, separator="\n")
            else:
                other.append(cols[0].get_text(strip=True, separator=" ") + "\n" + \
                             cols[1].get_text(strip=True, separator="\n"))

        self.other = "\n".join(other)

    def serialise(self):
        return self.__dict__


class RoB(object):
    def __init__(self):
        self.rand_seq_gen = {"judgement": None, "support": None}
        self.allo_conceal = {"judgement": None, "support": None}
        self.part_blinding = {"judgement": None, "support": None}
        self.outcome_blinding = {"judgement": None, "support": None}
        self.blinding = {"judgement": None, "support": None}
        self.incomplete_out_data = {"judgement": None, "support": None}
        self.selective_reporting = {"judgement": None, "support": None}
        self.other_bias = {"judgement": None, "support": None}
        self.other = None

    def parse(self, el):
        self.get_all(el)

    def serialise(self):
        return self.__dict__

    def get_all(self, el):
        def update(attr):
            attr["judgement"] = cols[1].get_text(strip=True, separator="\n")
            attr["support"] = cols[2].get_text(strip=True, separator="\n")

        other = []

        for row in el.table.find_all("tr"):
            cols = row.find_all("td")
            if len(cols) != 3:
                continue
            if "Random sequence generation" in cols[0].get_text(strip=True, separator=" "):
                update(self.rand_seq_gen)
            elif "Allocation concealment" in cols[0].get_text(strip=True, separator=" "):
                update(self.allo_conceal)
            elif "Blinding of participants and personnel" in cols[0].get_text(strip=True, separator=" "):
                update(self.part_blinding)
            elif "Blinding of outcome assessment" in cols[0].get_text(strip=True, separator=" "):
                update(self.outcome_blinding)
            elif "Blinding" in cols[0].get_text(strip=True, separator=" "):
                update(self.blinding)
            elif "Incomplete outcome data" in cols[0].get_text(strip=True, separator=" "):
                update(self.incomplete_out_data)
            elif "Selective reporting" in cols[0].get_text(strip=True, separator=" "):
                update(self.selective_reporting)
            elif "Other bias" in cols[0].get_text(strip=True, separator=" "):
                update(self.other_bias)
            else:
                txt_0 = cols[0].get_text(strip=True, separator=" ")
                txt_1 = cols[1].get_text(strip=True, separator=" ")
                txt_2 = cols[2].get_text(strip=True, separator=" ")
                if txt_0 == "Bias" and txt_1 == "Authors\' judgement" and txt_2 == "Support for judgement":
                    continue
                other.append(cols[0].get_text(strip=True, separator=" ") + "\n" + \
                             cols[1].get_text(strip=True, separator="\n") + "\n" + \
                             cols[2].get_text(strip=True, separator="\n"))

        self.other = "\n".join(other)


class StudyChars(object):
    def __init__(self, studies_chars, title, study_id):
        self.studies_chars = studies_chars

        self.title = title
        self.study_id = study_id
        self.char = Char()
        self.rob = RoB()

    def parse(self):
        found = None
        if self.studies_chars is not None:
            for el in self.studies_chars.find_all(attrs={"class": "table"}):
                el_title = el.find(attrs={"class": "table-title"})
                if el_title is not None:
                    table_title = el_title.get_text(strip=True, separator=" ")
                    if table_title == self.title:
                        found = el
                        break
            if found is not None:
                self.char.parse(found)
                self.rob.parse(found)

    def serialise(self):
        return {
            "study_id": self.study_id,
            "char": self.char.serialise(),
            "rob": self.rob.serialise(),
            "title": self.title
        }


class Reference(object):
    def __init__(self, ref):
        self.ref = ref

        self.id = None
        self.title = None
        self.gs_citation = None
        self.doi = None
        self.pmid = None

    def parse(self):
        self.id = self.ref["id"]
        self.title = self.get_title()
        self.gs_citation, self.doi, self.pmid = self.get_citation_ids()

    def get_citation_ids(self):
        links = self.ref.find_all(attrs={"class": "citation-link"})
        gs, doi, pmid = None, None, None
        for link in links:
            if link is not None:
                if link.get_text(strip=True, separator=" ") == "Google Scholar":
                    gs = link["href"]
                elif link.get_text(strip=True, separator=" ") == "Link to article":
                    doi = link["href"]
                    # assert doi.startswith("https://doi"), doi
                    doi = "/".join(doi.split("/")[-2:])
                elif link.get_text(strip=True, separator=" ") == "PubMed":
                    pmid = link["href"].split("/")[-1]

        return gs, doi, pmid

    def get_title(self):
        title = self.ref.find("span", attrs={"class": "citation-title"})
        if title is not None:
            return title.get_text(strip=True, separator=" ")
        else:
            return None

    def serialise(self):
        return {
            "id": self.id,
            "gs_citation": self.gs_citation,
            "doi": self.doi,
            "pmid": self.pmid
        }


class Study(object):
    def __init__(self, study_refs, studies_chars, status):
        self.study_refs = study_refs
        self.studies_chars = studies_chars

        self.id = None
        self.title = None
        self.status = status  # included, excluded, ongoing, additional, otherVersions
        self.references = []
        self.study_chars = None  # StudyChars

    def parse(self):
        self.id = self.study_refs["id"]
        self.title = self.get_title()
        self.references = self.get_references()
        self.study_chars = StudyChars(self.studies_chars, self.title, self.id)
        self.study_chars.parse()

    def get_references(self):
        parsed = []
        refs = self.study_refs.find_all(attrs={"class": "bibliography-section"})
        for ref in refs:
            r = Reference(ref)
            r.parse()
            parsed.append(r)

        return parsed

    def serialise(self):
        return {
            "id": self.id,
            "status": self.status,
            "references": [ref.serialise() for ref in self.references],
            "study_chars": self.study_chars.serialise()
        }

    def get_title(self):
        title = self.study_refs.find(attrs={"title"})
        if title is not None:
            return title.get_text(strip=True, separator=" ").split("{")[0].strip()
        else:
            return None


class Studies(object):
    def __init__(self, refs_soup):
        self.refs_soup = refs_soup

        self.included = None  # [Study]
        self.excluded = None
        self.ongoing = None
        self.additional = None
        # other versions of that review:
        self.other = None

    def parse(self):
        self.included = self.parse_status("includedStudies")
        self.excluded = self.parse_status("excludedStudies")
        self.ongoing = self.parse_status("ongoingStudies")
        self.additional = self.parse_status("additionalStudies")
        self.other = self.parse_status("otherVersions")

    def parse_status(self, status):
        parsed = []
        # each study can be associated with many references
        studies_refs = self.refs_soup.find_all(attrs={"class": f"bibliographies references_{status}"})
        studies_chars = self.refs_soup.find(
            attrs={"class": f"characteristic{status[0].capitalize() + status[1:]}Content"})

        for study_refs in studies_refs:
            study = Study(study_refs, studies_chars, status)
            study.parse()
            parsed.append(study)

        return parsed

    def serialise(self):
        return {
            "included": [study.serialise() for study in self.included],
            "excluded": [study.serialise() for study in self.excluded],
            "ongoing": [study.serialise() for study in self.ongoing],
            "additional": [study.serialise() for study in self.additional],
            "other": [study.serialise() for study in self.other]
        }
