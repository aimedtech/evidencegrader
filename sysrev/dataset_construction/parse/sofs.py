import re

from bs4 import BeautifulSoup, NavigableString

LABEL_TO_STR = {"inconsistency":
    {
        "inconsistency",
        "heterogeneity"
    },
    "imprecision":
        {
            "imprecision",
            "wide confidence interval",
            "wide 95% confidence interval"
        },
    "indirectness":
        {
            "indirectness"
        },
    "risk of bias":
        {
            "risk of bias",
            "risk of selection bias",
            "risk of serious bias",
            "study design or execution",
            "allocation concealment",
            "blinding"
        },
    "publication bias":
        {
            "publication bias",
            "selective publication"
        }
}
TERMS = [i for s in LABEL_TO_STR.values() for i in s]


def map_n(m, idx):
    """
    :param m: regex match object
    :param idx: group to take
    :return: int
    """
    if m is not None:
        if m.group(idx):
            return int(m.group(idx).replace(",", ""))
        else:
            return 0
    else:
        return 0


class Outcome(object):
    def __init__(self, i, rows, header_txts, table_id):
        """
        :param i: index for current row from which to obtain Outcome information
        :param rows: all rows
        :param header_cells: all cells of table header
        """
        self.table_id = table_id
        self.i = i
        self.rows = rows
        self.header_txts = header_txts

        self.O = None
        self.id = None
        # relative effect
        self.type_effect = None
        self.relative_effect = None
        self.ci_lower = None
        self.ci_upper = None

        self.n_participants = None
        self.n_studies = None

        self.grade = None
        self.reasons = None  # {"raw": "", "labels": defaultdict(label: count)}
        self.absolute_effect = None
        self.comment = None

    def parse(self, footnotes, outcome_id):
        self.id = outcome_id
        self.O = self.get_outcome()
        self.n_participants, self.n_studies = self.get_ns()
        self.type_effect, self.relative_effect, self.ci_lower, self.ci_upper = self.get_relative_effect()

        grade_cell = self.get_grade_cell()
        self.grade = self.get_grade(grade_cell)
        self.reasons = self.get_reasons(grade_cell, footnotes)
        self.absolute_effect = self.get_absolute_effect()
        self.comment = self.get_comment()

    def get_ns(self):
        # get No. of participants/studies cell
        col_n = 0
        text_p = None
        for col in self.rows[self.i].find_all("td"):
            col_n += int(col["colspan"])
            # number of participants / studies
            if col_n == 5:
                text_p = col.get_text(strip=True, separator=" ")
                break

        n_participants, n_studies = self.get_n_part_stud(text_p)

        return n_participants, n_studies

    def get_absolute_effect(self):
        # try to get the right row span:
        if self.grade is not None:
            rowspan = int(self.get_grade_cell()["rowspan"])
            txts = []
            if rowspan - 1 + self.i > len(self.rows) - 1:
                return None
            for row_n in range(rowspan):
                cells = self.rows[self.i + row_n].find_all("td", attrs={"class": "table-tint table-highlight"})
                txts.append([cell.get_text(strip=True, separator=" ") for cell in cells])
            return txts
        else:
            return None

    def get_relative_effect(self):
        col_n = 0
        text_re = None
        for col in self.rows[self.i].find_all("td"):
            col_n += int(col["colspan"])
            # relative effect
            if col_n == 4:
                text_re = col.get_text(strip=True, separator=" ")
                break
        # relative effect, confidence interval
        type_ef, rel_ef, ci_lower, ci_upper = self.get_re_ci(text_re)

        return type_ef, rel_ef, ci_lower, ci_upper

    def get_grade_cell(self):
        grade_backidx = -2
        if self.header_txts is not None:
            for c, txt in enumerate(reversed(self.header_txts)):
                if "grade" in txt.lower():
                    grade_backidx = -(c + 1)

        try:
            tds = self.rows[self.i].find_all("td")
        except IndexError:
            tds = None

        if tds is None:
            found = None
        else:
            try:
                found = tds[grade_backidx]
            except IndexError:
                found = None
            if found is None:
                try:
                    found = self.rows[self.i].find_all("td")[-2]
                except IndexError:
                    found = None

        return found

    def get_grade(self, grade_cell):
        if grade_cell is not None:
            txt = grade_cell.get_text(strip=True, separator=" ")
            match = re.search("(very low|low|moderate|high)", txt, flags=re.IGNORECASE)
            if match is None:
                grade = "other"
            else:
                grade = match.group().lower()
        else:
            print("Not found")
            grade = None

        return grade

    def get_reasons(self, grade_cell, footnotes):
        reasons = {}
        raw = []
        if grade_cell is not None:
            sup = grade_cell.sup
            if sup is not None:  # <sup> tag
                txt = sup.get_text(strip=True, separator=" ")
                footnote_ids = re.findall("(\d+|\w)", txt)
                raw = [footnotes[footnote_id] for footnote_id in footnote_ids if footnote_id in footnotes]
            else:
                # superscript unicode
                footnote_ids = re.findall("[⁰¹²³⁴⁵⁶⁷⁸⁹]+", grade_cell.get_text(strip=True, separator=" "))
                if footnote_ids:
                    raw = [footnotes[footnote_id] for footnote_id in footnote_ids if footnote_id in footnotes]

            reasons["raw"] = raw

            reasons["labels"] = self.get_reasons_labels(raw)
        return reasons

    def serialise(self):
        d = {
            "id": self.id,
            "O": self.O,
            "absolute_effect": self.absolute_effect,
            "grade": self.grade,
            "comment": self.comment,
            "n_participants": self.n_participants,
            "n_studies": self.n_studies,
            "reasons": self.reasons,
            "type_effect": self.type_effect,
            "relative_effect": self.relative_effect,
            "ci_lower": self.ci_lower,
            "ci_upper": self.ci_upper
        }
        return d

    def get_outcome_and_followup(self):
        O, followup = None, None
        cell = self.rows[self.i].td.get_text(strip=True, separator=" ")

        # match body and follow-up
        m = re.match("(.*)Follow[ ‐-]up(.*)", cell)
        if m is not None:
            O = m.group(1).strip("( :")
            followup = m.group(2).strip(") :")
        else:
            O = cell
        return O, followup

    def get_outcome(self):
        cell = self.rows[self.i].td.get_text(strip=True, separator=" ")

        return cell

    def get_n_part_stud(self, text_p):
        # extract numbers for participants and studies
        if text_p is None or text_p.strip() == "-":
            n_participants = None
            n_studies = None
        else:
            m = re.match("(\d+(?:\,\d+)?).*?(\d+)", text_p)
            n_participants = map_n(m, 1)
            n_studies = map_n(m, 2)

        return n_participants, n_studies

    def get_re_ci(self, text_re):
        # relative effect and confidence interval
        type_ef, rel_ef, ci_lower, ci_upper = None, None, None, None
        if text_re is not None:
            # float regex
            float_group = "(\d+.\d+)"

            m = re.match(f"(.*?){float_group}.*?\({float_group} to {float_group}\)", text_re)
            if m is not None:
                type_ef = m.group(1).strip(" -")
                try:
                    rel_ef = float(m.group(2))
                except ValueError:
                    rel_ef = None
                try:
                    ci_lower = float(m.group(3))
                except ValueError:
                    ci_lower = None
                try:
                    ci_upper = float(m.group(4))
                except ValueError:
                    ci_upper = None

        return type_ef, rel_ef, ci_lower, ci_upper

    def get_comment(self):
        comment = None
        found = self.rows[self.i].find_all("td")[-1]

        if found is not None:
            comment = found.get_text(strip=True, separator=" ")

        return comment

    def get_reasons_labels(self, raw):
        labels = {}
        for r in raw:
            # find downgrading labels that were downgraded twice
            # this can overwrite any single-score label
            found = self.get_downgraded_twice(r)
            for l in found:
                labels[l] = 2
            # find rest
            for label, txt in LABEL_TO_STR.items():
                for t in txt:
                    if label not in labels and t in r.lower():
                        labels[label] = 1

        return labels

    def get_downgraded_twice(self, r):
        return re.findall(f"(?:twice|two times|two steps|two levels) (?:for|due to|because of) .*?({'|'.join(TERMS)})",
                          r, flags=re.IGNORECASE)


class SummaryOfFindings(object):
    def __init__(self, table):
        self.table = table

        self.table_id = None
        self.title = None
        self.P = None
        self.setting = None
        self.I = None
        self.C = None
        self.outcomes = []  # list of Outcome objects
        self.table_comment = None  # general table comment (*)
        self.abbrs = None
        self.absolute_effect_header = None

    def parse(self):
        self.table_id = self.table["id"]
        self.title = self.get_title()
        self.P, self.setting, self.I, self.C = self.get_psic()

        self.get_outcomes()
        self.table_comment, self.abbrs = self.get_comment_abbr()
        self.get_absolute_effect_header()

    def get_outcomes(self):
        footnotes = self.get_footnotes()  # get footnotes to extract the reasons for downgrading
        header_txts = self.get_header()
        rows = self.get_outcome_rows()
        self.fill_outcomes(footnotes, header_txts, rows)

    def serialise(self):
        return {
            "P": self.P,
            "I": self.I,
            "C": self.C,
            "abbrs": self.abbrs,
            "absolute_effect_header": self.absolute_effect_header,
            "table_comment": self.table_comment,
            "outcomes": [outcome.serialise() for outcome in self.outcomes],
            "setting": self.setting,
            "table_id": self.table_id,
            "title": self.title
        }

    def get_title(self):
        return self.table.find("div", "table-heading").find("span", "table-title").text

    def get_psic(self):
        population = None
        setting = None
        intervention = None
        comparison = None

        bodies = self.table.find_all("tbody")
        assert len(bodies) == 1
        body = bodies.pop()

        for p in body.find_all("p"):
            txt_split = p.get_text("\n", strip=True).split("\n")
            for i in range(len(txt_split)):
                if population is None and txt_split[i].startswith("Patient or population"):
                    if i + 1 <= len(txt_split) - 1:
                        population = txt_split[i + 1].strip(" :")
                elif setting is None and txt_split[i].startswith("Setting"):
                    if i + 1 <= len(txt_split) - 1:
                        setting = txt_split[i + 1].strip(" :")
                elif intervention is None and txt_split[i].startswith("Intervention"):
                    if i + 1 <= len(txt_split) - 1:
                        intervention = txt_split[i + 1].strip(" :")
                elif comparison is None and txt_split[i].startswith("Comparison"):
                    if i + 1 <= len(txt_split) - 1:
                        comparison = txt_split[i + 1].strip(" :")

        intervention, comparison = self.fix_intervention(intervention, comparison)

        return population, setting, intervention, comparison

    def fix_intervention(self, intervention, comparison):
        if intervention is not None:
            m = re.search("(.*) versus (.*)", intervention)
            if m is not None:
                intervention = m.group(1)
                # assert comparison is None
                comparison = m.group(2)

        return intervention, comparison

    def get_absolute_effect_header(self):
        headers = []
        for tr in self.table.find("tbody").find_all("tr", attrs={"class": "table-header separated"}):
            cells = tr.find_all("td", attrs={"class": "table-tint table-highlight"})
            if cells is not None:
                headers.append([cell.get_text(strip=True, separator=" ") for cell in cells])

        return headers

    def get_comment_abbr(self):
        comment = None
        abbrs = None
        row = self.get_comment_abbr_row()

        if row is not None:
            pars = row.find_all("p")
            if len(pars) == 2:
                # reasonably confident we have comment and abbr:
                comment = self.get_comment(pars)
                abbrs = self.get_abbr(pars)
            elif len(pars) == 1:
                # break at <br/>
                par = str(pars[0]).replace("<br/>", "|br|").replace("<br>", "|br|")
                els = []
                for i in BeautifulSoup(par, "lxml").get_text(strip=True, separator=" ").split("|br|"):
                    if i.strip():
                        els.append(BeautifulSoup(i.strip(), "lxml"))
                if len(els) == 2:
                    comment = self.get_comment(els)
                    abbrs = self.get_abbr(els)
                else:
                    comment = self.get_comment(pars)
            else:
                comment = self.get_comment(pars)

        return comment, abbrs

    def get_comment_abbr_row(self):
        row = None
        was_highlight = False  # to mark the last highlighted row (before table comment)
        for tr in self.table.find("tbody").find_all("tr", attrs={"class": "separated"}):
            if len(tr.attrs["class"]) == 1:
                if tr.find("td", attrs={"class": "table-tint table-highlight"}):
                    was_highlight = True
                elif was_highlight and tr.find("td", attrs={"colspan": "7"}):
                    row = tr
                    break

        return row

    def get_comment(self, pars):
        if len(pars) == 2:
            return pars[0].get_text(strip=True, separator=" ")
        elif len(pars) == 0:
            return None
        else:
            return "\n".join([par.get_text(strip=True, separator=" ") for par in pars])

    def get_abbr(self, pars):
        abbr_map = {}
        for i in pars[1].get_text(strip=True, separator=" ").strip(";").split(";"):
            if len(i.split(":")) != 2:
                continue
            abbr, desc = i.split(":")
            abbr_map[abbr] = desc.strip(" .")

        return abbr_map

    def get_header(self):
        header = self.table.find("tbody").find("tr", attrs={"class": "table-header separated"})
        if header:
            header_txts = [td.get_text(strip=True, separator=" ") for td in header.find_all("td")]
        else:
            header_txts = None

        return header_txts

    def get_outcome_rows(self):
        rows = []
        for tr in self.table.find("tbody").find_all("tr", attrs={"class": "separated"}):
            if len(tr.attrs["class"]) == 1 and tr.find("td", attrs={"class": "table-tint table-highlight"}):
                rows.append(tr)

        return rows

    def fill_outcomes(self, footnotes, header_txts, rows):
        for i in range(len(rows)):
            n_cols = sum(int(col["colspan"]) for col in rows[i].find_all("td"))
            if n_cols == 7 or n_cols == 6:
                outcome = Outcome(i, rows, header_txts, self.table_id)
                outcome_id = len(self.outcomes)
                outcome.parse(footnotes, outcome_id)
                self.outcomes.append(outcome)

    def get_footnotes(self):
        # get footnotes to extract the reasons for downgrading
        footnotes = {}
        footers = self.get_footers()

        for footer in footers:
            footer_split = footer.contents
            # one footnote in one paragraph
            if len(footer_split) == 2 and footer_split[0].name == "sup":
                footnotes[footer_split[0].get_text(strip=True, separator=" ")] = footer_split[1]
            # several footnotes in one paragraph
            elif len(footer_split) > 2 and footer_split[0].name == "sup":
                if footer_split[0].name == "sup":
                    f_sym = footer_split[0].get_text(strip=True, separator=" ")
                    txt = []
                    for el in footer_split[1:]:
                        if isinstance(el, NavigableString) and not el.strip():
                            continue
                        if el.name == "br":
                            footnotes[f_sym] = " ".join(txt)
                            f_sym = None
                            txt = []
                            continue
                        if el.name == "sup" and f_sym is None:
                            f_sym = el.get_text(strip=True, separator=" ")
                        else:
                            if isinstance(el, NavigableString):
                                txt.append(el)
                            else:
                                txt.append(el.get_text(strip=True, separator=" "))
                    if f_sym is not None:
                        footnotes[f_sym] = " ".join(txt)
                else:
                    print("Unhandled case!")

            # one footnote in one paragraph, no superscript for the footnote symbol
            elif len(footer_split) == 1:
                found = re.match("([⁰¹²³⁴⁵⁶⁷⁸⁹]+)(.*)", footer_split[0].strip())
                if found is not None:
                    footnotes[found.group(1)] = found.group(2).strip()

        return footnotes

    def get_footers(self):
        tfoot = self.table.find("tfoot")
        if tfoot is None:
            footers = [tr for tr in self.table.find("tbody").find_all("tr") if tr.find("td", attrs={"colspan": "7"})]
            # TODO take all footer rows, not just the last one
            footers = [footers[-1]] if footers else []
        else:
            footers = tfoot.find("div", "table-footnote").find_all("p")

        return footers
