# Parsing scraped systematic reviews from the Cochrane database.

To parse the scraped html files (i.e. create a structured representation of each file in json format),
run [parse.py](parse.py) with the following arguments:

- `-output_dir`: path to directory that will contain the new parsed files
- `-input_dir`: path to directory containing scraped html files
- `-refs_input_dir`: path to directory with html files for references (`refs_html`)
- `-metadata_f`: path to metadata csv file created during scraping

Here's a high-level view of the data structure that we capture in the resulting json files:

![Generic data model](data_model.png)