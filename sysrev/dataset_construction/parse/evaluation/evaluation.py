import argparse
import random
from pathlib import Path

from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

from sysrev.dataset_construction.scrape.metadata import get_title_to_docids, get_docid_to_urls
from sysrev.dataset_construction.util.util import read_csv, load_json, get_file_list

GRADE_LEVELS = ["high", "moderate", "low", "very low"]
GRADE_TO_STEPS = {"high": 0, "moderate": 1, "low": 2, "very low": 3}
HEADER_TO_LABEL = {"DowngradedRoBrandomisationblindi": "risk of bias",
                   "Downgradedimprecision": "imprecision",
                   "Downgradedinconsistency": "inconsistency",
                   "Downgradedpublicationbias": "publication bias",
                   "Downgradedindirectness": "indirectness"}
TO_EVAL = {'4ea497e0383a4088ad0025be12601df9', '8fadec6e2d4f48ba9fcbbca03e746dcc', '352aae7c36d34259a2fcf467ea7d233a',
           'fa7a5236090343c8b395d6ac9282cc2a', '2975cd93332c46f19dc12516be9a07a4', '5e3c1c1eadf340cab7274212fed3993f',
           'e3ad1e29a045450797a0cd706518df14', '862abdbeb6cd4629adb5d438b3ab4191', '669d9d842ceb4a0c9093660b631b1628',
           '3f214145705a47a3a44f5b942a176506', '1b946d10fc454a568905778651c8d1ad', '787bb649f1f8471eb6522b2fbec67f16',
           '8907168f389c454795c7f44f8221380b', 'bd7bf683ebe24147afb9f76986d992bb', '72fee692e4834b1baf6c6a5368d550ea',
           '0901dcd234284c98919459eda8156e42', '44de6ae928534133a99e7945fe291c67', '77d34dcd7843491e8ca4d310aa03c7ff',
           'c142fcce74384dea8b4a5258ee101f6c', 'e16d2894f0534c148ae2c7a239ce1305', 'fb2ca337a0a84a67880dd5c433356166',
           'ad332288f5974de2a5ea27e1eef55bef', '2bb98cacf0c9406f8f533e1421bf9c4e', 'e50282a073ec4a669a7f6f358089b40d',
           '4aab5b7e9c70464989d45eeb37b50fd8', 'd099174455034f5a9659a758a85b2765', '53d8020cdd804a83acaf5240212ba41a',
           'f0611e10cf3141b1b7014b396a56ea98', 'db56b5284eb145c4b2a84e5d895534e8', '820e01267b124bd69ac104e63f4fc0f5',
           '69e48520c615419a8d06e7e28bda1d69', 'a46fe9540d584637be4a10ad2fd18288', '3d7f02a9084f4019a4d6109f054dd225',
           'a501aa3055ab423faf3a0451fb5ee12f', 'ba61cc5421074af2bd69ffb5cb4bba4a', '0ecfcf401054477f94af112e2100cf44',
           'd05b06c5da8c4f45a6dcbae391062756', 'af0666633ce340c3a3676c86bc393075', '27cd705281984e3eae1323b5dd48bb24',
           'e3d3bb37ec7f4c4286c3aaf21593cbee', 'a712f4701b3446c7a9ae1eded551dd88', 'ef33a48e4dbc441592a210fdd59cbf54',
           'e7727b6549f34508b150f4687fdb99b2', '1d5f574c557a45b58152b9afd7ae44a7', 'f4718a454e4745b38e8981256eb75bc6',
           'a6509014d4a245ab87ea1e8c9f57df7c', 'f02e3cb1bf5342b4978afe542bde00bd', '90c2f9c479ed4e0cbc489a00cdea2652',
           '3158b9934076483d927e695510d39957', '68d8505308b841cdbdfd3654cc9d386b', 'bbfecd7b6e17475ba1d82e64d85816d7',
           '50ab55f4f60c4fe5bbfe3cd1ae4abe1f', '35b6071c19bd4913b3958240d3ec38e0', '75984ed2d4cf456490743574827fd313',
           'd05a35f0577f496488e7db2febd1a78e', '0b6ad617c58d4cb48f1a5b100c874702', '78eb65c064b443aba61e5919be1613e5',
           '4cce24ff2b744537b4e7e415fc40152c', 'c928f735096e4589b3a60f5578deb54c', '16bf8a8b4030448eb4def9198592742f',
           '16f597000a204423bbe79ec8d595215c', 'a17b50c32791490badd6e91bed6d7cae', 'ff2cbca1f7a643e9a036435d1d54f515',
           '18ae3bd3c62f4216b4f6153ea867d6e4', 'b342265f1a4740ac9547292896323ef6', 'c495cd2cf122478380b663d7f21091a5',
           'd67318c87aa040aca6740b911cc4f755', 'b5bc8bc96e644f1f80206866a3c02edd', '6c1e5b82df6e44eda5c50da979c22472',
           '277d5b779aa84be5a1374d9df47e1bed', 'f39fc6f4857a414782c3d638981ed712', '8f95924fff6a47cf8c74476cea46b555',
           '2675a1b45fb14d9d96a82374ef1adaa7', 'e3b45f191c9547b498096c5f152b7d7d', 'cc58ff56dab744a49b19505742337de4',
           '6d95058894d746bf84387b5a7ba1128b', '23f171447fdb4cbeb0575121e03a15f5', 'f75faa74a40143e3ba55819998ae1b92',
           'bc50a075f26e4235aa989d52c5f1ec31', '5d50687f274943f2955aa828273befb4', '81632abc0ff04f95a258036f4fdcab89',
           'b416ab1e2064446e9f8ed8905ecd3fa7', 'a728a3177b8441348656d7beb2a9d239', 'd89df2467200477791f4a68ba01a5501',
           '4a6bf4eed3f94c3fb835c42299d4f1a4', 'cd5e74bbff794cd696d789d7a16217b2'}


def get_docid(title, title_to_docids):
    title = title.replace("?", " ")

    if title not in title_to_docids:
        return None
    assert len(title_to_docids[title]) == 1
    return title_to_docids[title].pop()


class EvalInstance:
    def __init__(self, docid):
        self.docid = docid
        self.grade = None
        self.n_studies = None
        self.n_participants = None
        self.reasons = None

    def parse(self, unit):
        self.grade = self.get_grade(unit)
        self.n_studies, self.n_participants = self.get_ns(unit)
        self.reasons = self.get_reasons(unit)


class GoldInstance(EvalInstance):
    def __init__(self, docid, row):
        EvalInstance.__init__(self, docid)
        self.parse(row)

    def get_grade(self, row):
        grade_vec = [row["PHigh"], row["PModerate"], row["PLow"], row["PVerylow"]]
        if "1" not in grade_vec:
            return None
        grade = GRADE_LEVELS[grade_vec.index("1")]
        return grade

    def get_ns(self, row):
        try:
            n_studies = int(row["Pnumberoftrials"])
        except ValueError:
            n_studies = None

        try:
            n_participants = int(row["Ptotaln"])
        except ValueError:
            n_participants = None

        return n_studies, n_participants

    def get_reasons(self, row):
        reasons = []
        for r in ["DowngradedRoBrandomisationblindi", "Downgradedimprecision", "Downgradedinconsistency",
                  "Downgradedpublicationbias", "Downgradedindirectness"]:
            if row[r] == "1":
                reasons.append(HEADER_TO_LABEL[r])

        return sorted(reasons)


class OurInstance(EvalInstance):
    def __init__(self, docid, parsed):
        EvalInstance.__init__(self, docid)
        self.parse(parsed)

    def get_grade(self, parsed):
        try:
            # only first!
            grade = parsed["sofs"][0]["outcomes"][0]["grade"]
        except IndexError:
            grade = None

        return grade

    def get_ns(self, parsed):
        try:
            # only first!
            n_studies = parsed["sofs"][0]["outcomes"][0]["n_studies"]
        except IndexError:
            n_studies = None

        try:
            # only first!
            n_participants = parsed["sofs"][0]["outcomes"][0]["n_participants"]
        except IndexError:
            n_participants = None

        return n_studies, n_participants

    def get_reasons(self, parsed):
        try:
            # only first!
            reasons = parsed["sofs"][0]["outcomes"][0]["reasons"]["labels"]
        except IndexError:
            reasons = None

        return reasons


class OurInstanceAll(EvalInstance):
    def __init__(self, docid, parsed):
        EvalInstance.__init__(self, docid)
        self.parse(parsed)
        self.fields = self.get_fields(parsed)

    def get_fields(self, parsed):
        for sof in parsed["sofs"]:
            table_id = sof["table_id"]
            table_title = sof["title"]
            for outcome in sof["outcomes"]:
                outcome_name = outcome["O"]
                yield outcome_name, table_id, table_title

    def get_grade(self, parsed):
        for sof in parsed["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                yield grade

    def get_ns(self, parsed):
        return None, None

    def get_reasons(self, parsed):
        for sof in parsed["sofs"]:
            for outcome in sof["outcomes"]:
                try:
                    reasons = outcome["reasons"]["labels"]
                except KeyError:
                    reasons = None
                yield reasons


def load_conwayetal2017(f, title_to_docids, input_dir):
    data = []
    for row in read_csv(f):
        docid = get_docid(row["Review"], title_to_docids)
        if docid is None or docid not in TO_EVAL:
            continue
        parsed = load_json(f"{input_dir}/{docid}.json")
        data.append((GoldInstance(docid, row), OurInstance(docid, parsed)))

    return data


class EvalReport(object):
    def __init__(self, paired_data):
        self.paired_data = paired_data

    def grade_report(self):
        gs, ps, diffs = [], [], []
        for g, p in self.paired_data:
            if g.grade is not None:
                gs.append(g.grade)
                if g.grade != p.grade:
                    diffs.append((g.grade, p.grade, g.docid))
                if p.grade is None:
                    ps.append("None")
                else:
                    ps.append(p.grade)

        score = classification_report(gs, ps, labels=["high", "moderate", "low", "very low", "None"])
        print(f"Grade, classification report:\n{score}")

        score = confusion_matrix(gs, ps, labels=["high", "moderate", "low", "very low", "None"])
        print(f"Grade, confusion matrix:\n{score}")

        return diffs

    def ns_report(self):
        gs, ps, diffs = [], [], []
        for g, p in self.paired_data:
            if g.n_studies is not None:
                gs.append(g.n_studies)

                if p.n_studies != g.n_studies:
                    diffs.append((g.n_studies, p.n_studies, g.docid))

                if p.n_studies is None:
                    ps.append(-1)
                else:
                    ps.append(p.n_studies)
        score = accuracy_score(gs, ps)
        print(f"N studies, acc.:\n{score}")

        gs, ps = [], []
        for g, p in self.paired_data:
            if g.n_participants is not None:
                gs.append(g.n_participants)
                if p.n_participants is None:
                    ps.append(-1)
                else:
                    ps.append(p.n_participants)

        score = accuracy_score(gs, ps)
        print(f"N participants, acc.:\n{score}")

        return diffs

    def reasons_report(self, docid_to_urls):
        gs, ps, diffs = [], [], []
        for g, p in self.paired_data:
            if g.reasons is not None:
                gs.append(g.reasons)
                if g.reasons != p.reasons:
                    diffs.append((g.reasons, p.reasons, g.docid))
                if p.reasons is not None and p.grade not in {"high", "other"} and len(p.reasons) == GRADE_TO_STEPS[
                    p.grade]:
                    ps.append((p.reasons, p.grade, docid_to_urls[p.docid][0], g.reasons))
                # if p.reasons is None:
                #    ps.append("None")
                # else:
                #    ps.append(p.reasons)

        # score = classification_report(gs, ps, labels=["high", "moderate", "low", "very low", "None"])
        for p in ps:
            print(f"{p[0]}\t{p[1]}\t{p[2]}\t{p[3]}")
        print()
        return diffs


def load_random_valid_reasons(input_dir, conwayetal2017_f, title_to_docids, docid_to_urls, k=100):
    """
    Selects random k files, loads them. Only instances where GRADE is moderate, low, very low and where number of
    reasons corresponds to the GRADE score.
    """
    fs = set(get_file_list(input_dir))

    # filter out Conway's files
    conway_fs = set()
    for row in read_csv(conwayetal2017_f):
        docid = get_docid(row["Review"], title_to_docids)
        if docid is not None:
            conway_fs.add(f"{input_dir}{docid}.json")

    filtered_fs = list(fs - conway_fs)

    # get all grades and reasons
    ps = []
    for f in filtered_fs:
        fh = load_json(f)
        p = OurInstanceAll(Path(f).stem, fh)
        for outcome_name, grade, reasons in zip(p.O, p.grade, p.reasons):
            if reasons is not None and grade not in {"high", "other"} and len(reasons) == GRADE_TO_STEPS[grade]:
                docurl = docid_to_urls[p.docid][0]
                ps.append((outcome_name, reasons, grade, docurl))
    print(f"N all instances: {len(ps)}")

    # random k instances
    random.Random(4).shuffle(ps)
    for p in ps[:k]:
        print(f"{p[0]}\t{p[1]}\t{p[2]}\t{p[3]}")


def load_random_invalid_reasons(input_dir, conwayetal2017_f, title_to_docids, docid_to_urls, k=100):
    """
    Selects random k files, loads them. Only instances where GRADE is moderate, low, very low and where number of
    reasons DOES NOT corresponds to the GRADE score.
    """
    fs = set(get_file_list(input_dir))

    # filter out Conway's files
    conway_fs = set()
    for row in read_csv(conwayetal2017_f):
        docid = get_docid(row["Review"], title_to_docids)
        if docid is not None:
            conway_fs.add(f"{input_dir}{docid}.json")

    filtered_fs = list(fs - conway_fs)

    # get all grades and reasons
    ps = []
    for f in filtered_fs:
        fh = load_json(f)
        p = OurInstanceAll(Path(f).stem, fh)
        for fields, grade, reasons in zip(p.fields, p.grade, p.reasons):
            outcome_name, table_id, table_title = fields
            if grade not in {"high", "other"} and (reasons is None or sum(reasons.values()) != GRADE_TO_STEPS[grade]):
                docurl = docid_to_urls[p.docid][0]
                ps.append((outcome_name, table_title, table_id, reasons, grade, docurl))
    print(f"N all instances: {len(ps)}")

    # random k instances
    random.Random(4).shuffle(ps)
    c = 0
    for p in ps:
        if c == k:
            break
        if isinstance(p[0], list):
            continue
        print(f"{p[0]}\t{p[1]}\t{p[2]}\t{p[3]}\t{p[4]}\t{p[5]}")
        c += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-metadata_f',
                        help='path to metadata.csv')
    parser.add_argument('-conwayetal2017_f',
                        help='path to csv evaluation file from Conway et al. 2017')
    parser.add_argument('-input_dir',
                        help='input dir containing parsed json files')
    args = parser.parse_args()

    title_to_docids = get_title_to_docids(args.metadata_f)
    docid_to_urls = get_docid_to_urls(args.metadata_f)
    """
    paired_data = load_conwayetal2017(args.conwayetal2017_f, title_to_docids, args.input_dir)
    report = EvalReport(paired_data)

    diffs_grade = report.grade_report()
    for g_grade, p_grade, docid in diffs_grade:
        print(g_grade.replace(" ", "_") if isinstance(g_grade, str) else g_grade,
              p_grade.replace(" ", "_") if isinstance(p_grade, str) else p_grade,
              docid_to_urls[docid])

    diffs_n_studies = report.ns_report()
    for g_n_studies, p_n_studies, docid in diffs_n_studies:
        print(g_n_studies, p_n_studies, docid_to_urls[docid])

    diffs_reasons = report.reasons_report(docid_to_urls)
    """
    # load_random_valid_reasons(args.input_dir, args.conwayetal2017_f, title_to_docids, docid_to_urls)
    load_random_invalid_reasons(args.input_dir, args.conwayetal2017_f, title_to_docids, docid_to_urls)
