import argparse
from collections import Counter

from sysrev.util.util import read_csv


def f1_score(pred, gold):
    common = Counter(pred) & Counter(gold)
    num_same = sum(common.values())
    if num_same == 0:
        return 0, 0, 0
    precision = 1.0 * num_same / len(pred)
    recall = 1.0 * num_same / len(gold)
    f1 = (2 * precision * recall) / (precision + recall)
    return precision, recall, f1


def fill_gold(row):
    gold = []
    for reason in ["imprecision", "inconsistency", "indirectness", "risk of bias", "publication bias"]:
        if row[reason]:
            for i in range(int(row[reason])):
                gold.append(reason)

    return gold


def get_insts(f):
    insts = []
    for row in read_csv(f):
        pred = eval(row["pred"])
        gold = fill_gold(row)
        if gold:
            insts.append((pred, gold))

    return insts


def report_f1(insts):
    precisions = 0
    recalls = 0
    f1s = 0
    for inst in insts:
        precision, recall, f1 = f1_score(*inst)
        precisions += precision
        recalls += recalls
        f1s += f1

    print(f"avg precision: {precisions / len(insts)}")
    print(f"avg recall: {recalls / len(insts)}")
    print(f"avg f1: {f1s / len(insts)}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', help='csv file with manual annotations')
    args = parser.parse_args()

    insts = get_insts(args.f)
    report_f1(insts)
