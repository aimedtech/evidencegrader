import argparse
import multiprocessing
import os
import sys
from pathlib import Path

from bs4 import BeautifulSoup
from tqdm import tqdm

from sysrev.dataset_construction.parse.sofs import SummaryOfFindings
from sysrev.dataset_construction.parse.studies import Studies
from sysrev.dataset_construction.scrape.metadata import get_docid_to_topics, get_docid_to_year
from sysrev.dataset_construction.util.util import get_file_list, save_json

sys.setrecursionlimit(100000)


class Parsed(object):
    def __init__(self, f, refs_f):
        self.f = f
        self.refs_f = refs_f

        self.docid = Path(self.f).stem
        self.doi = None
        self.texts = None
        self.title = None
        self.authors = None
        self.sofs = None  # [SummaryOfFindings()]
        self.studies = None  # Studies()
        self.review_type = None
        self.topics = None
        self.year = None

    def parse(self, docid_to_topics=None, docid_to_year=None):
        if docid_to_topics is None:
            docid_to_topics = {}
        if docid_to_year is None:
            docid_to_year = {}

        with open(self.f) as fh, open(self.refs_f) as fh_refs:
            soup = BeautifulSoup(fh, "lxml")
            refs_soup = BeautifulSoup(fh_refs, "lxml")

        self.doi = self.get_doi(soup)
        self.texts = self.get_texts(soup)
        self.title = self.get_title(soup)
        self.authors = self.get_authors(soup)

        # summaries of findings
        self.sofs = self.get_sofs(soup)

        # review type: intervention, diagnostic, overview, qualitative, ...
        self.review_type = self.get_review_type(soup)

        self.studies = self.get_refs(refs_soup)

        self.topics = docid_to_topics.get(self.docid, None)
        self.year = docid_to_year.get(self.docid, None)

    def get_sofs(self, soup):
        sofs = []
        sofs_sec = soup.find("section", attrs={"class": "summaryOfFindings"})
        if sofs_sec is not None:
            tables = sofs_sec.find_all("div", "table")
            for table in tables:
                sof = SummaryOfFindings(table)
                sof.parse()
                sofs.append(sof)

        return sofs

    def write(self, output_dir):
        ser_parsed = self.serialise()
        f_id = Path(self.f).stem
        save_json(ser_parsed, f"{output_dir}/{f_id}.json")

    def get_review_type(self, soup):
        """
        Return review type:
        intervention, diagnostic, overview, qualitative, ...
        """
        review_type_node = soup.find("span", "publish-type")
        if review_type_node is None:
            return None
        # assert len(review_type_node.find_parents("header")) == 1

        return review_type_node.text

    @staticmethod
    def get_refs(self, refs_soup):
        studies = Studies(refs_soup)
        studies.parse()

        return studies

    def report(self, n):
        for c, sof in enumerate(self.sofs):
            for outcome in sof.outcomes:
                yield n, c, self.doi, len(self.studies.included), outcome.n_studies, outcome.grade, outcome.reasons

    def get_doi(self, soup):
        url = soup.find("meta", attrs={"property": "og:url"})
        if url is not None:
            doi = url["content"]
        else:
            doi = None

        return doi

    def serialise(self):
        return {"docid": self.docid,
                "doi": self.doi,
                "texts": self.texts,
                "title": self.title,
                "authors": self.authors,
                "sofs": [sof.serialise() for sof in self.sofs],
                "review_type": self.review_type,
                "studies": self.studies.serialise(),
                "topics": self.topics,
                "year": self.year}

    def get_title(self, soup):
        titles = soup.find_all(attrs={"class": "publication-title"})
        if len(titles) == 1:
            return titles.pop().get_text(strip=True, separator=" ")
        elif len(titles) > 1:
            return " ".join([title.get_text(strip=True, separator=" ") for title in titles])
        else:
            return None

    def get_authors(self, soup):
        authorss = soup.find_all(attrs={"class": "publication-authors"})
        if len(authorss) == 1:
            authors = authorss.pop()
            authors = [el for el in authors.find_all(attrs={"class": "author"}) if len(el.attrs["class"]) == 1]
            return [author.get_text(strip=True, separator=" ") for author in authors]
        elif len(authorss) > 1:
            all = []
            for authors in authorss:
                _authors = [el for el in authors.find_all(attrs={"class": "author"}) if len(el.attrs["class"]) == 1]
                all.append([author.get_text(strip=True, separator=" ") for author in _authors])
            return all
        else:
            return None

    def get_texts(self, soup):
        full_abstract = soup.find(attrs={"class": "full_abstract"})
        full_abstract_txt = full_abstract.get_text(strip=True, separator="\n") if full_abstract is not None else None

        abstract_conclusion_txt = self.get_abstract_conclusion(full_abstract)

        plain_language_summary = soup.find(attrs={"class": "abstract_plainLanguageSummary"})
        plain_language_summary_txt = plain_language_summary.get_text(strip=True,
                                                                     separator="\n") if plain_language_summary is not None else None

        authors_conclusions = soup.find("section", attrs={"class": "conclusions"})
        authors_conclusions_txt = authors_conclusions.get_text(strip=True,
                                                               separator="\n") if authors_conclusions is not None else None

        return {
            "full_abstract_txt": full_abstract_txt,
            "abstract_conclusion_txt": abstract_conclusion_txt,
            "plain_language_summary_txt": plain_language_summary_txt,
            "authors_conclusions_txt": authors_conclusions_txt
        }

    def get_abstract_conclusion(self, full_abstract):
        conc_txt = None
        if full_abstract is not None:
            conc = full_abstract.find("h3", string="Authors' conclusions")
            if conc is not None:
                conc_ps = conc.parent.find_all("p")
                conc_txt = "\n".join([p.get_text(strip=True, separator=" ") for p in conc_ps])

        return conc_txt


def get_refs_f(f, refs_input_dir):
    """
    Get path for the reference file, if exists, else return None.
    """
    name = Path(f).name
    refs_f = f"{refs_input_dir}/{name}"
    if Path(refs_f).is_file():
        return refs_f
    else:
        return None


def process(f, refs_input_dir, output_dir, docid_to_topics=None, docid_to_year=None):
    refs_f = get_refs_f(f, refs_input_dir)
    p = Parsed(f, refs_f)
    p.parse(docid_to_topics, docid_to_year)
    p.write(output_dir)


def parse_articles(input_dir, refs_input_dir, output_dir, metadata_f):
    fs = get_file_list(input_dir, identifiers=["html"])
    docid_to_topics = get_docid_to_topics(metadata_f)
    docid_to_year = get_docid_to_year(metadata_f)

    p = multiprocessing.Pool()

    for n, f in enumerate(tqdm(fs)):
        # f_id = Path(f).stem
        # if os.path.exists(f"{output_dir}/{f_id}.json"):
        #    continue
        # process(f, refs_input_dir, output_dir, docid_to_topics, docid_to_year)
        p.apply_async(process, [f, refs_input_dir, output_dir, docid_to_topics, docid_to_year])

    p.close()
    p.join()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_dir',
                        help='input dir for html files')
    parser.add_argument('-refs_input_dir',
                        help='input dir for reference html files')
    parser.add_argument('-metadata_f',
                        help='path to metadata.csv')
    parser.add_argument('-output_dir',
                        help='output dir for processed json files')
    args = parser.parse_args()

    if not Path(args.output_dir).exists():
        os.makedirs(args.output_dir)
    parse_articles(args.input_dir, args.refs_input_dir, args.output_dir, args.metadata_f)
