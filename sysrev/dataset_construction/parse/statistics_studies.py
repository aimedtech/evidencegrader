import argparse
from collections import Counter

import editdistance
import numpy as np
from scipy.stats import describe
from tqdm import tqdm

from sysrev.dataset_construction.parse.evaluation.evaluation import GRADE_TO_STEPS
from sysrev.dataset_construction.util.util import get_file_list, load_json

EDIT_DISTANCES = {}


def equal_effect_size(param, param1):
    if param is None and param1 is None:
        return True
    if param is None or param1 is None:
        return False
    return round(param, 2) == round(param1, 2)


def get_from_best_key(d, search_key):
    """
    Return the value from 'd' for the key that best matches the 'search_key'. The criterion is minimum-edit distance.
    """
    curr_distances = []
    for comp_key in d.keys():
        try:
            # look up
            dist = EDIT_DISTANCES[search_key, comp_key]
        except KeyError:
            # calculate and save
            dist = editdistance.eval(search_key, comp_key)
            EDIT_DISTANCES[(search_key, comp_key)] = dist
        curr_distances.append(dist)
    best_comp_key = list(d.keys())[np.argmin(curr_distances)]
    return d[best_comp_key], best_comp_key


def get_relevant_studies(studies_per_outcome, doi, sof_title, outcome, n_studies, type_effect, relative_effect):
    # find the right studies for this outcome
    comps = studies_per_outcome[doi]
    if not comps:
        studies = None
        vals = None
    else:
        try:
            outcomes_to_studies = comps[sof_title]
        except KeyError:
            # attempt selecting the candidate based on MED
            outcomes_to_studies, _ = get_from_best_key(comps, sof_title)
        if outcomes_to_studies is not None:
            try:
                studies = outcomes_to_studies[outcome]["studies"]
                studies = set(studies)
                vals = {}
                for k, v in outcomes_to_studies[outcome].items():
                    if k.startswith("val_"):
                        vals[k] = v
            except KeyError:
                # attempt selecting the candidate based on MED
                _studies, best_outcome = get_from_best_key(outcomes_to_studies, outcome)
                studies = _studies["studies"]
                vals = {}
                for k, v in _studies.items():
                    if k.startswith("val_"):
                        vals[k] = v
                studies = set(studies)
                # sanity check
                accept = len(studies) == n_studies and _studies["effect_measure"] == type_effect and equal_effect_size(
                    _studies["effect_size"], relative_effect)
                if not accept:
                    studies = None
                    vals = None
        else:
            studies = None
            vals = None
    return studies, vals


def stats_studies_per_outcome(fs, studies_per_outcome):
    total = 0
    relevant = 0
    c_subset = Counter()
    length_diffs = []

    for f in tqdm(fs):
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    total += 1
                    if "labels" in outcome["reasons"] and sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[
                        grade]:
                        relevant += 1
                        # find the right studies for this outcome
                        studies, vals = get_relevant_studies(studies_per_outcome, fh["doi"], sof["title"], outcome["O"],
                                                             outcome["n_studies"],
                                                             outcome["type_effect"],
                                                             outcome["relative_effect"])
                        if studies is not None:
                            c_subset[len(studies)] += 1
                            if len(fh['studies']["included"]) > 0:
                                length_diffs.append(len(studies) / len(fh['studies']["included"]))

    # print(f"{(relevant / total) * 100:.3f} %")
    print(f"N of relevant: {relevant}")
    print(sum(c_subset.values()))
    print(describe(length_diffs))
    for k, v in c_subset.items():
        print(k, v / sum(c_subset.values()))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_f',
                        help='input json file containing a mapping between DOI and studies per outcome.')
    parser.add_argument('-input_dir',
                        help='input dir containing parsed json files')
    parser.add_argument('-metadata_f',
                        help='path to metadata.csv')
    args = parser.parse_args()

    fs = get_file_list(args.input_dir)
    studies_per_outcome = load_json(args.input_f)
    # n_strict_reasons(fs)
    stats_studies_per_outcome(fs, studies_per_outcome)
