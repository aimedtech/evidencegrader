import argparse
import csv
import re
from collections import defaultdict, Counter
from pathlib import Path

from scipy import stats

from sysrev.dataset_construction.parse.evaluation.evaluation import GRADE_TO_STEPS
from sysrev.dataset_construction.scrape.metadata import get_docid_to_topics, get_docid_to_year
from sysrev.dataset_construction.util.util import get_file_list, load_json

LABEL_TO_STR = {"inconsistency":
    {
        "inconsistency",
        "heterogeneity"
    },
    "imprecision":
        {
            "imprecision",
            "wide confidence interval",
            "wide 95% confidence interval"
        },
    "indirectness":
        {
            "indirectness"
        },
    "risk of bias":
        {
            "risk of bias",
            "risk of selection bias",
            "risk of serious bias",
            "study design or execution",
            "allocation concealment",
            "blinding"
        },
    "publication bias":
        {
            "publication bias",
            "selective publication"
        }
}


def get_data(input_f):
    data_dict = defaultdict(list)

    with open(input_f) as fh:
        reader = csv.DictReader(
            fh)  # "n_file", "n_outcome", "doi", "n_included", "n_outcome_studies", "grade", "reasons"
        for row in reader:
            data_dict[row["doi"]].append({"n_file": int(row["n_file"]),
                                          "n_outcome": int(row["n_outcome"]),
                                          "n_included": int(row["n_included"]),
                                          "n_outcome_studies": int(row["n_outcome_studies"]),
                                          "grade": row["grade"],
                                          "reasons": row["reasons"]})

    return data_dict


def text_stats(fs):
    """
    Prints statistic of 'texts' field in parsed files. Counting texts for every instance (although they can be repeated).
    """
    import numpy as np

    def get_len(field):
        field_len = None
        field_txt = fh["texts"][field]
        if field_txt is not None:
            field_len = len(field_txt.split())

        return field_len

    def update_list(l, field_len):
        if field_len is not None:
            l.append(field_len)

    def percentage_over_max_len(l, max_len=512):
        return (sum(np.array(l) > max_len) / len(l)) * 100

    lens_full_abstract_txt = []
    lens_abstract_conclusion_txt = []
    lens_plain_language_summary_txt = []
    lens_authors_conclusions_txt = []
    lens_outcome = []

    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    if "labels" in outcome["reasons"] and sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[
                        grade]:
                        update_list(lens_full_abstract_txt, get_len("full_abstract_txt"))
                        update_list(lens_abstract_conclusion_txt, get_len("abstract_conclusion_txt"))
                        update_list(lens_plain_language_summary_txt, get_len("plain_language_summary_txt"))
                        update_list(lens_authors_conclusions_txt, get_len("authors_conclusions_txt"))
                        update_list(lens_outcome, len(outcome["O"].split()) if outcome["O"] is not None else None)
                elif grade == "high":
                    update_list(lens_full_abstract_txt, get_len("full_abstract_txt"))
                    update_list(lens_abstract_conclusion_txt, get_len("abstract_conclusion_txt"))
                    update_list(lens_plain_language_summary_txt, get_len("plain_language_summary_txt"))
                    update_list(lens_authors_conclusions_txt, get_len("authors_conclusions_txt"))
                    update_list(lens_outcome, len(outcome["O"].split()) if outcome["O"] is not None else None)

    print("full_abstract_txt:")
    print(stats.describe(lens_full_abstract_txt))
    print(f"% over 512: {percentage_over_max_len(lens_full_abstract_txt)}")

    print("abstract_conclusion_txt:")
    print(stats.describe(lens_abstract_conclusion_txt))
    max_len = 150
    print(f"% over {max_len}: {percentage_over_max_len(lens_abstract_conclusion_txt, max_len=max_len)}")

    print("plain_language_summary_txt:")
    print(stats.describe(lens_plain_language_summary_txt))
    print(f"% over 512: {percentage_over_max_len(lens_plain_language_summary_txt)}")

    print("authors_conclusions_txt:")
    print(stats.describe(lens_authors_conclusions_txt))
    print(f"% over 512: {percentage_over_max_len(lens_authors_conclusions_txt)}")

    print("outcome:")
    print(stats.describe(lens_outcome))
    max_len = 20
    print(f"% over {max_len}: {percentage_over_max_len(lens_outcome, max_len=max_len)}")


def n_strict_reasons(fs):
    """
    Prints the ratio between  number of cases with GRADE score in which the number of reasons corresponds to n of downgraded steps due to GRADE score.
    Prints the distributions of GRADE scores for all instances and separately for the subset.
    """
    total = 0
    relevant = 0
    c = Counter()
    c_subset = Counter()

    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    total += 1
                    if "labels" in outcome["reasons"] and sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[
                        grade]:
                        c_subset[grade] += 1
                        relevant += 1
                    c[grade] += 1

    print(f"{(relevant / total) * 100:.3f} %")
    print(f"N of relevant: {relevant}")
    print(f"GRADE dist: {c}")
    for k, v in c.items():
        print(k, v / sum(c.values()))
    print(f"GRADE dist for subset: {c_subset}")
    for k, v in c_subset.items():
        print(k, v / sum(c_subset.values()))


def n_strict_reasons_topics(fs, metadata_f):
    """
    Prints the distributions of topics for all instances and separately for the subset.
    """
    docid_to_topics = get_docid_to_topics(metadata_f)
    c = Counter()
    c_subset = Counter()

    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    topics = docid_to_topics[Path(f).stem]
                    if "labels" in outcome["reasons"] and sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[
                        grade]:
                        c_subset.update(topics)
                    c.update(topics)

    print(f"Topic dist: {c}")
    for k, v in c.most_common(20):
        print(k, round(v / sum(c.values()), 2))
    print(f"Topic dist for subset: {c_subset}")
    for k, v in c_subset.most_common(20):
        print(k, round(v / sum(c_subset.values()), 2))


def n_strict_reasons_grade(fs, output_dir):
    """
    Prints the GRADE freqs with reasons for the subset.
    """
    c_subset = Counter()
    reasons = {"very low": Counter(), "low": Counter(), "moderate": Counter()}
    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    if "labels" in outcome["reasons"] and sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[
                        grade]:
                        c_subset[grade] += 1
                        reasons[grade].update(
                            [reason for reason, n in outcome["reasons"]["labels"].items() for _ in range(n)])
                if grade is not None and grade == "high":
                    c_subset[grade] += 1

    with open(output_dir + "grades", mode='w') as fh_out_grades, \
            open(output_dir + "moderate", mode='w') as fh_out_moderate, \
            open(output_dir + "low", mode='w') as fh_out_low, \
            open(output_dir + "very_low", mode='w') as fh_out_very_low:

        writer_grades = csv.writer(fh_out_grades)
        writer_mod = csv.writer(fh_out_moderate)
        writer_low = csv.writer(fh_out_low)
        writer_vlow = csv.writer(fh_out_very_low)

        for grade, count in c_subset.items():
            writer_grades.writerow((grade, count))
            print(f"{grade}: {count}")

        print("moderate:")
        for reason, count in sorted(reasons["moderate"].items()):
            if reason in LABEL_TO_STR:
                writer_mod.writerow((reason, count))
                print(f"{reason}: {count}")

        print("low:")
        for reason, count in sorted(reasons["low"].items()):
            if reason in LABEL_TO_STR:
                writer_low.writerow((reason, count))
                print(f"{reason}: {count}")

        print("very low:")
        for reason, count in sorted(reasons["very low"].items()):
            if reason in LABEL_TO_STR:
                writer_vlow.writerow((reason, count))
                print(f"{reason}: {count}")


def n_strict_reasons_year(fs, metadata_f):
    """
    Prints the distributions of topics for all instances and separately for the subset.
    """
    docid_to_year = get_docid_to_year(metadata_f)
    c = Counter()
    c_subset = Counter()

    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    year = docid_to_year[Path(f).stem]
                    if "labels" in outcome["reasons"] and sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[
                        grade]:
                        c_subset.update([year])
                    c.update([year])

    print(f"Year dist: {c}")
    for k, v in c.most_common(20):
        print(k, round(v / sum(c.values()), 2))
    print(f"Year dist for subset: {c_subset}")
    for k, v in c_subset.most_common(20):
        print(k, round(v / sum(c_subset.values()), 2))


def inspect_reasons(fs):
    """
    Prints reasons that mention downgrading twice.
    """
    terms = [i for s in LABEL_TO_STR.values() for i in s]
    c = 0
    for f in fs:
        fh = load_json(f)
        for sof in fh["sofs"]:
            for outcome in sof["outcomes"]:
                grade = outcome["grade"]
                if grade is not None and grade not in {"high", "other"}:
                    if not sum(outcome["reasons"]["labels"].values()) == GRADE_TO_STEPS[grade]:
                        for reason in outcome["reasons"]["raw"]:
                            found = re.findall(
                                f"(?:twice|two times|two steps|two levels) (?:for|due to|because of) .*?({'|'.join(terms)})",
                                reason, flags=re.IGNORECASE)
                            if found:
                                print(found, reason)

    print(c)


def get_gs_citations(fs):
    """
    :return: {doi: {gs_citation, ...}}
    """
    all_gs_citations = defaultdict(set)  # {doi: [citation]}

    for f in fs:
        fh = load_json(f)
        for study in fh["studies"]["included"]:
            for ref in study["references"]:
                if ref["gs_citation"] is not None:
                    all_gs_citations[fh["doi"]].add(ref["gs_citation"])

    return all_gs_citations


def gs_citation_stats(fs):
    gs_citations = get_gs_citations(fs)

    # get gs_citation to doi map:
    d = defaultdict(list)
    for k, v in gs_citations.items():
        for cit in v:
            d[cit].append(k)

    # print stats
    print(f"n GS citations: {len(d)}")
    print(f"distribution of n of reviews (dois) per citation: {Counter([len(v) for v in d.values()])}")
    print("example citation-dois when a citation features in >1 review (doi)")
    for k, v in list(d.items())[:100]:
        if len(v) > 1:
            print(k, v)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-input_dir',
                        help='input dir containing parsed json files')
    parser.add_argument('-metadata_f',
                        help='path to metadata.csv')
    parser.add_argument('-output_dir',
                        help='output dir to which potential output files are written to.')
    args = parser.parse_args()

    fs = get_file_list(args.input_dir)

    # text_stats(fs)
    # gs_citation_stats(fs)
    # n_strict_reasons(fs)
    # n_strict_reasons_grade(fs, args.output_dir)
    n_strict_reasons_topics(fs, args.metadata_f)
    # n_strict_reasons_year(fs, args.metadata_f)
    # inspect_reasons(fs)

    """
    print(f"n reviews (dois): {len(data_dict)}")
    counter = Counter([d["grade"] for v in data_dict.values() for d in v])
    print(counter)
    print(sum([v for k, v in counter.items() if k != "other"]))

    n_studies = []
    for v in data_dict.values():
        for d in v:
            if d["grade"] == "other" or d["n_outcome_studies"] == 0:
                continue
            n_studies.append((d["n_included"], d["n_outcome_studies"]))
    n_same = 0
    for k, v in n_studies:
        if k == v:
            n_same += 1
    print(n_same, len(n_studies))

    print(np.mean([n for n,_ in n_studies]))
    print(np.mean([n for _,n in n_studies]))

    """
